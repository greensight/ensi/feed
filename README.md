# Ensi Feed

## Main info

Name: Ensi Feed  
Domain: Feeds  
Purpose: Transferring data in the required format and semantics to third-party services (advertising systems, marketplaces);
Updating information about products, prices and stocks on third-party sites.

## Development

Instructions describing how to deploy, launch and test the service on a local machine can be found in a separate document at [Gitlab Pages](https://ensi-platform.gitlab.io/docs/tech/back)

The regulations for working on tasks are also in [Gitlab Pages](https://ensi-platform.gitlab.io/docs/guid/regulations)

## Service structure

You can read about the service structure [here](https://docs.ensi.tech/backend-guides/principles/service-structure)

## Dependencies

| Name                    | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | Environment variables                                                                                                                          |
|-------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------|
| PostgreSQL              | Service database                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | DB_CONNECTION<br/>DB_HOST<br/>DB_PORT<br/>DB_DATABASE<br/>DB_USERNAME<br/>DB_PASSWORD                                                          |
| Kafka                   | Message broker. <br/> Consumer is listening to following topics:<br/> `<contour>.catalog.fact.offers.1`</br>`<contour>.catalog.fact.stocks.1`</br>`<contour>.catalog.fact.brands.1`</br>`<contour>.catalog.fact.categories.1`</br>`<contour>.catalog.fact.property-directory-values.1`</br>`<contour>.catalog.fact.properties.1`</br>`<contour>.catalog.fact.published-products.1`</br>`<contour>.catalog.fact.category-product-links.1`</br>`<contour>.catalog.fact.published-property-values.1`</br>`<contour>.catalog.fact.product-groups.1`</br>`<contour>.catalog.fact.product-group-products.1`</br>`<contour>.cms.fact.discount-catalog-calculate.1` | KAFKA_CONTOUR<br/>KAFKA_BROKER_LIST<br/>KAFKA_SECURITY_PROTOCOL<br/>KAFKA_SASL_MECHANISMS<br/>KAFKA_SASL_USERNAME<br/>KAFKA_SASL_PASSWORD<br/> |
| **Ensi services**       | **Ensi services with which this service communicates**                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| Catalog                 | Ensi Offers<br/>Ensi PIM                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    | CATALOG_OFFERS_SERVICE_HOST<br/>CATALOG_PIM_SERVICE_HOST                                                                                       |
| Marketing               | Ensi Marketing                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              | MARKETING_MARKETING_SERVICE_HOST                                                                                                               |
| **Ensi Cloud services** | **Ensi services with which this service communicates**                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| Api                     | Ensi Cloud Api                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              | CLOUD_API_SERVICE_HOST                                                                                                                         |

## Environments

### Test

CI: https://jenkins-infra.ensi.tech/job/ensi-stage-1/job/feeds/job/feed/  
URL: https://feeds-master-dev.ensi.tech/docs/swagger  

### Preprod

N/A

### Prod

N/A

## Contacts

The team supporting this service: https://gitlab.com/groups/greensight/ensi/-/group_members  
Email: mail@greensight.ru

## License

[Open license for the right to use the Greensight Ecom Platform (GEP) computer program](LICENSE.md).
