<?php

namespace Tests;

use Ensi\CloudApiSdk\Api\IndexesApi;
use Ensi\MarketingClient\Api\CalculatorsApi;
use Mockery\MockInterface;

trait MockServicesApi
{
    public function mockMarketingCalculatorsApi(): MockInterface|CalculatorsApi
    {
        return $this->mock(CalculatorsApi::class);
    }

    public function mockCloudApiIndexesApi(): MockInterface|IndexesApi
    {
        return $this->mock(IndexesApi::class);
    }
}
