<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('brands', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('brand_id')->unique();

            $table->string('name');
            $table->string('code');
            $table->text('description')->nullable();
            $table->string('logo_file')->nullable();
            $table->string('logo_url')->nullable();

            $table->boolean('is_migrated')->default(true);
            $table->timestamps(6);
        });

        Schema::create('categories', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('category_id')->unique();

            $table->string('name');
            $table->string('code');
            $table->unsignedBigInteger('parent_id')->nullable();

            $table->boolean('is_active')->default(true);
            $table->boolean('is_real_active')->default(true);

            $table->boolean('is_migrated')->default(true);
            $table->timestamps(6);
        });

        Schema::create('offers', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('offer_id')->unique();

            $table->unsignedBigInteger('product_id');

            $table->unsignedBigInteger('base_price')->nullable();
            $table->unsignedBigInteger('price')->nullable();

            $table->boolean('is_migrated')->default(true);
            $table->timestamps(6);
        });

        Schema::create('products', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('product_id')->unique();

            $table->boolean('allow_publish')->default(true);
            $table->string('main_image_file')->nullable();
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('brand_id')->nullable();
            $table->string('name');
            $table->string('code');
            $table->text('description')->nullable();
            $table->integer('type');
            $table->string('vendor_code');
            $table->string('barcode')->nullable();
            $table->decimal('weight', 18, 4)->nullable();
            $table->decimal('weight_gross', 18, 4)->nullable();
            $table->decimal('width', 18, 4)->nullable();
            $table->decimal('height', 18, 4)->nullable();
            $table->decimal('length', 18, 4)->nullable();
            $table->boolean('is_adult')->nullable();

            $table->boolean('is_migrated')->default(true);
            $table->timestamps(6);
        });

        Schema::create('product_groups', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('product_group_id')->unique();
            $table->unsignedBigInteger('category_id');

            $table->string('name');

            $table->unsignedBigInteger('main_product_id')->nullable();
            $table->boolean('is_active')->default(true);

            $table->boolean('is_migrated')->default(true);
            $table->timestamps(6);
        });

        Schema::create('product_group_product', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('product_group_product_id')->unique();

            $table->unsignedBigInteger('product_group_id');
            $table->unsignedBigInteger('product_id');

            $table->boolean('is_migrated')->default(true);
            $table->timestamps(6);
        });

        Schema::create('product_property_values', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('product_property_value_id')->unique();

            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('property_id');
            $table->unsignedBigInteger('directory_value_id')->nullable();
            $table->string('type', 20)->nullable();
            $table->string('value', 1000)->nullable();
            $table->string('name', 1000)->nullable();

            $table->boolean('is_migrated')->default(true);
            $table->timestamps(6);
        });

        Schema::create('properties', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('property_id')->unique();

            $table->string('name', 500);
            $table->string('code');
            $table->string('type', 20);
            $table->boolean('is_public')->default(true);
            $table->boolean('is_active')->default(true);

            $table->boolean('is_migrated')->default(true);
            $table->timestamps(6);
        });

        Schema::create('property_directory_values', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('directory_value_id')->unique();

            $table->unsignedBigInteger('property_id');
            $table->string('name', 1000)->nullable();
            $table->string('code')->nullable();
            $table->string('value', 1000);
            $table->string('type', 20);

            $table->boolean('is_migrated')->default(true);
            $table->timestamps(6);
        });

        Schema::create('stocks', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('stock_id')->unique();

            $table->unsignedBigInteger('store_id');
            $table->unsignedBigInteger('offer_id');

            $table->decimal('qty', 18, 4);

            $table->boolean('is_migrated')->default(true);
            $table->timestamps(6);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('stocks');
        Schema::dropIfExists('property_directory_values');
        Schema::dropIfExists('properties');
        Schema::dropIfExists('product_property_values');
        Schema::dropIfExists('product_group_product');
        Schema::dropIfExists('product_groups');
        Schema::dropIfExists('products');
        Schema::dropIfExists('offers');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('brands');
    }
};
