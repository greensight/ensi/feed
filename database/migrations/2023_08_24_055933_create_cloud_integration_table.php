<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up()
    {
        Schema::create('cloud_integration', function (Blueprint $table) {
            $table->id();

            $table->boolean('integration');

            $table->string('private_api_key')->nullable();
            $table->string('public_api_key')->nullable();
            $table->string('stage')->unique();

            $table->timestamps(6);
        });
    }

    public function down()
    {
        Schema::dropIfExists('cloud_integration');
    }
};
