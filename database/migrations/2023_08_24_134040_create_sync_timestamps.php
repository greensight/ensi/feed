<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up()
    {
        Schema::create('sync_timestamps', function (Blueprint $table) {
            $table->id();

            $table->string('type');

            $table->string('stage');

            $table->timestamp('last_schedule', 6);
        });
    }

    public function down()
    {
        Schema::dropIfExists('sync_timestamps');
    }
};
