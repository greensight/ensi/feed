<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('category_product_links', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('category_product_id')->unique();
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('product_id');

            $table->boolean('is_migrated')->default(true);

            $table->timestamps(6);

            $table->index(['category_id']);
            $table->index(['product_id']);
        });

        Schema::table('products', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id')->nullable()->change();
        });
    }

    public function down(): void
    {
        DB::table('category_product_links')->select(['id', 'product_id', 'category_id'])->chunkById(100, function (Collection $productLinks) {
            foreach ($productLinks as $productLink) {
                DB::table('products')
                    ->where('product_id', $productLink->product_id)
                    ->whereNull('category_id')
                    ->update(['category_id' => $productLink->category_id]);
            }
        });

        Schema::table('products', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id')->nullable(false)->change();
        });

        Schema::dropIfExists('category_product_links');
    }
};
