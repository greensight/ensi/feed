<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('feeds', function (Blueprint $table) {
            $table->id()->index();
            $table->string('name')->nullable(false);
            $table->string('type')->nullable(false);
            $table->integer('created_by')->nullable(false);
            $table->integer('updated_by')->nullable(true)->default(null);
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('feeds');
    }
};
