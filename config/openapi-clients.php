<?php

return [
    'catalog' => [
        'offers' => [
            'base_uri' => env('CATALOG_OFFERS_SERVICE_HOST') . "/api/v1",
        ],
        'pim' => [
            'base_uri' => env('CATALOG_PIM_SERVICE_HOST') . "/api/v1",
        ],
    ],
    'marketing' => [
        'marketing' => [
            'base_uri' => env('MARKETING_MARKETING_SERVICE_HOST') . "/api/v1",
        ],
    ],
    'external' => [
        'cloud-api' => [
            'base_uri' => env('CLOUD_API_SERVICE_HOST') . "/api/v1",
        ],
    ],
];
