<?php

// configurattion options can be found here: https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md
// if an option is set to null it is ignored.
$contour = env('KAFKA_CONTOUR', 'local');

return [
    'connections' => [
        'default' => [
            'settings' => [
                'metadata.broker.list' => env('KAFKA_BROKER_LIST'),
                'security.protocol' => env('KAFKA_SECURITY_PROTOCOL', 'plaintext'),
                'sasl.mechanisms' => env('KAFKA_SASL_MECHANISMS'),
                'sasl.username' => env('KAFKA_SASL_USERNAME'),
                'sasl.password' => env('KAFKA_SASL_PASSWORD'),
                'log_level' => env('KAFKA_DEBUG', false) ? (string)LOG_DEBUG : (string)LOG_INFO,
                'debug' => env('KAFKA_DEBUG', false) ? 'all' : null,
            ],
            'topics' => [
                'offers' => $contour . '.catalog.fact.offers.1',
                'stocks' => $contour . '.catalog.fact.stocks.1',
                'brands' => $contour . '.catalog.fact.brands.1',
                'categories' => $contour . '.catalog.fact.categories.1',
                'property-directory-values' => $contour . '.catalog.fact.property-directory-values.1',
                'properties' => $contour . '.catalog.fact.properties.1',
                'published-products' => $contour . '.catalog.fact.published-products.1',
                'category-product-links' => $contour . '.catalog.fact.category-product-links.1',
                'published-property-values' => $contour . '.catalog.fact.published-property-values.1',
                'product-groups' => $contour . '.catalog.fact.product-groups.1',
                'product-group-products' => $contour . '.catalog.fact.product-group-products.1',
                'discount-catalog-calculate' => $contour . '.marketing.command.discount-catalog-calculate.1',
            ],
        ],
    ],
    'consumers' => [
        'default' => [
            'connection' => 'default',
            'additional-settings' => [
                'group.id' => env('KAFKA_CONSUMER_GROUP_ID', env('APP_NAME')),
                'enable.auto.commit' => true,
                'auto.offset.reset' => 'beginning',
            ],
        ],
    ],
    'producers' => [
        'default' => [
            'connection' => 'default',
            'additional-settings' => [
                'compression.codec' => env('KAFKA_PRODUCER_COMPRESSION_CODEC', 'snappy'),
            ],
        ],
    ],
];
