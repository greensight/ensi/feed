<?php

use App\Domain\Feeds\Models\Feed;
use App\Domain\Feeds\Models\FeedSettings;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;
use Illuminate\Support\Collection;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'feeds');

test('GET /api/v1/feeds/feeds/{id} 200', function (?bool $always) {
    /** @var ApiV1ComponentTestCase $this */
    FakerProvider::$optionalAlways = $always;

    /** @var Feed $feed */
    $feed = Feed::factory()->create();

    getJson("/api/v1/feeds/feeds/{$feed->id}")
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'code', 'file']]);
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/feeds/feeds/{id} 404', function () {
    getJson('/api/v1/feeds/feeds/404')
        ->assertNotFound();
});

test('POST /api/v1/feeds/feeds:search 200', function (?bool $always) {
    /** @var ApiV1ComponentTestCase $this */
    FakerProvider::$optionalAlways = $always;

    /** @var Collection<Feed> $feeds */
    $feeds = Feed::factory()
        ->forEachSequence(
            ['code' => 'code_1'],
            ['code' => 'code_2'],
        )
        ->create();

    postJson('/api/v1/feeds/feeds:search', [
        "filter" => ["code_like" => '2'],
        "sort" => ["-id"],
    ])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $feeds->last()->id)
        ->assertJsonPath('data.0.code', 'code_2');
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/feeds/feeds:search filter success', function (
    string  $fieldKey,
    mixed   $value,
    ?string $filterKey,
    mixed   $filterValue,
    ?bool   $always
) {
    /** @var ApiV1ComponentTestCase $this */
    FakerProvider::$optionalAlways = $always;

    /** @var Feed $feed */
    $feed = Feed::factory()->create($value ? [$fieldKey => $value] : []);
    Feed::factory()->create();

    postJson('/api/v1/feeds/feeds:search', ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $feed->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $feed->id);
})->with([
    ['id', null, null, null],
    ['feed_settings_id', fn () => FeedSettings::factory()->create(['id' => 1]), 'feed_settings_id', 1],
    ['code', 'Foo', 'code_like', 'o'],


    ['planned_delete_at', '2022-04-20', 'planned_delete_at_gte', '2022-04-19'],
    ['planned_delete_at', '2022-04-20', 'planned_delete_at_lte', '2022-04-21'],

    ['created_at', '2022-04-20', 'created_at_gte', '2022-04-19'],
    ['created_at', '2022-04-20', 'created_at_lte', '2022-04-21'],
    ['updated_at', '2022-04-20', 'updated_at_gte', '2022-04-19'],
    ['updated_at', '2022-04-20', 'updated_at_lte', '2022-04-21'],
], FakerProvider::$optionalDataset);

test("POST /api/v1/feeds/feeds:search sort success", function (string $sort, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    Feed::factory()->create();
    postJson("/api/v1/feeds/feeds:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id', 'code', 'planned_delete_at', 'created_at', 'updated_at',
])->with(FakerProvider::$optionalDataset);

test("POST /api/v1/feeds/feeds:search include success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var FeedSettings $feedSettings */
    $feedSettings = FeedSettings::factory()->create();
    Feed::factory()->for($feedSettings)->create();

    postJson(
        "/api/v1/feeds/feeds:search",
        ["include" => ['feed_settings']]
    )
        ->assertOk()
        ->assertJsonPath('data.0.feed_settings.id', $feedSettings->id);
})->with(FakerProvider::$optionalDataset);
