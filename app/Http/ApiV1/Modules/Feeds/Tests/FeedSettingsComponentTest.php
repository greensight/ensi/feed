<?php

use App\Domain\Feeds\Models\Feed;
use App\Domain\Feeds\Models\FeedSettings;
use App\Http\ApiV1\Modules\Feeds\Tests\Factories\FeedSettingsRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\FeedPlatformEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\FeedTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;
use Illuminate\Support\Collection;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'feed-settings');

test('POST /api/v1/feeds/feed-settings 201', function (?bool $always) {
    /** @var ApiV1ComponentTestCase $this */
    FakerProvider::$optionalAlways = $always;

    $request = FeedSettingsRequestFactory::new()->make();

    $id = postJson('/api/v1/feeds/feed-settings', $request)
        ->assertCreated()
        ->json('data.id');

    assertDatabaseHas(FeedSettings::class, ['id' => $id]);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/feeds/feed-settings/{id} 200', function (?bool $always) {
    /** @var ApiV1ComponentTestCase $this */
    FakerProvider::$optionalAlways = $always;

    /** @var FeedSettings $feedSettings */
    $feedSettings = FeedSettings::factory()->create();
    $newCode = $feedSettings->code . '_new';

    $request = FeedSettingsRequestFactory::new()->make(['code' => $newCode]);

    patchJson("/api/v1/feeds/feed-settings/{$feedSettings->id}", $request)
        ->assertOk()
        ->assertJsonPath('data.code', $newCode);

    assertDatabaseHas(FeedSettings::class, ['id' => $feedSettings->id, 'code' => $newCode]);
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/feeds/feed-settings/{id} 200', function (?bool $always) {
    /** @var ApiV1ComponentTestCase $this */
    FakerProvider::$optionalAlways = $always;

    /** @var FeedSettings $feedSettings */
    $feedSettings = FeedSettings::factory()->create();

    getJson("/api/v1/feeds/feed-settings/{$feedSettings->id}")
        ->assertOk()
        ->assertJsonPath('data.id', $feedSettings->id)
        ->assertJsonStructure(['data' => ['id', 'name', 'code', 'active']]);
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/feeds/feed-settings/{id} 404', function () {
    getJson('/api/v1/feeds/feed-settings/404')
        ->assertNotFound();
});

test('POST /api/v1/feeds/feed-settings:search 200', function (?bool $always) {
    /** @var ApiV1ComponentTestCase $this */
    FakerProvider::$optionalAlways = $always;

    /** @var Collection<FeedSettings> $feedSettings */
    $feedSettings = FeedSettings::factory()
        ->forEachSequence(
            ['code' => 'code_1'],
            ['code' => 'code_2'],
        )
        ->create();

    postJson('/api/v1/feeds/feed-settings:search', [
        "filter" => ["code_like" => '2'],
        "sort" => ["-id"],
    ])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $feedSettings->last()->id)
        ->assertJsonPath('data.0.code', 'code_2');
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/feeds/feed-settings:search filter success', function (
    string  $fieldKey,
    mixed   $value,
    ?string $filterKey,
    mixed   $filterValue,
    ?bool   $always
) {
    /** @var ApiV1ComponentTestCase $this */
    FakerProvider::$optionalAlways = $always;

    /** @var FeedSettings $feedSettings */
    $feedSettings = FeedSettings::factory()->create($value ? [$fieldKey => $value] : []);
    FeedSettings::factory()->create();

    postJson('/api/v1/feeds/feed-settings:search', ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $feedSettings->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $feedSettings->id);
})->with([
    ['id', null, null, null],
    ['name', 'Foo', 'name_like', 'o'],
    ['code', 'Foo', 'code_like', 'o'],
    ['active', true, null, null],

    ['type', FeedTypeEnum::YML, 'type', [FeedTypeEnum::YML]],
    ['platform', FeedPlatformEnum::YANDEX_DIRECT, 'platform', [FeedPlatformEnum::YANDEX_DIRECT]],

    ['created_at', '2022-04-20', 'created_at_gte', '2022-04-19'],
    ['created_at', '2022-04-20', 'created_at_lte', '2022-04-21'],
    ['updated_at', '2022-04-20', 'updated_at_gte', '2022-04-19'],
    ['updated_at', '2022-04-20', 'updated_at_lte', '2022-04-21'],
], FakerProvider::$optionalDataset);

test("POST /api/v1/feeds/feed-settings:search sort success", function (string $sort, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    FeedSettings::factory()->create();
    postJson("/api/v1/feeds/feed-settings:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id', 'name', 'code', 'active', 'type', 'platform', 'created_at', 'updated_at',
])->with(FakerProvider::$optionalDataset);

test("POST /api/v1/feeds/feed-settings:search include success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var FeedSettings $feedSettings */
    $feedSettings = FeedSettings::factory()->create();
    /** @var Feed $feed */
    $feed = Feed::factory()->for($feedSettings)->create();

    postJson(
        "/api/v1/feeds/feed-settings:search",
        ["include" => ['feeds']]
    )
        ->assertOk()
        ->assertJsonPath('data.0.feeds.0.id', $feed->id);
})->with(FakerProvider::$optionalDataset);
