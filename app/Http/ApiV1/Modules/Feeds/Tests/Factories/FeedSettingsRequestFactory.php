<?php

namespace App\Http\ApiV1\Modules\Feeds\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\FeedPlatformEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\FeedTypeEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class FeedSettingsRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'name' => $this->faker->sentence(),
            'code' => $this->faker->unique()->word(),
            'active' => $this->faker->boolean(),
            'type' => $this->faker->randomEnum(FeedTypeEnum::cases()),
            'platform' => $this->faker->randomEnum(FeedPlatformEnum::cases()),
            'active_product' => $this->faker->boolean(),
            'active_category' => $this->faker->boolean(),
            'shop_name' => $this->faker->sentence(),
            'shop_url' => $this->faker->sentence(),
            'shop_company' => $this->faker->sentence(),
            'update_time' => $this->faker->numberBetween(1, 100),
            'delete_time' => $this->faker->numberBetween(1, 100),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
