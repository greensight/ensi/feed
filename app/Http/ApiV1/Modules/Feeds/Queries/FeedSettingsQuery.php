<?php

namespace App\Http\ApiV1\Modules\Feeds\Queries;

use App\Domain\Feeds\Models\FeedSettings;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class FeedSettingsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(FeedSettings::query());

        $this->allowedSorts(['id', 'name', 'code', 'active', 'type', 'platform', 'created_at', 'updated_at']);

        $this->allowedIncludes(['feeds']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            ...StringFilter::make('name')->contain(),
            ...StringFilter::make('code')->contain(),
            AllowedFilter::exact('active'),
            AllowedFilter::exact('type'),
            AllowedFilter::exact('platform'),
            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);

        $this->defaultSort('id');
    }
}
