<?php

namespace App\Http\ApiV1\Modules\Feeds\Queries;

use App\Domain\Feeds\Models\Feed;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

class FeedsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Feed::query());

        $this->allowedSorts(['id', 'code', 'planned_delete_at', 'created_at', 'updated_at']);

        $this->allowedIncludes([
            AllowedInclude::relationship('feed_settings', 'feedSettings'),
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('feed_settings_id'),
            ...StringFilter::make('code')->contain(),
            ...DateFilter::make('planned_delete_at')->lte()->gte(),
            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);

        $this->defaultSort('id');
    }
}
