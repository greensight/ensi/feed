<?php

namespace App\Http\ApiV1\Modules\Feeds\Controllers;

use App\Domain\Feeds\Actions\CreateFeedSettingsAction;
use App\Domain\Feeds\Actions\PatchFeedSettingsAction;
use App\Http\ApiV1\Modules\Feeds\Queries\FeedSettingsQuery;
use App\Http\ApiV1\Modules\Feeds\Requests\CreateFeedSettingsRequest;
use App\Http\ApiV1\Modules\Feeds\Requests\PatchFeedSettingsRequest;
use App\Http\ApiV1\Modules\Feeds\Resources\FeedSettingsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class FeedSettingsController
{
    public function create(CreateFeedSettingsRequest $request, CreateFeedSettingsAction $action): Responsable
    {
        return FeedSettingsResource::make($action->execute($request->validated()));
    }

    public function patch(int $id, PatchFeedSettingsRequest $request, PatchFeedSettingsAction $action): Responsable
    {
        return FeedSettingsResource::make($action->execute($id, $request->validated()));
    }

    public function get(int $id, FeedSettingsQuery $query): Responsable
    {
        return FeedSettingsResource::make($query->findOrFail($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, FeedSettingsQuery $query): Responsable
    {
        return FeedSettingsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
