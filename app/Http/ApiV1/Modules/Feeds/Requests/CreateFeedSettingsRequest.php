<?php

namespace App\Http\ApiV1\Modules\Feeds\Requests;

use App\Domain\Feeds\Models\FeedSettings;
use App\Http\ApiV1\OpenApiGenerated\Enums\FeedPlatformEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\FeedTypeEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateFeedSettingsRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'code' => ['required', 'string', Rule::unique(FeedSettings::class)],
            'active' => ['required', 'boolean'],
            'type' => ['required', 'integer', Rule::enum(FeedTypeEnum::class)],
            'platform' => ['required', 'integer', Rule::enum(FeedPlatformEnum::class)],
            'active_product' => ['required', 'boolean'],
            'active_category' => ['required', 'boolean'],
            'shop_name' => ['required', 'string'],
            'shop_url' => ['required', 'string'],
            'shop_company' => ['required', 'string'],
            'update_time' => ['required', 'integer', 'min:1'],
            'delete_time' => ['required', 'integer', 'min:1'],
        ];
    }
}
