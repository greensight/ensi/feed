<?php

namespace App\Http\ApiV1\Modules\Feeds\Requests;

use App\Domain\Feeds\Models\FeedSettings;
use App\Http\ApiV1\OpenApiGenerated\Enums\FeedPlatformEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\FeedTypeEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchFeedSettingsRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $id = (int) $this->route('id');

        return [
            'name' => ['sometimes', 'string'],
            'code' => ['sometimes', 'string', Rule::unique(FeedSettings::class)->ignore($id)],
            'active' => ['sometimes', 'boolean'],
            'type' => ['sometimes', 'integer', Rule::enum(FeedTypeEnum::class)],
            'platform' => ['sometimes', 'integer', Rule::enum(FeedPlatformEnum::class)],
            'active_product' => ['sometimes', 'boolean'],
            'active_category' => ['sometimes', 'boolean'],
            'shop_name' => ['sometimes', 'string'],
            'shop_url' => ['sometimes', 'string'],
            'shop_company' => ['sometimes', 'string'],
            'update_time' => ['sometimes', 'integer', 'min:1'],
            'delete_time' => ['sometimes', 'integer', 'min:1'],
        ];
    }
}
