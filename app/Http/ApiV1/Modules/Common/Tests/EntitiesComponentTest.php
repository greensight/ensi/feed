<?php

use App\Domain\Common\Jobs\MigrateEntitiesJob;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/common/entities:migrate 200', function () {
    Queue::fake();

    postJson('/api/v1/common/entities:migrate')->assertOk();

    Queue::assertPushed(MigrateEntitiesJob::class);
});
