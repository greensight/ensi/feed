<?php

namespace App\Http\ApiV1\Modules\Cloud\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class PatchCloudIntegrationRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $integration = $this->faker->boolean;

        return [
            'private_api_key' => $this->faker->nullable($integration ? 1 : 0)->ean8(),
            'public_api_key' => $this->faker->nullable($integration ? 1 : 0)->ean8(),

            'integration' => $integration,
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
