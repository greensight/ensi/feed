<?php

namespace App\Http\ApiV1\Modules\Cloud\Controllers;

use App\Domain\Cloud\Actions\CloudIntegrations\CreateCloudIntegrationAction;
use App\Domain\Cloud\Actions\CloudIntegrations\GetCloudIntegrationAction;
use App\Domain\Cloud\Actions\CloudIntegrations\PatchCloudIntegrationAction;
use App\Http\ApiV1\Modules\Cloud\Requests\CreateCloudIntegrationRequest;
use App\Http\ApiV1\Modules\Cloud\Requests\PatchCloudIntegrationRequest;
use App\Http\ApiV1\Modules\Cloud\Resources\CloudIntegrationsResource;
use Illuminate\Contracts\Support\Responsable;

class CloudIntegrationsController
{
    public function get(GetCloudIntegrationAction $action): Responsable
    {
        return new CloudIntegrationsResource($action->execute());
    }

    public function create(CreateCloudIntegrationRequest $request, CreateCloudIntegrationAction $action): Responsable
    {
        return CloudIntegrationsResource::make($action->execute($request->validated()));
    }

    public function patch(PatchCloudIntegrationRequest $request, PatchCloudIntegrationAction $action): Responsable
    {
        return CloudIntegrationsResource::make($action->execute($request->validated()));
    }
}
