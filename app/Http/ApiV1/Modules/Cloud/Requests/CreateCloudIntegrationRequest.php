<?php

namespace App\Http\ApiV1\Modules\Cloud\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateCloudIntegrationRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'integration' => [
                'required',
                'boolean',
                'declined_if:public_api_key,"",null',
                'declined_if:private_api_key,"",null',
            ],
            'public_api_key' => ['present', 'nullable', 'string'],
            'private_api_key' => ['present', 'nullable', 'string'],
        ];
    }
}
