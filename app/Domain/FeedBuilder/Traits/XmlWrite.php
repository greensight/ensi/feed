<?php

namespace App\Domain\FeedBuilder\Traits;

use App\Domain\FeedBuilder\Data\CData;
use XMLWriter;

/**
 * @mixin XMLWriter
 */
trait XmlWrite
{
    public function writeNullableAttribute(string $name, string|int|float|null $content): bool
    {
        if ($content === null) {
            return false;
        }

        return $this->writeAttribute($name, (string)$content);
    }

    public function writeNullableElement(string $name, string|int|float|null $content): bool
    {
        if ($content === null) {
            return false;
        }

        return $this->writeElement($name, (string)$content);
    }

    /**
     * @param array<string, string|int|null> $attributes
     */
    public function writeTag(string $name, string $content, array $attributes): bool
    {
        $this->startElement($name);

        foreach ($attributes as $attribute => $value) {
            $this->writeNullableAttribute($attribute, $value);
        }

        $this->text($content);
        $this->endElement();

        return true;
    }

    public function writeCDataElement(string $name, ?CData $content): bool
    {
        if ($content === null) {
            return false;
        }

        $this->startElement($name);
        $this->writeCdata((string)$content);
        $this->endElement();

        return true;
    }

    public function writeBoolElement(string $name, bool $content): bool
    {
        return $this->writeElement($name, $content ? 'true' : 'false');
    }
}
