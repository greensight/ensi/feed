<?php

namespace App\Domain\FeedBuilder;

use App\Domain\FeedBuilder\Contracts\FeedFile;
use App\Domain\FeedBuilder\Data\EntityData;
use App\Domain\FeedBuilder\Data\FeedInfo;
use Exception;

final class FeedBuilder
{
    public function __construct(protected FeedFile $file, protected EntityData $data)
    {
    }

    public function build(string $name): FeedInfo
    {
        try {
            $this->file->create();
            $this->file->fill($this->data);
            $this->file->save($name);
        } catch (Exception $e) {
            return $this->file->info()->feedFailure($e->getMessage());
        }

        return $this->file->info();
    }
}
