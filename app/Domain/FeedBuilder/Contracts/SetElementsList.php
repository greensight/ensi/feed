<?php

namespace App\Domain\FeedBuilder\Contracts;

use App\Domain\FeedBuilder\Files\FileTypes\XmlFile;
use Illuminate\Support\LazyCollection;

interface SetElementsList
{
    public function execute(XmlFile $file, LazyCollection $collection): void;
}
