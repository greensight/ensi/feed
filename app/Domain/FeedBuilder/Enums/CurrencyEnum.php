<?php

namespace App\Domain\FeedBuilder\Enums;

enum CurrencyEnum: string
{
    case RUR = 'RUR';
}
