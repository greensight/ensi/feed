<?php

namespace App\Domain\FeedBuilder\Files\FileTypes;

use App\Domain\FeedBuilder\Contracts\FeedFile;
use App\Domain\FeedBuilder\Data\FeedInfo;
use App\Domain\FeedBuilder\Traits\XmlWrite;
use Ensi\LaravelEnsiFilesystem\EnsiStorageFacade;
use Illuminate\Support\Facades\Storage;
use XMLWriter;

abstract class XmlFile extends XMLWriter implements FeedFile
{
    use XmlWrite;
    protected FeedInfo $info;

    public function __construct()
    {
        $this->info = new FeedInfo();
    }

    public function create(): void
    {
        $this->openMemory();
        $this->configure();
    }

    private function configure(): void
    {
        $this->startDocument('1.0', 'UTF-8');
        $this->setIndent(true);
    }

    public function save(string $fileName, string $path = self::FILE_DIR): void
    {
        $filePath = $path . $fileName;
        Storage::disk(EnsiStorageFacade::publicDiskName())->put($filePath, $this->outputMemory());

        $this->info->setFilePath($filePath);
    }

    public function info(): FeedInfo
    {
        return $this->info;
    }
}
