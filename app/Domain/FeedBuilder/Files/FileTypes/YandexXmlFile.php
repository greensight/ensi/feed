<?php

namespace App\Domain\FeedBuilder\Files\FileTypes;

use App\Domain\FeedBuilder\Contracts\SetElementsList;
use App\Domain\FeedBuilder\Data\EntityData;

class YandexXmlFile extends XmlFile
{
    private ?EntityData $entities = null;

    public function __construct(
        protected readonly SetElementsList $setCategories,
        protected readonly SetElementsList $setOffers,
    ) {
        parent::__construct();
    }

    public function fill(EntityData $data): void
    {
        $this->entities = $data;

        $this->createTemplate();
        $this->setBody();
        $this->closeAllElement();
    }

    protected function createTemplate(): void
    {
        $this->startElement('yml_catalog');
        $this->writeAttribute('date', date(DATE_RFC3339));
        $this->startElement('shop');

        $this->addShopInfo();
        $this->addCurrencies();
    }

    protected function setBody(): void
    {
        $this->setCategories->execute($this, $this->entities->categories);
        $this->setOffers->execute($this, $this->entities->offers);
    }

    protected function closeAllElement(): void
    {
        $this->endElement();
        $this->fullEndElement();
        $this->fullEndElement();
        $this->endDocument();
    }

    private function addShopInfo(): void
    {
        foreach ($this->entities->shopInfo->toArray() as $name => $value) {
            $this->writeNullableElement($name, $value);
        }
    }

    private function addCurrencies(): void
    {
        $this->startElement('currencies');

        $this->startElement('currency');
        $this->writeAttribute('id', $this->entities->currency->id->value);
        $this->writeAttribute('rate', (string)$this->entities->currency->rate);
        $this->endElement();

        $this->fullEndElement();
    }
}
