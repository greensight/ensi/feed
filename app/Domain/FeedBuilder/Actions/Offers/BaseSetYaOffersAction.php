<?php

namespace App\Domain\FeedBuilder\Actions\Offers;

use App\Domain\FeedBuilder\Contracts\SetElementsList;
use App\Domain\FeedBuilder\Data\EntityData\OfferData;
use App\Domain\FeedBuilder\Files\FileTypes\XmlFile;
use Exception;
use Illuminate\Support\LazyCollection;

abstract class BaseSetYaOffersAction implements SetElementsList
{
    protected XmlFile $file;

    /**
     * @param LazyCollection<OfferData> $collection
     */
    public function execute(XmlFile $file, LazyCollection $collection): void
    {
        $this->file = $file;
        $this->addOffers($collection);
    }

    /**
     * @param LazyCollection<OfferData> $offers
     */
    protected function addOffers(LazyCollection $offers): void
    {
        $this->file->startElement('offers');

        foreach ($offers as $offer) {
            try {
                if ($offer instanceof OfferData) {
                    $this->addOffer($offer);
                }
                $this->file->info()->addSuccessfulOffer();
            } catch (Exception) {
                $this->file->info()->addErrorOffer();
            }
        }

        $this->file->fullEndElement();
    }

    abstract protected function addOffer(OfferData $offer): void;

    protected function addProductParameters(OfferData $offer): void
    {
        foreach ($offer->getParameters() as $parameter) {
            $this->file->writeTag(
                name: 'param',
                content: $parameter->value,
                attributes: [
                    'name' => $parameter->name,
                ],
            );
        }
    }
}
