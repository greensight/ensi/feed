<?php

namespace App\Domain\FeedBuilder\Actions\Offers;

use App\Domain\FeedBuilder\Actions\FormatTextAction;
use App\Domain\FeedBuilder\Data\EntityData\OfferData;

class SetYaMarketOffersAction extends BaseSetYaOffersAction
{
    protected function addOffer(OfferData $offer): void
    {
        $this->file->startElement('offer');
        $this->file->writeAttribute('id', (string)$offer->id);
        $this->file->writeNullableAttribute('group_id', $offer->groupId);

        $this->addOfferData($offer);
        $this->addVendorData($offer);
        $this->addCategoryData($offer);
        $this->addProductData($offer);
        $this->addProductParameters($offer);

        $this->file->fullEndElement();
    }

    protected function addOfferData(OfferData $offer): void
    {
        $this->file->writeElement('price', (string)$offer->getPrice());
        $this->file->writeElement('oldPrice', (string)$offer->getCost());
        $this->file->writeElement('currencyId', $offer->currencyId->value);
        $this->file->writeElement('count', (string)$offer->stockQty);
    }

    protected function addVendorData(OfferData $offer): void
    {
        $this->file->writeNullableElement('vendor', $offer->vendorName);
        $this->file->writeNullableElement('vendorCode', $offer->vendorCode);
    }

    protected function addCategoryData(OfferData $offer): void
    {
        $this->file->writeElement('categoryId', (string)$offer->categoryId);
    }

    protected function addProductData(OfferData $offer): void
    {
        $this->file->writeElement('name', FormatTextAction::execute($offer->name));
        $this->file->writeElement('url', $offer->url);
        $this->file->writeNullableElement('picture', $offer->getMainImageUrl());
        $this->file->writeCDataElement('description', $offer->getDescription());
        $this->file->writeBoolElement('adult', $offer->isAdult);
        $this->file->writeNullableElement('barcode', $offer->barcode);
        $this->file->writeNullableElement('weight', $offer->weight);
        $this->file->writeNullableElement('dimensions', $offer->getStringDimensions());
    }
}
