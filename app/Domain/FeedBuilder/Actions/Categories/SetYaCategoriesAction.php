<?php

namespace App\Domain\FeedBuilder\Actions\Categories;

use App\Domain\FeedBuilder\Actions\FormatTextAction;
use App\Domain\FeedBuilder\Contracts\SetElementsList;
use App\Domain\FeedBuilder\Data\EntityData\CategoryData;
use App\Domain\FeedBuilder\Files\FileTypes\XmlFile;
use Exception;
use Illuminate\Support\LazyCollection;

class SetYaCategoriesAction implements SetElementsList
{
    private XmlFile $file;

    /**
     * @param LazyCollection<CategoryData> $collection
     */
    public function execute(XmlFile $file, LazyCollection $collection): void
    {
        $this->file = $file;
        $this->addCategories($collection);
    }

    /**
     * @param LazyCollection<CategoryData> $categories
     */
    protected function addCategories(LazyCollection $categories): void
    {
        $this->file->startElement('categories');

        foreach ($categories as $category) {
            try {
                if ($category instanceof CategoryData) {
                    $this->addCategory($category);
                }
                $this->file->info()->addSuccessfulCategory();
            } catch (Exception) {
                $this->file->info()->addErrorCategory();
            }
        }

        $this->file->fullEndElement();
    }

    protected function addCategory(CategoryData $category): void
    {
        $this->file->writeTag(
            name: 'category',
            content: FormatTextAction::execute($category->name),
            attributes: [
                'id' => $category->id,
                'parentId' => $category->parentId,
            ],
        );
    }
}
