<?php

namespace App\Domain\FeedBuilder\Actions;

class ValueConverterAction
{
    public static function formatCopToRub(int $price): float
    {
        return $price / 100;
    }

    public static function formatMmToCm(float $distance): float
    {
        return $distance / 1000;
    }
}
