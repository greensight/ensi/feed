<?php

namespace App\Domain\FeedBuilder\Data;

use App\Http\ApiV1\OpenApiGenerated\Enums\FeedPlatformEnum;

class FeedInfo
{
    protected bool $successful = true;
    protected ?string $errorMessage = null;

    protected ?string $filePath = null;

    protected int $countSuccessfulOffers = 0;
    protected int $countErrorOffers = 0;

    protected int $countSuccessfulCategories = 0;
    protected int $countErrorCategories = 0;

    public function isSuccessful(): bool
    {
        return $this->successful;
    }

    public function feedFailure(string $message): self
    {
        $this->successful = false;
        $this->errorMessage = $message;

        return $this;
    }

    public function setFilePath(string $path): void
    {
        $this->filePath = $path;
    }

    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    public function addSuccessfulOffer(): void
    {
        $this->countSuccessfulOffers++;
    }

    public function getSuccessfulOffer(): int
    {
        return $this->countSuccessfulOffers;
    }

    public function addErrorOffer(): void
    {
        $this->countErrorOffers++;
    }

    public function addSuccessfulCategory(): void
    {
        $this->countSuccessfulCategories++;
    }

    public function getSuccessfulCategory(): int
    {
        return $this->countSuccessfulCategories;
    }

    public function addErrorCategory(): void
    {
        $this->countErrorCategories++;
    }

    protected function yandexFeedInfo(): string
    {
        $status = $this->isSuccessful() ? "Да" : "Нет";

        $report = [
            "Файл успешно сгенерирован: {$status}",
            "Кол-во успешно записанных категорий: {$this->countSuccessfulCategories}",
            "Кол-во ошибочных категорий: {$this->countErrorCategories}",
            "Кол-во успешно записанных оферов: {$this->countSuccessfulOffers}",
            "Кол-во ошибочных оферов: {$this->countErrorOffers}",
        ];

        if (!$this->isSuccessful()) {
            $report = array_merge($report, ["Ошибка: {$this->errorMessage}"]);
        }

        return implode(PHP_EOL, $report);
    }

    public function getStringInfo(FeedPlatformEnum $enum): string
    {
        return match ($enum) {
            FeedPlatformEnum::YANDEX_MARKET, FeedPlatformEnum::YANDEX_DIRECT => $this->yandexFeedInfo(),
        };
    }
}
