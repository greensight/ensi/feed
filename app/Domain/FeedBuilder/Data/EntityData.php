<?php

namespace App\Domain\FeedBuilder\Data;

use App\Domain\FeedBuilder\Data\EntityData\CategoryData;
use App\Domain\FeedBuilder\Data\EntityData\CurrencyData;
use App\Domain\FeedBuilder\Data\EntityData\OfferData;
use App\Domain\FeedBuilder\Data\EntityData\ShopInfoData;
use App\Domain\FeedBuilder\Tests\Factories\EntityDataFactory;
use Illuminate\Support\LazyCollection;

class EntityData
{
    /**
     * @param ShopInfoData $shopInfo
     * @param CurrencyData $currency
     * @param LazyCollection<CategoryData> $categories
     * @param LazyCollection<OfferData> $offers
     */
    public function __construct(
        public ShopInfoData $shopInfo,
        public CurrencyData $currency,
        public LazyCollection $categories,
        public LazyCollection $offers,
    ) {
    }

    public static function factory(): EntityDataFactory
    {
        return EntityDataFactory::new();
    }
}
