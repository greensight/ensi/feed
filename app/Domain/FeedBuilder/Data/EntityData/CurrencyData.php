<?php

namespace App\Domain\FeedBuilder\Data\EntityData;

use App\Domain\FeedBuilder\Enums\CurrencyEnum;

class CurrencyData
{
    public function __construct(
        public CurrencyEnum $id,
        public int $rate,
    ) {
    }

    public static function default(): self
    {
        return new self(CurrencyEnum::RUR, 1);
    }
}
