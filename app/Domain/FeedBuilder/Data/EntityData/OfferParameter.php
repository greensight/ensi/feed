<?php

namespace App\Domain\FeedBuilder\Data\EntityData;

class OfferParameter
{
    public function __construct(
        public string $name,
        public string $value,
    ) {
    }
}
