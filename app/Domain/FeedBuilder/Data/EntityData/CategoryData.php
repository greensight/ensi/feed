<?php

namespace App\Domain\FeedBuilder\Data\EntityData;

use App\Domain\FeedBuilder\Tests\Factories\CategoryDataFactory;

class CategoryData
{
    public function __construct(
        public int $id,
        public ?int $parentId,
        public string $name,
    ) {
    }

    public static function factory(): CategoryDataFactory
    {
        return CategoryDataFactory::new();
    }
}
