<?php

namespace App\Domain\FeedBuilder\Data\EntityData;

use App\Domain\FeedBuilder\Actions\ValueConverterAction;
use App\Domain\FeedBuilder\Data\CData;
use App\Domain\FeedBuilder\Enums\CurrencyEnum;
use App\Domain\FeedBuilder\Tests\Factories\OfferDataFactory;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;

class OfferData
{
    /** @var OfferParameter[] */
    private array $parameters = [];

    public function __construct(
        // Offer data
        public int $id,
        public string $url,
        protected int $price,
        protected int $cost,
        public CurrencyEnum $currencyId,
        public float $stockQty,
        // Brand data
        public ?string $vendorName,
        public ?string $vendorCode,
        // Category data
        public int $categoryId,
        // Product data
        public string $name,
        public ?int $groupId,
        protected ?string $mailImageFile,
        protected ?string $description,
        public bool $isAdult,
        public ?string $barcode,
        public ?float $weight,
        protected ?float $width,
        protected ?float $height,
        protected ?float $length,
    ) {
    }

    public function addParameter(OfferParameter $parameter): void
    {
        $this->parameters[] = $parameter;
    }

    public function getParameters(): array
    {
        return $this->parameters;
    }

    public function getDescription(): ?CData
    {
        return $this->description ? new CData($this->description) : null;
    }

    public function getMainImageUrl(): ?string
    {
        return $this->mailImageFile ? EnsiFile::public($this->mailImageFile)->getUrl() : null;
    }

    public function getStringDimensions(): ?string
    {
        if ($this->width && $this->height && $this->length) {
            $width = ValueConverterAction::formatMmToCm($this->width);
            $height = ValueConverterAction::formatMmToCm($this->height);
            $length = ValueConverterAction::formatMmToCm($this->length);

            return "$width/$height/$length";
        }

        return null;
    }

    public function getPrice(): float
    {
        return match ($this->currencyId) {
            CurrencyEnum::RUR => ValueConverterAction::formatCopToRub($this->price),
        };
    }

    public function getCost(): float
    {
        return match ($this->currencyId) {
            CurrencyEnum::RUR => ValueConverterAction::formatCopToRub($this->cost),
        };
    }

    public static function factory(): OfferDataFactory
    {
        return OfferDataFactory::new();
    }
}
