<?php

namespace App\Domain\FeedBuilder\Data\EntityData;

class ShopInfoData
{
    public function __construct(
        public string $name,
        public string $company,
        public string $url,
    ) {
    }

    public function toArray(): array
    {
        return [
            'name' => $this->name,
            'company' => $this->company,
            'url' => $this->url,
        ];
    }
}
