<?php

namespace App\Domain\FeedBuilder\Tests\Factories;

use App\Domain\FeedBuilder\Data\EntityData\CategoryData;
use Ensi\LaravelTestFactories\Factory;

class CategoryDataFactory extends Factory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'parentId' => $this->faker->nullable()->modelId(),
            'name' => $this->faker->sentence(),
        ];
    }

    public function make(array $extra = []): CategoryData
    {
        return new CategoryData(...$this->makeArray($extra));
    }
}
