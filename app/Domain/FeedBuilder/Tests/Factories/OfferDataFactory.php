<?php

namespace App\Domain\FeedBuilder\Tests\Factories;

use App\Domain\FeedBuilder\Data\EntityData\OfferData;
use App\Domain\FeedBuilder\Enums\CurrencyEnum;
use Ensi\LaravelTestFactories\Factory;

class OfferDataFactory extends Factory
{
    protected function definition(): array
    {
        $cost = $this->faker->randomNumber();

        return [
            'id' => $this->faker->modelId(),
            'url' => $this->faker->url(),
            'price' => $this->faker->numberBetween(1, $cost),
            'cost' => $cost,
            'currencyId' => $this->faker->randomElement(CurrencyEnum::cases()),
            'stockQty' => $this->faker->randomFloat(0, 1, 99),

            'vendorName' => $this->faker->nullable()->sentence(),
            'vendorCode' => $this->faker->nullable()->numerify('###-###-###'),

            'categoryId' => $this->faker->modelId(),

            'name' => $this->faker->sentence(),
            'groupId' => $this->faker->nullable()->modelId(),
            'mailImageFile' => $this->faker->nullable()->filePath(),
            'description' => $this->faker->nullable()->text(),
            'isAdult' => $this->faker->boolean(),
            'barcode' => $this->faker->nullable()->ean13(),
            'weight' => $this->faker->nullable()->randomFloat(3, 1, 100),
            'width' => $this->faker->nullable()->randomFloat(2, 1, 1000),
            'height' => $this->faker->nullable()->randomFloat(2, 1, 1000),
            'length' => $this->faker->nullable()->randomFloat(2, 1, 1000),
        ];
    }

    public function make(array $extra = []): OfferData
    {
        return new OfferData(...$this->makeArray($extra));
    }
}
