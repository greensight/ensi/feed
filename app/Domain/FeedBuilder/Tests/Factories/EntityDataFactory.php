<?php

namespace App\Domain\FeedBuilder\Tests\Factories;

use App\Domain\FeedBuilder\Data\EntityData;
use App\Domain\FeedBuilder\Data\EntityData\CategoryData;
use App\Domain\FeedBuilder\Data\EntityData\CurrencyData;
use App\Domain\FeedBuilder\Data\EntityData\OfferData;
use App\Domain\FeedBuilder\Data\EntityData\ShopInfoData;
use App\Domain\Offers\Models\Category;
use App\Domain\Offers\Models\Offer;
use Ensi\LaravelTestFactories\Factory;
use Faker\Generator;
use Illuminate\Support\Collection;

class EntityDataFactory extends Factory
{
    /** @var Collection<Offer> */
    public Collection $offers;
    /** @var Collection<Category> */
    public Collection $categories;

    public function __construct(Generator $faker)
    {
        parent::__construct($faker);
        $this->offers = collect();
        $this->categories = collect();
    }

    protected function definition(): array
    {
        return [
            'name' => $this->faker->sentence(),
            'url' => $this->faker->sentence(),
            'company' => $this->faker->sentence(),
        ];
    }

    public function withOffer(?OfferData $offer = null): self
    {
        $offer = $offer ?? OfferData::factory()->make();

        $this->offers->add($offer);

        return $this;
    }

    public function withCategory(?CategoryData $category = null): self
    {
        $category = $category ?? CategoryData::factory()->make();

        $this->categories->add($category);

        return $this;
    }

    public function withLazyData(): self
    {
        return $this->withOffer()->withCategory();
    }

    public function make(array $extra = []): EntityData
    {
        return new EntityData(
            shopInfo: new ShopInfoData(...$this->makeArray($extra)),
            currency: CurrencyData::default(),
            categories: $this->offers->lazy(),
            offers: $this->categories->lazy(),
        );
    }
}
