<?php

use App\Domain\FeedBuilder\Actions\FormatTextAction;
use App\Domain\FeedBuilder\Data\EntityData;
use App\Domain\FeedBuilder\FeedBuilder;
use App\Domain\FeedBuilder\Files\FileFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\FeedPlatformEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\FeedTypeEnum;
use Illuminate\Support\Facades\Storage;
use PHPUnit\Framework\Assert;
use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('component', 'feeds');

test('Action FormatTextAction repeating symbols', function () {
    $inputString = '&foo&';
    $expectedString = '&amp;foo&amp;';

    $resultString = FormatTextAction::execute($inputString);

    Assert::assertEquals($expectedString, $resultString);
});

test('Action FormatTextAction different symbols', function () {
    $inputString = '<foo>';
    $expectedString = '&lt;foo&gt;';

    $resultString = FormatTextAction::execute($inputString);

    Assert::assertEquals($expectedString, $resultString);
});

test('FeedFile generate feed', function (FeedPlatformEnum $platform, FeedTypeEnum $type) {
    /** @var ComponentTestCase $this */
    Storage::fake();

    $name = "test.xml";

    $fileFactory = resolve(FileFactory::class);
    $feedFile = $fileFactory->execute($platform, $type);
    $data = EntityData::factory()->withLazyData()->make();

    $feedBuilder = new FeedBuilder($feedFile, $data);
    $result = $feedBuilder->build($name);

    Assert::assertEquals(true, $result->isSuccessful());
    Assert::assertNotNull($result->getFilePath());
    Assert::assertEquals(true, Str::endsWith($result->getFilePath(), $name));

    Assert::assertEquals(count($data->offers), $result->getSuccessfulOffer());
    Assert::assertEquals(count($data->categories), $result->getSuccessfulCategory());
})->with([
    'Yandex Market Feed' => [FeedPlatformEnum::YANDEX_MARKET, FeedTypeEnum::YML],
    'Yandex Direct Feed' => [FeedPlatformEnum::YANDEX_DIRECT, FeedTypeEnum::YML],
]);
