<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Category;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\CategoryEventMessageFactory;

class CategoryEventMessage extends ModelEventMessage
{
    public CategoryPayload $attributes;

    public function setAttributes(array $attributes): void
    {
        $this->attributes = new CategoryPayload($attributes);
    }

    public static function factory(): CategoryEventMessageFactory
    {
        return CategoryEventMessageFactory::new();
    }
}
