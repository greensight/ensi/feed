<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

use App\Domain\Offers\Models\ProductGroup;

class ProductGroupEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'name' => $this->faker->sentence(),
            'category_id' => $this->faker->modelId(),
            'main_product_id' => $this->faker->modelId(),
            'is_active' => $this->faker->boolean(),
            'created_at' => $this->faker->date(self::DATE_TIME_FORMAT),
            'updated_at' => $this->faker->date(self::DATE_TIME_FORMAT),
        ];
    }

    public function forModel(ProductGroup $model): static
    {
        return $this->attributes([
            'id' => $model->product_group_id,
            'name' => $model->name,
            'category_id' => $model->category_id,
            'main_product_id' => $model->main_product_id,
            'is_active' => $model->is_active,
            'created_at' => $model->created_at->format(self::DATE_TIME_FORMAT),
            'updated_at' => $model->updated_at->format(self::DATE_TIME_FORMAT),
        ]);
    }
}
