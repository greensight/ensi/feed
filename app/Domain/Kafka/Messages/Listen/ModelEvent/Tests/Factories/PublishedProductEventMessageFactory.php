<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

use App\Domain\Offers\Models\Product;
use Ensi\PimClient\Dto\ProductTypeEnum;

class PublishedProductEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        return [
            'id' => $this->faker->modelId(),

            'name' => $this->faker->sentence(),
            'code' => $this->faker->unique()->slug(),
            'description' => $this->faker->nullable()->text(),
            'category_id' => $this->faker->modelId(),
            'brand_id' => $this->faker->modelId(),
            'external_id' => $this->faker->unique()->nullable()->numerify('######'),
            'type' => $this->faker->randomElement(ProductTypeEnum::getAllowableEnumValues()),
            'status_id' => $this->faker->modelId(),
            'status_comment' => $this->faker->nullable()->text(),
            'allow_publish' => $this->faker->boolean(),
            'main_image_file' => $this->faker->filePath(),

            'barcode' => $this->faker->nullable()->ean13(),
            'vendor_code' => $this->faker->numerify('###-###-###'),

            'weight' => $this->faker->nullable()->randomFloat(3, 1, 100),
            'weight_gross' => $this->faker->nullable()->randomFloat(3, 1, 100),
            'length' => $this->faker->nullable()->randomFloat(2, 1, 1000),
            'height' => $this->faker->nullable()->randomFloat(2, 1, 1000),
            'width' => $this->faker->nullable()->randomFloat(2, 1, 1000),
            'is_adult' => $this->faker->boolean(),

            'created_at' => $this->faker->date(self::DATE_TIME_FORMAT),
            'updated_at' => $this->faker->date(self::DATE_TIME_FORMAT),

        ];
    }

    public function forModel(Product $model): self
    {
        return $this->attributes([
            'id' => $model->product_id,

            'name' => $model->name,
            'code' => $model->code,
            'description' => $model->description,
            'brand_id' => $model->brand_id,
            'type' => $model->type,
            'allow_publish' => $model->allow_publish,
            'main_image_file' => $model->main_image_file,

            'barcode' => $model->barcode,
            'vendor_code' => $model->vendor_code,

            'weight' => $model->weight,
            'weight_gross' => $model->weight_gross,
            'length' => $model->length,
            'height' => $model->height,
            'width' => $model->width,
            'is_adult' => $model->is_adult,

            'created_at' => $model->created_at->format(self::DATE_TIME_FORMAT),
            'updated_at' => $model->updated_at->format(self::DATE_TIME_FORMAT),
        ]);
    }
}
