<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use Ensi\LaravelTestFactories\Factory;
use RdKafka\Message;

abstract class ModelEventMessageFactory extends Factory
{
    public const DATE_TIME_FORMAT = 'Y-m-d\TH:i:s.u\Z';

    protected array $attributes = [];
    protected ?string $event = null;

    protected function definition(): array
    {
        $event = $this->event ?: $this->faker->randomElement([
            ModelEventMessage::CREATE,
            ModelEventMessage::UPDATE,
            ModelEventMessage::DELETE,
        ]);

        $attributes = array_merge($this->definitionAttributes(), $this->attributes);

        return [
            'event' => $event,
            'attributes' => $attributes,
            'dirty' => $event == ModelEventMessage::UPDATE ? $this->faker->randomElements(array_keys($attributes)) : null,
        ];
    }

    abstract protected function definitionAttributes(): array;

    public function attributes(array $attributes): static
    {
        $this->attributes = array_merge($this->attributes, $attributes);

        return $this;
    }

    public function event(string $event): static
    {
        $this->event = $event;

        return $this;
    }

    public function make(array $extra = []): Message
    {
        $message = new Message();
        $message->payload = json_encode($this->makeArray($extra));

        return $message;
    }
}
