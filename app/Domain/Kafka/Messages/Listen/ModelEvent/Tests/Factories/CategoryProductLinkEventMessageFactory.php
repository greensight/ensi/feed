<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

class CategoryProductLinkEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        return [
            'id' => $this->faker->unique()->modelId(),
            'product_id' => $this->faker->modelId(),
            'category_id' => $this->faker->modelId(),
            'created_at' => $this->faker->date(self::DATE_TIME_FORMAT),
            'updated_at' => $this->faker->date(self::DATE_TIME_FORMAT),
        ];
    }
}
