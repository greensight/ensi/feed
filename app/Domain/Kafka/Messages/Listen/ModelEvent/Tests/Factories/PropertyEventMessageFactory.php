<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

use App\Domain\Offers\Models\Property;
use Ensi\PimClient\Dto\PropertyTypeEnum;

class PropertyEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        return [
            'id' => $this->faker->modelId(),

            'name' => $this->faker->word(),
            'display_name' => $this->faker->word(),
            'code' => $this->faker->unique()->slug(2),
            'type' => $this->faker->randomElement(PropertyTypeEnum::getAllowableEnumValues()),
            'is_multiple' => $this->faker->boolean(),
            'is_filterable' => $this->faker->boolean(),
            'is_active' => $this->faker->boolean(),
            'hint_value' => $this->faker->sentence(),
            'hint_value_name' => $this->faker->sentence(),
            'is_common' => $this->faker->boolean(),
            'is_required' => $this->faker->boolean(),
            'is_system' => $this->faker->boolean(),
            'has_directory' => $this->faker->boolean(),
            'is_moderated' => $this->faker->boolean(),
            'is_public' => $this->faker->boolean(),

            'created_at' => $this->faker->date(self::DATE_TIME_FORMAT),
            'updated_at' => $this->faker->date(self::DATE_TIME_FORMAT),
        ];
    }

    public function forModel(Property $model): static
    {
        return $this->attributes([
            'id' => $model->property_id,

            'display_name' => $model->name,
            'code' => $model->code,
            'type' => $model->type,
            'is_active' => $model->is_active,
            'is_public' => $model->is_public,

            'created_at' => $model->created_at->format(self::DATE_TIME_FORMAT),
            'updated_at' => $model->updated_at->format(self::DATE_TIME_FORMAT),
        ]);
    }
}
