<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

class StockEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        return [
            'id' => $this->faker->modelId(),

            'store_id' => $this->faker->modelId(),
            'offer_id' => $this->faker->modelId(),

            'qty' => $this->faker->nullable()->randomFloat(0, 1, 99),

            'created_at' => $this->faker->date(self::DATE_TIME_FORMAT),
            'updated_at' => $this->faker->date(self::DATE_TIME_FORMAT),
        ];
    }
}
