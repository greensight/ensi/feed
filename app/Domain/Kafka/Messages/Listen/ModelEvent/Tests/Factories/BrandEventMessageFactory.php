<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

use App\Domain\Offers\Models\Brand;

class BrandEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        $logoFile = $this->faker->nullable()->filePath();

        return [
            'id' => $this->faker->modelId(),
            'name' => $this->faker->sentence(),
            'code' => $this->faker->slug(),
            'description' => $this->faker->nullable()->sentence(),
            'is_active' => $this->faker->nullable()->boolean(),
            'logo_file' => $logoFile,
            'logo_url' => $this->faker->nullable($logoFile ? 1 : 0)->imageUrl,
            'created_at' => $this->faker->date(self::DATE_TIME_FORMAT),
            'updated_at' => $this->faker->date(self::DATE_TIME_FORMAT),
        ];
    }

    public function forModel(Brand $model): static
    {
        return $this->attributes([
            'id' => $model->brand_id,
            'name' => $model->name,
            'code' => $model->code,
            'description' => $model->description,
            'logo_file' => $model->logo_file,
            'logo_url' => $model->logo_url,
            'created_at' => $model->created_at->format(self::DATE_TIME_FORMAT),
            'updated_at' => $model->updated_at->format(self::DATE_TIME_FORMAT),
        ]);
    }
}
