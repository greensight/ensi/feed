<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

use App\Domain\Offers\Models\Offer;

class OfferEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
            'price' => $this->faker->nullable()->numberBetween(10, 100_000),
            'allow_publish' => $this->faker->boolean(),
            'is_active' => $this->faker->boolean(),
            'active_comment' => $this->faker->nullable()->word(),
            'created_at' => $this->faker->date(self::DATE_TIME_FORMAT),
            'updated_at' => $this->faker->date(self::DATE_TIME_FORMAT),
        ];
    }

    public function forModel(Offer $model): static
    {
        return $this->attributes([
            'id' => $model->offer_id,
            'product_id' => $model->product_id,
            'price' => $model->base_price,
            'created_at' => $model->created_at->format(self::DATE_TIME_FORMAT),
            'updated_at' => $model->updated_at->format(self::DATE_TIME_FORMAT),
        ]);
    }
}
