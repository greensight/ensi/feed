<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

use App\Domain\Offers\Models\Category;

class CategoryEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'name' => $this->faker->word(),
            'code' => $this->faker->unique()->regexify('[A-Za-z0-9_]{30}'),
            'parent_id' => $this->faker->nullable()->modelId(),
            'is_active' => $this->faker->boolean(),
            'is_inherits_properties' => $this->faker->boolean(),
            'is_real_active' => $this->faker->boolean(),
            'full_code' => null,
            'actualized_at' => $this->faker->nullable()->date(self::DATE_TIME_FORMAT),
            'created_at' => $this->faker->date(self::DATE_TIME_FORMAT),
            'updated_at' => $this->faker->date(self::DATE_TIME_FORMAT),
        ];
    }

    public function forModel(Category $model): static
    {
        return $this->attributes([
            'id' => $model->category_id,
            'name' => $model->name,
            'code' => $model->code,
            'parent_id' => $model->parent_id,
            'is_active' => $model->is_active,
            'is_real_active' => $model->is_real_active,
            'created_at' => $model->created_at->format(self::DATE_TIME_FORMAT),
            'updated_at' => $model->updated_at->format(self::DATE_TIME_FORMAT),
        ]);
    }
}
