<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Offer;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\OfferEventMessageFactory;

class OfferEventMessage extends ModelEventMessage
{
    public OfferPayload $attributes;

    public function setAttributes(array $attributes): void
    {
        $this->attributes = new OfferPayload($attributes);
    }

    public static function factory(): OfferEventMessageFactory
    {
        return OfferEventMessageFactory::new();
    }
}
