<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Offer;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;
use Carbon\CarbonInterface;

/**
 * @property int $id
 *
 * @property int $product_id - id товара
 * @property bool $allow_publish  - Признак активности для витрины
 * @property int|null $price - Цена оффера
 * @property bool $is_active - Системная активность оффера
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 */
class OfferPayload extends Payload
{
}
