<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Property;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\PropertyEventMessageFactory;

class PropertyEventMessage extends ModelEventMessage
{
    public PropertyPayload $attributes;

    public function setAttributes(array $attributes): void
    {
        $this->attributes = new PropertyPayload($attributes);
    }

    public static function factory(): PropertyEventMessageFactory
    {
        return PropertyEventMessageFactory::new();
    }
}
