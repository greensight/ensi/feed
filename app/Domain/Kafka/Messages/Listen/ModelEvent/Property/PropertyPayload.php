<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Property;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;
use Carbon\CarbonInterface;

/**
 * @property int $id
 *
 * @property string $name Название в системе
 * @property string $display_name Название на витрине
 * @property string $code Код атрибута
 * @property string $type Тип (string, integer, double, datetime, color)
 * @property string $hint_value Подсказка при заполнении для значения
 * @property string $hint_value_name Подсказка при заполнении для названия значения
 *
 * @property bool $is_active Активность свойства
 * @property bool $is_multiple Поддерживает множественные значения
 * @property bool $is_filterable Показывать в фильтре на витрине
 * @property bool $is_system Признак системного атрибута
 * @property bool $is_public Отображать на витрине
 * @property bool $is_common Общий для всех товаров, привязывается автоматически ко всем категориям
 * @property bool $is_required Обязательность для общего атрибута
 * @property bool $has_directory Признак наличия справочника значений
 * @property bool $is_moderated Признак модерируемого атрибута
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 */
class PropertyPayload extends Payload
{
}
