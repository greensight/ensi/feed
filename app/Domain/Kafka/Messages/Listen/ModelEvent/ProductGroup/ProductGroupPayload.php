<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\ProductGroup;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;
use Carbon\CarbonInterface;

/**
 * @property int $id
 *
 * @property int $category_id Id категории
 * @property string $name Название
 * @property int|null $main_product_id ID главного товара
 * @property bool $is_active Активность склейки
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 */
class ProductGroupPayload extends Payload
{
}
