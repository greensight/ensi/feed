<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Stock;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\StockEventMessageFactory;

class StockEventMessage extends ModelEventMessage
{
    public StockPayload $attributes;

    public function setAttributes(array $attributes): void
    {
        $this->attributes = new StockPayload($attributes);
    }

    public static function factory(): StockEventMessageFactory
    {
        return StockEventMessageFactory::new();
    }
}
