<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Stock;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;
use Carbon\CarbonInterface;

/**
 * @property int $id
 *
 * @property int $store_id - id склада
 * @property int $offer_id - id оффера
 *
 * @property float|null $qty - кол-во товара
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 */
class StockPayload extends Payload
{
}
