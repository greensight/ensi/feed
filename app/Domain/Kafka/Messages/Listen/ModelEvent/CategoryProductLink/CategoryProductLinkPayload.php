<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\CategoryProductLink;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;
use Carbon\CarbonInterface;

/**
 * @property int $id
 *
 * @property int $category_id Category ID from PIM
 * @property int $product_id Product ID from PIM
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 */
class CategoryProductLinkPayload extends Payload
{
}
