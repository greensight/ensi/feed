<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\CategoryProductLink;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\CategoryProductLinkEventMessageFactory;

class CategoryProductLinkEventMessage extends ModelEventMessage
{
    public CategoryProductLinkPayload $attributes;

    public function setAttributes(array $attributes): void
    {
        $this->attributes = new CategoryProductLinkPayload($attributes);
    }

    public static function factory(): CategoryProductLinkEventMessageFactory
    {
        return CategoryProductLinkEventMessageFactory::new();
    }
}
