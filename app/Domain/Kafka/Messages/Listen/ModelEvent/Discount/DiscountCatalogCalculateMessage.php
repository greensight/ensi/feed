<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Discount;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\DiscountCatalogCalculateMessageFactory;
use RdKafka\Message;

class DiscountCatalogCalculateMessage
{
    public function __construct(
        public array $productIds = [],
    ) {
    }

    public static function makeFromRdKafka(Message $message): static
    {
        $payload = json_decode($message->payload, true);

        return new static(
            productIds: $payload['product_ids'],
        );
    }

    public static function factory(): DiscountCatalogCalculateMessageFactory
    {
        return DiscountCatalogCalculateMessageFactory::new();
    }
}
