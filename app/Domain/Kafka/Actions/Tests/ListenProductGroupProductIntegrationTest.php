<?php

use App\Domain\Kafka\Actions\Listen\ListenProductGroupProductAction;
use App\Domain\Kafka\Actions\Tests\Factories\ProductLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ProductGroupProduct\ProductGroupProductEventMessage;
use App\Domain\Offers\Models\ProductGroupProduct;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelMissing;
use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-product-group-product');

test("Action ListenProductGroupProductAction create success", function (?bool $always) {
    /** @var IntegrationTestCase $this */
    FakerProvider::$optionalAlways = $always;

    $productGroupProductId = 1;
    $productId = 2;
    $message = ProductGroupProductEventMessage::factory()
        ->attributes(['id' => $productGroupProductId, 'product_id' => $productId])
        ->event(ModelEventMessage::CREATE)
        ->make();

    // Создаём товар, который связан с этой моделью и должен обновиться cloud_fields_updated_at
    $productMark = ProductLinkedFactory::createFromProduct($productId);
    // Создаём товар, который не связан с этой моделью, чтобы проверить что лишнее не помечается
    $productOther = ProductLinkedFactory::createFromProduct($productId + 1);

    resolve(ListenProductGroupProductAction::class)->execute($message);

    assertDatabaseHas(ProductGroupProduct::class, ['product_group_product_id' => $productGroupProductId]);
    assertNewModelFieldGreaterThan($productMark, 'cloud_fields_updated_at');
    assertNewModelFieldEquals($productOther, 'cloud_fields_updated_at');
})->with(FakerProvider::$optionalDataset);

test("Action ListenProductGroupProductAction update success", function (string $updatedField, mixed $newValue, bool $isUpdated, bool $isCloudUpdated) {
    /** @var IntegrationTestCase $this */

    /** @var ProductGroupProduct $productGroupProduct */
    $productGroupProduct = ProductGroupProduct::factory()->create();
    $updatedAt = $productGroupProduct->updated_at->addDay();
    $message = ProductGroupProductEventMessage::factory()
        ->forModel($productGroupProduct)
        ->attributes([$updatedField => $newValue, 'updated_at' => $updatedAt->toJSON()])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => [$updatedField]]);

    // Создаём товар, который связан с этой моделью и должен обновиться cloud_fields_updated_at
    $productMark = ProductLinkedFactory::createFromProduct($productGroupProduct->product_id);
    $productMarkNew = null;
    if ($updatedField == 'product_id') {
        $productMarkNew = ProductLinkedFactory::createFromProduct($newValue);
    }
    // Создаём товар, который не связан с этой моделью, чтобы проверить что лишнее не помечается
    $productOther = ProductLinkedFactory::createFromProduct($productGroupProduct->product_id + 1);

    resolve(ListenProductGroupProductAction::class)->execute($message);

    $needUpdatedAt = $isUpdated ? $updatedAt : $productGroupProduct->updated_at;
    $needField = $isUpdated ? $newValue : $productGroupProduct->getAttribute($updatedField);
    $productGroupProduct->refresh();
    assertEquals($needUpdatedAt, $productGroupProduct->updated_at);
    assertEquals($needField, $productGroupProduct->getAttribute($updatedField));

    // Проверка аналогична созданию, но если сохранения не происходит, то и пометки быть не должно
    if ($isCloudUpdated) {
        assertNewModelFieldGreaterThan($productMark, 'cloud_fields_updated_at');
        if ($productMarkNew) {
            assertNewModelFieldGreaterThan($productMarkNew, 'cloud_fields_updated_at');
        }
    } else {
        assertNewModelFieldEquals($productMark, 'cloud_fields_updated_at');
    }
    assertNewModelFieldEquals($productOther, 'cloud_fields_updated_at');
})->with([
    ['product_group_id', 33, true, true],
    ['product_id', 33, true, true],
    ['other_field', 'value', false, false],
]);

test("Action ListenProductGroupProductAction delete success", function () {
    /** @var IntegrationTestCase $this */

    /** @var ProductGroupProduct $productGroupProduct */
    $productGroupProduct = ProductGroupProduct::factory()->create();
    $message = ProductGroupProductEventMessage::factory()
        ->forModel($productGroupProduct)
        ->event(ModelEventMessage::DELETE)
        ->make();

    // Создаём товар, который связан с этой моделью
    $product = ProductLinkedFactory::createFromProduct($productGroupProduct->product_id);

    resolve(ListenProductGroupProductAction::class)->execute($message);

    assertModelMissing($productGroupProduct);
    // Удаление произошло, и пометка есть
    assertNewModelFieldGreaterThan($product, 'cloud_fields_updated_at');
});
