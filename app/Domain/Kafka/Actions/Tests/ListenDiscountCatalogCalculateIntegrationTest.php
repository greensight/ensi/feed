<?php

use App\Domain\Discounts\Data\DiscountOfferIdentify;
use App\Domain\Discounts\Events\CalculateDiscount;
use App\Domain\Kafka\Actions\Listen\ListenDiscountCatalogCalculateAction;
use App\Domain\Kafka\Actions\Tests\Factories\ProductLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Discount\DiscountCatalogCalculateMessage;
use App\Domain\Offers\Models\Offer;
use Illuminate\Database\Eloquent\Collection;

use function Pest\Laravel\assertDatabaseHas;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-discount-catalog-calculate');

test("Action ListenDiscountCatalogCalculateAction success", function () {
    Event::fake([
        CalculateDiscount::class,
    ]);

    /** @var Collection<Offer> $offers */
    $offers = Offer::factory()->count(5)
        ->withBasePrice()
        ->sequence(
            ['price' => null],
            ['price' => 10_00],
        )
        ->create();

    $markProducts = $otherProducts = [];
    foreach ($offers as $offer) {
        $product = ProductLinkedFactory::createFromProduct($offer->product_id);
        if (!is_null($offer->price)) {
            $markProducts[] = $product;
        } else {
            $otherProducts[] = $product;
        }
    }

    Offer::factory()->withBasePrice(with: false)->create();
    $otherProducts[] = ProductLinkedFactory::createFromProduct();

    $message = DiscountCatalogCalculateMessage::factory()->make([
        'product_ids' => $offers->pluck('product_id')->all(),
    ]);

    resolve(ListenDiscountCatalogCalculateAction::class)->execute($message);

    foreach ($offers as $offer) {
        assertDatabaseHas(Offer::class, ['id' => $offer->id, 'price' => null]);
    }

    foreach ($markProducts as $productMark) {
        assertNewModelFieldGreaterThan($productMark, 'cloud_fields_updated_at');
    }
    foreach ($otherProducts as $productOther) {
        assertNewModelFieldEquals($productOther, 'cloud_fields_updated_at');
    }

    CalculateDiscount::assertDispatched(new CalculateDiscount(new DiscountOfferIdentify(
        offerIds: $offers->pluck('offer_id')->all(),
    )));
});
