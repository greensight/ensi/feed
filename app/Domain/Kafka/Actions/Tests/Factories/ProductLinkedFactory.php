<?php

namespace App\Domain\Kafka\Actions\Tests\Factories;

use App\Domain\Offers\Models\CategoryProductLink;
use App\Domain\Offers\Models\Product;
use App\Domain\Offers\Models\ProductGroupProduct;
use App\Domain\Offers\Models\ProductPropertyValue;

class ProductLinkedFactory
{
    protected static function create(array $extra = []): Product
    {
        return Product::factory()->create(array_merge($extra, [
            'cloud_fields_updated_at' => now()->subDay(),
            'updated_at' => now()->subDay(),
        ]));
    }

    public static function createFromProduct(?int $productId = null): Product
    {
        return static::create($productId ? ['product_id' => $productId] : []);
    }

    public static function createFromBrand(int $brandId): Product
    {
        return static::create(['brand_id' => $brandId]);
    }

    public static function createFromCategory(int $categoryId): Product
    {
        $link = CategoryProductLink::factory()->create(['category_id' => $categoryId]);

        return static::createFromProduct($link->product_id);
    }

    public static function createFromDirectoryValue(int $directoryValueId): Product
    {
        $productPropertyValue = ProductPropertyValue::factory()->create([
            'directory_value_id' => $directoryValueId,
        ]);

        return static::createFromProduct($productPropertyValue->product_id);
    }

    public static function createFromProperty(int $propertyId): Product
    {
        $productPropertyValue = ProductPropertyValue::factory()->create([
            'property_id' => $propertyId,
        ]);

        return static::createFromProduct($productPropertyValue->product_id);
    }

    public static function createFromProductGroup(int $groupId, ?int $productId = null): Product
    {
        $extra = [];
        if ($productId) {
            $extra['product_id'] = $productId;
        }
        $productGroupProduct = ProductGroupProduct::factory()->create(array_merge($extra, [
            'product_group_id' => $groupId,
        ]));

        return static::createFromProduct($productGroupProduct->product_id);
    }
}
