<?php

use App\Domain\Discounts\Events\CalculateDiscount;
use App\Domain\Kafka\Actions\Listen\ListenOfferAction;
use App\Domain\Kafka\Actions\Tests\Factories\ProductLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Offer\OfferEventMessage;
use App\Domain\Offers\Models\Offer;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelMissing;
use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-offer');

test("Action ListenOfferAction create success", function (?bool $always) {
    /** @var IntegrationTestCase $this */
    FakerProvider::$optionalAlways = $always;
    Event::fake([
        CalculateDiscount::class,
    ]);

    /** @var IntegrationTestCase $this */
    $offerId = 1;
    $productId = 2;
    $message = OfferEventMessage::factory()
        ->attributes(['id' => $offerId, 'product_id' => $productId])
        ->event(ModelEventMessage::CREATE)
        ->make();

    // Создаём товар, который связан с этой моделью и должен обновиться cloud_fields_updated_at
    $productMark = ProductLinkedFactory::createFromProduct($productId);
    // Создаём товар, который не связан с этой моделью, чтобы проверить что лишнее не помечается
    $productOther = ProductLinkedFactory::createFromBrand($productId + 1);

    resolve(ListenOfferAction::class)->execute($message);

    assertDatabaseHas(Offer::class, ['offer_id' => $offerId]);
    assertNewModelFieldGreaterThan($productMark, 'cloud_fields_updated_at');
    assertNewModelFieldEquals($productOther, 'cloud_fields_updated_at');
})->with(FakerProvider::$optionalDataset);

test("Action ListenOfferAction update success", function (string $updatedField, string $modelField, mixed $newValue, bool $isUpdated, bool $isCloudUpdated) {
    /** @var IntegrationTestCase $this */
    Event::fake([
        CalculateDiscount::class,
    ]);

    /** @var Offer $offer */
    $offer = Offer::factory()->create();
    $updatedAt = $offer->updated_at->addDay();
    $message = OfferEventMessage::factory()
        ->forModel($offer)
        ->attributes([$updatedField => $newValue, 'updated_at' => $updatedAt->toJSON()])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => [$updatedField]]);

    // Создаём товар, который связан с этой моделью и должен обновиться cloud_fields_updated_at
    $productMark = ProductLinkedFactory::createFromProduct($offer->product_id);
    // Создаём товар, который не связан с этой моделью, чтобы проверить что лишнее не помечается
    $productOther = ProductLinkedFactory::createFromProduct($offer->product_id + 1);

    resolve(ListenOfferAction::class)->execute($message);

    $needUpdatedAt = $isUpdated ? $updatedAt : $offer->updated_at;
    $needField = $isUpdated ? $newValue : $offer->getAttribute($modelField);
    $offer->refresh();
    assertEquals($needUpdatedAt, $offer->updated_at);
    assertEquals($needField, $offer->getAttribute($modelField));

    // Проверка аналогична созданию, но если сохранения не происходит, то и пометки быть не должно
    if ($isCloudUpdated) {
        assertNewModelFieldGreaterThan($productMark, 'cloud_fields_updated_at');
    } else {
        assertNewModelFieldEquals($productMark, 'cloud_fields_updated_at');
    }
    assertNewModelFieldEquals($productOther, 'cloud_fields_updated_at');
})->with([
    ['price', 'base_price', 1000, true, true],
    ['other_field', 'other_field', 'value', false, false],
]);

test("Action ListenOfferAction delete success", function () {
    /** @var IntegrationTestCase $this */

    /** @var Offer $offer */
    $offer = Offer::factory()->create();
    $message = OfferEventMessage::factory()
        ->forModel($offer)
        ->event(ModelEventMessage::DELETE)
        ->make();

    // Создаём товар, который связан с этой моделью
    $product = ProductLinkedFactory::createFromProduct($offer->product_id);

    resolve(ListenOfferAction::class)->execute($message);

    assertModelMissing($offer);
    // Удаление произошло, и пометка есть
    assertNewModelFieldGreaterThan($product, 'cloud_fields_updated_at');
});
