<?php

use App\Domain\Kafka\Actions\Listen\ListenStockAction;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Stock\StockEventMessage;
use App\Domain\Offers\Models\Stock;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelMissing;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-stock');

test("Action ListenStockAction create success", function (?bool $always) {
    /** @var IntegrationTestCase $this */
    FakerProvider::$optionalAlways = $always;

    $stockId = 1;
    $message = StockEventMessage::factory()
        ->attributes(['id' => $stockId])
        ->event(ModelEventMessage::CREATE)
        ->make();

    resolve(ListenStockAction::class)->execute($message);

    assertDatabaseHas(Stock::class, ['stock_id' => $stockId]);
})->with(FakerProvider::$optionalDataset);

test("Action ListenStockAction update success", function (string $updatedField, mixed $newValue, ?bool $always) {
    /** @var IntegrationTestCase $this */
    FakerProvider::$optionalAlways = $always;

    /** @var Stock $stock */
    $stock = Stock::factory()->create();
    $message = StockEventMessage::factory()
        ->attributes(['id' => $stock->stock_id, $updatedField => $newValue])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => [$updatedField]]);

    resolve(ListenStockAction::class)->execute($message);

    assertDatabaseHas(Stock::class, [
        'stock_id' => $stock->stock_id,
        $updatedField => $newValue,
    ]);
})->with([
    ['store_id', 33],
    ['offer_id', 33],
    ['qty', 11.11],
], FakerProvider::$optionalDataset);

test("Action ListenStockAction delete success", function () {
    /** @var IntegrationTestCase $this */

    /** @var Stock $stock */
    $stock = Stock::factory()->create();
    $message = StockEventMessage::factory()
        ->attributes(['id' => $stock->stock_id])
        ->event(ModelEventMessage::DELETE)
        ->make();

    resolve(ListenStockAction::class)->execute($message);

    assertModelMissing($stock);
});
