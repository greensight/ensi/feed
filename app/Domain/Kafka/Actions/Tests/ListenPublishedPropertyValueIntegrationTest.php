<?php

use App\Domain\Kafka\Actions\Listen\ListenPublishedPropertyValueAction;
use App\Domain\Kafka\Actions\Tests\Factories\ProductLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\PublishedPropertyValue\PublishedPropertyValueEventMessage;
use App\Domain\Offers\Models\ProductPropertyValue;
use Ensi\LaravelTestFactories\FakerProvider;
use Ensi\PimClient\Dto\PropertyTypeEnum;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelMissing;
use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-published-property-value');

test("Action ListenPublishedPropertyValueAction create success", function (?bool $always) {
    /** @var IntegrationTestCase $this */
    FakerProvider::$optionalAlways = $always;

    $propertyValueId = 1;
    $productId = 2;
    $message = PublishedPropertyValueEventMessage::factory()
        ->attributes(['id' => $propertyValueId, 'product_id' => $productId, 'deleted_at' => null])
        ->event(ModelEventMessage::CREATE)
        ->make();

    // Создаём товар, который связан с этой моделью и должен обновиться cloud_fields_updated_at
    $productMark = ProductLinkedFactory::createFromProduct($productId);
    // Создаём товар, который не связан с этой моделью, чтобы проверить что лишнее не помечается
    $productOther = ProductLinkedFactory::createFromProduct($productId + 1);

    resolve(ListenPublishedPropertyValueAction::class)->execute($message);

    assertDatabaseHas(ProductPropertyValue::class, ['product_property_value_id' => $propertyValueId]);
    assertNewModelFieldGreaterThan($productMark, 'cloud_fields_updated_at');
    assertNewModelFieldEquals($productOther, 'cloud_fields_updated_at');
})->with(FakerProvider::$optionalDataset);

test("Action ListenPublishedPropertyValueAction update success", function (string $updatedField, mixed $newValue, bool $isUpdated, bool $isCloudUpdated) {
    /** @var IntegrationTestCase $this */

    /** @var ProductPropertyValue $propertyValue */
    $propertyValue = ProductPropertyValue::factory()->create([
        'type' => PropertyTypeEnum::BOOLEAN,
    ]);
    $updatedAt = $propertyValue->updated_at->addDay();
    $message = PublishedPropertyValueEventMessage::factory()
        ->forModel($propertyValue)
        ->attributes([$updatedField => $newValue, 'updated_at' => $updatedAt->toJSON(), 'deleted_at' => null])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => [$updatedField]]);

    // Создаём товар, который связан с этой моделью и должен обновиться cloud_fields_updated_at
    $productMark = ProductLinkedFactory::createFromProduct($propertyValue->product_id);
    $productMarkNew = null;
    if ($updatedField == 'product_id') {
        $productMarkNew = ProductLinkedFactory::createFromProduct($newValue);
    }
    // Создаём товар, который не связан с этой моделью, чтобы проверить что лишнее не помечается
    $productOther = ProductLinkedFactory::createFromProduct($propertyValue->product_id + 1);

    resolve(ListenPublishedPropertyValueAction::class)->execute($message);

    $needUpdatedAt = $isUpdated ? $updatedAt : $propertyValue->updated_at;
    $needField = $isUpdated ? $newValue : $propertyValue->getAttribute($updatedField);
    $propertyValue->refresh();
    assertEquals($needUpdatedAt, $propertyValue->updated_at);
    assertEquals($needField, $propertyValue->getAttribute($updatedField));

    // Проверка аналогична созданию, но если сохранения не происходит, то и пометки быть не должно
    if ($isCloudUpdated) {
        assertNewModelFieldGreaterThan($productMark, 'cloud_fields_updated_at');
        if ($productMarkNew) {
            assertNewModelFieldGreaterThan($productMarkNew, 'cloud_fields_updated_at');
        }
    } else {
        assertNewModelFieldEquals($productMark, 'cloud_fields_updated_at');
    }
    assertNewModelFieldEquals($productOther, 'cloud_fields_updated_at');
})->with([
    ['product_id', 33, true, true],
    ['property_id', 33, true, true],
    ['directory_value_id', 33, true, true],
    ['type', 'string', true, true],
    ['value', 'Новое значение', true, true],
    ['name', 'Новое имя', true, false],
    ['other_field', 'value', false, false],
]);

test("Action ListenPublishedPropertyValueAction delete success", function () {
    /** @var IntegrationTestCase $this */

    /** @var ProductPropertyValue $propertyValue */
    $propertyValue = ProductPropertyValue::factory()->create();
    $message = PublishedPropertyValueEventMessage::factory()
        ->attributes(['id' => $propertyValue->product_property_value_id])
        ->event(ModelEventMessage::DELETE)
        ->make();

    resolve(ListenPublishedPropertyValueAction::class)->execute($message);

    assertModelMissing($propertyValue);
});

test("Action ListenPublishedPropertyValueAction delete_at success", function () {
    /** @var IntegrationTestCase $this */

    /** @var ProductPropertyValue $propertyValue */
    $propertyValue = ProductPropertyValue::factory()->create();
    $message = PublishedPropertyValueEventMessage::factory()
        ->attributes([
            'id' => $propertyValue->product_property_value_id,
            'deleted_at' => now()->toJSON(),
        ])
        ->event(ModelEventMessage::UPDATE)
        ->make();

    // Создаём товар, который связан с этой моделью
    $product = ProductLinkedFactory::createFromProduct($propertyValue->product_id);

    resolve(ListenPublishedPropertyValueAction::class)->execute($message);

    assertModelMissing($propertyValue);
    // Удаление произошло, и пометка есть
    assertNewModelFieldGreaterThan($product, 'cloud_fields_updated_at');
});
