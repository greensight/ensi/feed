<?php

use App\Domain\Kafka\Actions\Listen\ListenPropertyAction;
use App\Domain\Kafka\Actions\Tests\Factories\ProductLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Property\PropertyEventMessage;
use App\Domain\Offers\Models\Property;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelMissing;
use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-property');

test("Action ListenPropertyAction create success", function (?bool $always) {
    /** @var IntegrationTestCase $this */
    FakerProvider::$optionalAlways = $always;

    $propertyId = 1;
    $message = PropertyEventMessage::factory()
        ->attributes(['id' => $propertyId])
        ->event(ModelEventMessage::CREATE)
        ->make();

    // Создаём товар, который связан с этой моделью и должен обновиться cloud_fields_updated_at
    $productMark = ProductLinkedFactory::createFromProperty($propertyId);
    // Создаём товар, который не связан с этой моделью, чтобы проверить что лишнее не помечается
    $productOther = ProductLinkedFactory::createFromProperty($propertyId + 1);

    resolve(ListenPropertyAction::class)->execute($message);

    assertDatabaseHas(Property::class, ['property_id' => $propertyId]);
    assertNewModelFieldGreaterThan($productMark, 'cloud_fields_updated_at');
    assertNewModelFieldEquals($productOther, 'cloud_fields_updated_at');
})->with(FakerProvider::$optionalDataset);

test("Action ListenPropertyAction update success", function (
    string $updatedField,
    mixed $newValue,
    bool $isUpdated,
    bool $isCloudUpdated,
    ?string $modelField = null,
) {
    /** @var IntegrationTestCase $this */

    /** @var Property $property */
    $property = Property::factory()->create(['is_public' => false, 'is_active' => false]);
    $updatedAt = $property->updated_at->addDay();
    $message = PropertyEventMessage::factory()
        ->forModel($property)
        ->attributes([$updatedField => $newValue, 'updated_at' => $updatedAt->toJSON()])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => [$updatedField]]);

    // Создаём товар, который связан с этой моделью и должен обновиться cloud_fields_updated_at
    $productMark = ProductLinkedFactory::createFromProperty($property->property_id);
    // Создаём товар, который не связан с этой моделью, чтобы проверить что лишнее не помечается
    $productOther = ProductLinkedFactory::createFromProperty($property->property_id + 1);

    resolve(ListenPropertyAction::class)->execute($message);

    $needUpdatedAt = $isUpdated ? $updatedAt : $property->updated_at;
    $modelField = $modelField ?? $updatedField;
    $needField = $isUpdated ? $newValue : $property->getAttribute($modelField);
    $property->refresh();
    assertEquals($needUpdatedAt, $property->updated_at);
    assertEquals($needField, $property->getAttribute($modelField));

    // Проверка аналогична созданию, но если сохранения не происходит, то и пометки быть не должно
    if ($isCloudUpdated) {
        assertNewModelFieldGreaterThan($productMark, 'cloud_fields_updated_at');
    } else {
        assertNewModelFieldEquals($productMark, 'cloud_fields_updated_at');
    }
    assertNewModelFieldEquals($productOther, 'cloud_fields_updated_at');
})->with([
    ['display_name', 'Новое имя', true, true, 'name'],
    ['code', 'new_code', true, false],
    ['type', 'string', true, false],
    ['is_public', true, true, true],
    ['is_active', true, true, true],
    ['other_field', 'value', false, false],
]);

test("Action ListenPropertyAction delete success", function () {
    /** @var IntegrationTestCase $this */

    /** @var Property $property */
    $property = Property::factory()->create();
    $message = PropertyEventMessage::factory()
        ->forModel($property)
        ->event(ModelEventMessage::DELETE)
        ->make();

    // Создаём товар, который связан с этой моделью и не должна обновиться cloud_fields_updated_at, т.к. при удалении не помечаем
    $product = ProductLinkedFactory::createFromProperty($property->property_id);

    resolve(ListenPropertyAction::class)->execute($message);

    assertModelMissing($property);
    // Удаление произошло, но пометки нет
    assertNewModelFieldEquals($product, 'cloud_fields_updated_at');
});
