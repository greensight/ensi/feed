<?php

use App\Domain\Kafka\Actions\Listen\ListenProductGroupAction;
use App\Domain\Kafka\Actions\Tests\Factories\ProductLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ProductGroup\ProductGroupEventMessage;
use App\Domain\Offers\Models\ProductGroup;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelMissing;
use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-product-group');

test("Action ListenProductGroupAction create success", function (?bool $always) {
    /** @var IntegrationTestCase $this */
    FakerProvider::$optionalAlways = $always;

    $productGroupId = 1;
    $message = ProductGroupEventMessage::factory()
        ->attributes(['id' => $productGroupId])
        ->event(ModelEventMessage::CREATE)
        ->make();

    // Создаём товар, который связан с этой моделью и должен обновиться cloud_fields_updated_at
    $productMark = ProductLinkedFactory::createFromProductGroup($productGroupId);
    // Создаём товар, который не связан с этой моделью, чтобы проверить что лишнее не помечается
    $productOther = ProductLinkedFactory::createFromProductGroup($productGroupId + 1);

    resolve(ListenProductGroupAction::class)->execute($message);

    assertDatabaseHas(ProductGroup::class, ['product_group_id' => $productGroupId]);
    assertNewModelFieldGreaterThan($productMark, 'cloud_fields_updated_at');
    assertNewModelFieldEquals($productOther, 'cloud_fields_updated_at');

})->with(FakerProvider::$optionalDataset);

test("Action ListenProductGroupAction update success", function (string $updatedField, mixed $newValue, bool $isUpdated, bool $isCloudUpdated) {
    /** @var IntegrationTestCase $this */

    /** @var ProductGroup $productGroup */
    $productGroup = ProductGroup::factory()->create(['is_active' => false]);
    $updatedAt = $productGroup->updated_at->addDay();
    $message = ProductGroupEventMessage::factory()
        ->forModel($productGroup)
        ->attributes([$updatedField => $newValue, 'updated_at' => $updatedAt->toJSON()])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => [$updatedField]]);

    // Создаём товар, который связан с этой моделью и должен обновиться cloud_fields_updated_at
    $productMark = ProductLinkedFactory::createFromProductGroup($productGroup->product_group_id, $productGroup->main_product_id);
    $productMarkNew = null;
    if ($updatedField == 'main_product_id') {
        $productMarkNew = ProductLinkedFactory::createFromProduct($newValue);
    }
    // Создаём товар, который не связан с этой моделью, чтобы проверить что лишнее не помечается
    $productOther = ProductLinkedFactory::createFromProductGroup($productGroup->product_group_id + 1);

    resolve(ListenProductGroupAction::class)->execute($message);

    $needUpdatedAt = $isUpdated ? $updatedAt : $productGroup->updated_at;
    $needField = $isUpdated ? $newValue : $productGroup->getAttribute($updatedField);
    $productGroup->refresh();
    assertEquals($needUpdatedAt, $productGroup->updated_at);
    assertEquals($needField, $productGroup->getAttribute($updatedField));

    // Проверка аналогична созданию, но если сохранения не происходит, то и пометки быть не должно
    if ($isCloudUpdated) {
        assertNewModelFieldGreaterThan($productMark, 'cloud_fields_updated_at');
        if ($productMarkNew) {
            assertNewModelFieldGreaterThan($productMarkNew, 'cloud_fields_updated_at');
        }
    } else {
        assertNewModelFieldEquals($productMark, 'cloud_fields_updated_at');
    }
    assertNewModelFieldEquals($productOther, 'cloud_fields_updated_at');
})->with([
    ['category_id', 33, true, false],
    ['name', 'Новое имя', true, false],
    ['main_product_id', 33, true, true],
    ['is_active', true, true, true],
    ['other_field', 'value', false, false],
]);

test("Action ListenProductGroupAction delete success", function () {
    /** @var IntegrationTestCase $this */

    /** @var ProductGroup $productGroup */
    $productGroup = ProductGroup::factory()->create();
    $message = ProductGroupEventMessage::factory()
        ->forModel($productGroup)
        ->event(ModelEventMessage::DELETE)
        ->make();

    // Создаём товар, который связан с этой моделью и не должна обновиться cloud_fields_updated_at, т.к. при удалении не помечаем
    $product = ProductLinkedFactory::createFromProductGroup($productGroup->product_group_id);

    resolve(ListenProductGroupAction::class)->execute($message);

    assertModelMissing($productGroup);
    // Удаление произошло, но пометки нет
    assertNewModelFieldEquals($product, 'cloud_fields_updated_at');
});
