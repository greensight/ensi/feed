<?php

namespace App\Domain\Kafka\Actions\Tests;

use App\Domain\Kafka\Actions\Listen\ListenCategoryProductLinkAction;
use App\Domain\Kafka\Actions\Tests\Factories\ProductLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\CategoryProductLink\CategoryProductLinkEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Offers\Models\CategoryProductLink;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka');

test("Action ListenCategoryProductLinkAction create success", function () {
    /** @var IntegrationTestCase $this */
    $categoryProductId = 1;
    $productId = 1;
    $message = CategoryProductLinkEventMessage::factory()
        ->attributes(['id' => $categoryProductId, 'product_id' => $productId])
        ->event(ModelEventMessage::CREATE)
        ->make();

    // Create product that's linked to category and must be marked
    $productMark = ProductLinkedFactory::createFromProduct($productId);
    // Create another product, to ensure it does not get marked
    $productOther = ProductLinkedFactory::createFromProduct($productId + 1);

    resolve(ListenCategoryProductLinkAction::class)->execute($message);

    assertDatabaseHas(CategoryProductLink::class, [
        'category_product_id' => $categoryProductId,
    ]);
    assertNewModelFieldGreaterThan($productMark, 'cloud_fields_updated_at');
    assertNewModelFieldEquals($productOther, 'cloud_fields_updated_at');
});

test("Action ListenCategoryProductLinkAction update success", function (array $dirty, bool $isDirty) {
    /** @var IntegrationTestCase $this */
    /** @var CategoryProductLink $link */
    $link = CategoryProductLink::factory()->create();
    $updatedAt = $link->updated_at->addDay();
    $message = CategoryProductLinkEventMessage::factory()
        ->attributes([
            'id' => $link->category_product_id,
            'product_id' => $link->product_id,
            'updated_at' => $updatedAt->toJSON(),
        ])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => $dirty]);

    // Create product that's linked to category and must be marked
    $productMark = ProductLinkedFactory::createFromProduct($link->product_id);
    // Create another product, to ensure it does not get marked
    $productOther = ProductLinkedFactory::createFromProduct($link->product_id + 1);

    resolve(ListenCategoryProductLinkAction::class)->execute($message);

    $needUpdatedAt = $isDirty ? $updatedAt : $link->updated_at;
    $link->refresh();
    assertEquals($needUpdatedAt, $link->updated_at);

    // If save wasn't triggered, product must not be marked
    if ($isDirty) {
        assertNewModelFieldGreaterThan($productMark, 'cloud_fields_updated_at');
    } else {
        assertNewModelFieldEquals($productMark, 'cloud_fields_updated_at');
    }
    assertNewModelFieldEquals($productOther, 'cloud_fields_updated_at');
})->with([
    [['product_id'], true],
    [['category_id'], true],
]);

test("Action ListenCategoryProductLinkAction delete success", function () {
    /** @var IntegrationTestCase $this */
    /** @var CategoryProductLink $link */
    $link = CategoryProductLink::factory()->create();
    $message = CategoryProductLinkEventMessage::factory()
        ->attributes([
            'id' => $link->category_product_id,
            'product_id' => $link->product_id,
        ])
        ->event(ModelEventMessage::DELETE)
        ->make();

    // Create product that's linked to category and must be marked
    $productMark = ProductLinkedFactory::createFromProduct($link->product_id);
    // Create another product, to ensure it does not get marked
    $productOther = ProductLinkedFactory::createFromProduct($link->product_id + 1);

    resolve(ListenCategoryProductLinkAction::class)->execute($message);

    assertDatabaseMissing(CategoryProductLink::class, ['category_product_id' => $link->category_product_id]);
    assertNewModelFieldGreaterThan($productMark, 'cloud_fields_updated_at');
    assertNewModelFieldEquals($productOther, 'cloud_fields_updated_at');
});
