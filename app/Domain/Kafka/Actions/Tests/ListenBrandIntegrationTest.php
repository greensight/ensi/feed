<?php

use App\Domain\Kafka\Actions\Listen\ListenBrandAction;
use App\Domain\Kafka\Actions\Tests\Factories\ProductLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Brand\BrandEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Offers\Models\Brand;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelMissing;
use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-brand');

test("Action ListenBrandAction create success", function (?bool $always) {
    /** @var IntegrationTestCase $this */
    FakerProvider::$optionalAlways = $always;

    $brandId = 1;
    $message = BrandEventMessage::factory()
        ->attributes(['id' => $brandId])
        ->event(ModelEventMessage::CREATE)
        ->make();

    // Создаём товар, который связан с этой моделью и должен обновиться cloud_fields_updated_at
    $productMark = ProductLinkedFactory::createFromBrand($brandId);
    // Создаём товар, который не связан с этой моделью, чтобы проверить что лишнее не помечается
    $productOther = ProductLinkedFactory::createFromBrand($brandId + 1);

    resolve(ListenBrandAction::class)->execute($message);

    assertDatabaseHas(Brand::class, ['brand_id' => $brandId]);
    assertNewModelFieldGreaterThan($productMark, 'cloud_fields_updated_at');
    assertNewModelFieldEquals($productOther, 'cloud_fields_updated_at');
})->with(FakerProvider::$optionalDataset);

test("Action ListenBrandAction update success", function (string $updatedField, mixed $newValue, bool $isUpdated, bool $isCloudUpdated) {
    /** @var IntegrationTestCase $this */

    /** @var Brand $brand */
    $brand = Brand::factory()->create();
    $updatedAt = $brand->updated_at->addDay();
    $message = BrandEventMessage::factory()
        ->forModel($brand)
        ->attributes([$updatedField => $newValue, 'updated_at' => $updatedAt->toJSON()])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => [$updatedField]]);

    // Создаём товар, который связан с этой моделью и должен обновиться cloud_fields_updated_at
    $productMark = ProductLinkedFactory::createFromBrand($brand->brand_id);
    // Создаём товар, который не связан с этой моделью, чтобы проверить что лишнее не помечается
    $productOther = ProductLinkedFactory::createFromBrand($brand->brand_id + 1);

    resolve(ListenBrandAction::class)->execute($message);

    $needUpdatedAt = $isUpdated ? $updatedAt : $brand->updated_at;
    $needField = $isUpdated ? $newValue : $brand->getAttribute($updatedField);
    $brand->refresh();
    assertEquals($needUpdatedAt, $brand->updated_at);
    assertEquals($needField, $brand->getAttribute($updatedField));

    // Проверка аналогична созданию, но если сохранения не происходит, то и пометки быть не должно
    if ($isCloudUpdated) {
        assertNewModelFieldGreaterThan($productMark, 'cloud_fields_updated_at');
    } else {
        assertNewModelFieldEquals($productMark, 'cloud_fields_updated_at');
    }
    assertNewModelFieldEquals($productOther, 'cloud_fields_updated_at');
})->with([
    ['name', 'Новое имя', true, true],
    ['code', 'new_code', true, false],
    ['description', 'Новое описание', true, false],
    ['logo_file', 'https://new-site.com', true, false],
    ['logo_url', 'https://new-site.com', true, false],
    ['other_field', 'value', false, false],
]);

test("Action ListenBrandAction delete success", function () {
    /** @var IntegrationTestCase $this */

    /** @var Brand $brand */
    $brand = Brand::factory()->create();
    $message = BrandEventMessage::factory()
        ->forModel($brand)
        ->event(ModelEventMessage::DELETE)
        ->make();

    // Создаём товар, который связан с этой моделью и не должна обновиться cloud_fields_updated_at, т.к. при удалении не помечаем
    $product = ProductLinkedFactory::createFromBrand($brand->brand_id);

    resolve(ListenBrandAction::class)->execute($message);

    assertModelMissing($brand);
    // Удаление произошло, но пометки нет
    assertNewModelFieldEquals($product, 'cloud_fields_updated_at');
});
