<?php

use App\Domain\Kafka\Actions\Listen\ListenPropertyDirectoryValueAction;
use App\Domain\Kafka\Actions\Tests\Factories\ProductLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\PropertyDirectoryValue\PropertyDirectoryValueEventMessage;
use App\Domain\Offers\Models\PropertyDirectoryValue;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelMissing;
use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-property-directory-value');

test("Action ListenPropertyDirectoryValueAction create success", function (?bool $always) {
    /** @var IntegrationTestCase $this */
    FakerProvider::$optionalAlways = $always;

    $propertyDirectoryValueId = 1;
    $message = PropertyDirectoryValueEventMessage::factory()
        ->attributes(['id' => $propertyDirectoryValueId])
        ->event(ModelEventMessage::CREATE)
        ->make();

    // Создаём товар, который связан с этой моделью и должен обновиться cloud_fields_updated_at
    $productMark = ProductLinkedFactory::createFromDirectoryValue($propertyDirectoryValueId);
    // Создаём товар, который не связан с этой моделью, чтобы проверить что лишнее не помечается
    $productOther = ProductLinkedFactory::createFromDirectoryValue($propertyDirectoryValueId + 1);

    resolve(ListenPropertyDirectoryValueAction::class)->execute($message);

    assertDatabaseHas(PropertyDirectoryValue::class, ['directory_value_id' => $propertyDirectoryValueId]);
    assertNewModelFieldGreaterThan($productMark, 'cloud_fields_updated_at');
    assertNewModelFieldEquals($productOther, 'cloud_fields_updated_at');
})->with(FakerProvider::$optionalDataset);

test("Action ListenPropertyDirectoryValueAction update success", function (string $updatedField, mixed $newValue, bool $isUpdated, bool $isCloudUpdated) {
    /** @var IntegrationTestCase $this */

    /** @var PropertyDirectoryValue $propertyDirectoryValue */
    $propertyDirectoryValue = PropertyDirectoryValue::factory()->create();
    $updatedAt = $propertyDirectoryValue->updated_at->addDay();
    $message = PropertyDirectoryValueEventMessage::factory()
        ->forModel($propertyDirectoryValue)
        ->attributes([$updatedField => $newValue, 'updated_at' => $updatedAt->toJSON()])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => [$updatedField]]);

    // Создаём товар, который связан с этой моделью и должен обновиться cloud_fields_updated_at
    $productMark = ProductLinkedFactory::createFromDirectoryValue($propertyDirectoryValue->directory_value_id);
    // Создаём товар, который не связан с этой моделью, чтобы проверить что лишнее не помечается
    $productOther = ProductLinkedFactory::createFromDirectoryValue($propertyDirectoryValue->directory_value_id + 1);

    resolve(ListenPropertyDirectoryValueAction::class)->execute($message);

    $needUpdatedAt = $isUpdated ? $updatedAt : $propertyDirectoryValue->updated_at;
    $needField = $isUpdated ? $newValue : $propertyDirectoryValue->getAttribute($updatedField);
    $propertyDirectoryValue->refresh();
    assertEquals($needUpdatedAt, $propertyDirectoryValue->updated_at);
    assertEquals($needField, $propertyDirectoryValue->getAttribute($updatedField));

    // Проверка аналогична созданию, но если сохранения не происходит, то и пометки быть не должно
    if ($isCloudUpdated) {
        assertNewModelFieldGreaterThan($productMark, 'cloud_fields_updated_at');
    } else {
        assertNewModelFieldEquals($productMark, 'cloud_fields_updated_at');
    }
    assertNewModelFieldEquals($productOther, 'cloud_fields_updated_at');
})->with([
    ['property_id', 33, true, false],
    ['name', 'Новое имя', true, false],
    ['code', 'new_code', true, false],
    ['value', 'new_value', true, true],
    ['type', 1, true, false],
    ['other_field', 'value', false, false],
]);

test("Action ListenPropertyDirectoryValueAction delete success", function () {
    /** @var IntegrationTestCase $this */

    /** @var PropertyDirectoryValue $propertyDirectoryValue */
    $propertyDirectoryValue = PropertyDirectoryValue::factory()->create();
    $message = PropertyDirectoryValueEventMessage::factory()
        ->forModel($propertyDirectoryValue)
        ->event(ModelEventMessage::DELETE)
        ->make();

    // Создаём товар, который связан с этой моделью и не должна обновиться cloud_fields_updated_at, т.к. при удалении не помечаем
    $product = ProductLinkedFactory::createFromDirectoryValue($propertyDirectoryValue->directory_value_id);

    resolve(ListenPropertyDirectoryValueAction::class)->execute($message);

    assertModelMissing($propertyDirectoryValue);
    // Удаление произошло, но пометки нет
    assertNewModelFieldEquals($product, 'cloud_fields_updated_at');
});
