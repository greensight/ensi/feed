<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Kafka\Messages\Listen\ModelEvent\PublishedProduct\PublishedProductEventMessage;
use App\Domain\Offers\Models\Product;
use RdKafka\Message;

class ListenPublishedProductAction
{
    public function __construct(
        protected SyncModelAction $syncModelAction,
    ) {
    }

    public function execute(Message $message): void
    {
        $eventMessage = PublishedProductEventMessage::makeFromRdKafka($message);
        $modelPayload = $eventMessage->attributes;

        $this->syncModelAction->execute(
            eventMessage: $eventMessage,
            interestingUpdatedFields: [
                'allow_publish',
                'main_image_file',
                'brand_id',
                'name',
                'code',
                'description',
                'type',
                'vendor_code',
                'barcode',
                'weight',
                'weight_gross',
                'width',
                'height',
                'length',
                'is_adult',
            ],
            findModel: fn () => Product::query()->where('product_id', $modelPayload->id)->first(),
            createModel: function () use ($modelPayload) {
                $model = new Product();
                $model->product_id = $modelPayload->id;
                $model->created_at = $modelPayload->created_at;

                return $model;
            },
            fillModel: function (Product $model) use ($modelPayload) {
                $model->allow_publish = $modelPayload->allow_publish;
                $model->main_image_file = $modelPayload->main_image_file;
                $model->brand_id = $modelPayload->brand_id;
                $model->name = $modelPayload->name;
                $model->code = $modelPayload->code;
                $model->description = $modelPayload->description;
                $model->type = $modelPayload->type;
                $model->vendor_code = $modelPayload->vendor_code;
                $model->barcode = $modelPayload->barcode;
                $model->weight = $modelPayload->weight;
                $model->weight_gross = $modelPayload->weight_gross;
                $model->width = $modelPayload->width;
                $model->height = $modelPayload->height;
                $model->length = $modelPayload->length;
                $model->is_adult = $modelPayload->is_adult;
                $model->updated_at = $modelPayload->updated_at;
            },
        );
    }
}
