<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ProductGroupProduct\ProductGroupProductEventMessage;
use App\Domain\Offers\Models\ProductGroupProduct;
use RdKafka\Message;

class ListenProductGroupProductAction
{
    public function __construct(
        protected SyncModelAction $syncModelAction,
    ) {
    }

    public function execute(Message $message): void
    {
        $eventMessage = ProductGroupProductEventMessage::makeFromRdKafka($message);
        $modelPayload = $eventMessage->attributes;

        $this->syncModelAction->execute(
            eventMessage: $eventMessage,
            interestingUpdatedFields: [
                'product_group_id',
                'product_id',
            ],
            findModel: fn () => ProductGroupProduct::query()->where('product_group_product_id', $modelPayload->id)->first(),
            createModel: function () use ($modelPayload) {
                $model = new ProductGroupProduct();
                $model->product_group_product_id = $modelPayload->id;
                $model->created_at = $modelPayload->created_at;

                return $model;
            },
            fillModel: function (ProductGroupProduct $model) use ($modelPayload) {
                $model->product_group_id = $modelPayload->product_group_id;
                $model->product_id = $modelPayload->product_id;
                $model->updated_at = $modelPayload->updated_at;
            },
        );
    }
}
