<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Discounts\Data\DiscountOfferIdentify;
use App\Domain\Discounts\Events\CalculateDiscount;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Discount\DiscountCatalogCalculateMessage;
use App\Domain\Offers\Models\Offer;
use Illuminate\Database\Eloquent\Collection;
use RdKafka\Message;

class ListenDiscountCatalogCalculateAction
{
    public function execute(Message $message): void
    {
        $eventMessage = DiscountCatalogCalculateMessage::makeFromRdKafka($message);
        /** @var Collection<Offer> $offers */
        $offers = Offer::query()
            ->whereNotNull('base_price')
            ->whereIn('product_id', $eventMessage->productIds)
            ->get();

        foreach ($offers as $offer) {
            $offer->price = null;
            $offer->save();
        }

        CalculateDiscount::dispatch(
            new DiscountOfferIdentify(
                $offers->pluck('offer_id')->all()
            )
        );
    }
}
