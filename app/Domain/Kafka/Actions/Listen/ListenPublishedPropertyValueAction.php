<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Kafka\Messages\Listen\ModelEvent\PublishedPropertyValue\PublishedPropertyValueEventMessage;
use App\Domain\Offers\Models\ProductPropertyValue;
use RdKafka\Message;

class ListenPublishedPropertyValueAction
{
    public function __construct(
        protected SyncModelAction $syncModelAction,
    ) {
    }

    public function execute(Message $message): void
    {
        $eventMessage = PublishedPropertyValueEventMessage::makeFromRdKafka($message);
        $modelPayload = $eventMessage->attributes;

        $this->syncModelAction->execute(
            eventMessage: $eventMessage,
            interestingUpdatedFields: [
                'product_id',
                'property_id',
                'directory_value_id',
                'type',
                'value',
                'name',
                'deleted_at',
            ],
            findModel: fn () => ProductPropertyValue::query()->where('product_property_value_id', $modelPayload->id)->first(),
            createModel: function () use ($modelPayload) {
                $model = new ProductPropertyValue();
                $model->product_property_value_id = $modelPayload->id;
                $model->created_at = $modelPayload->created_at;

                return $model;
            },
            fillModel: function (ProductPropertyValue $model) use ($modelPayload) {
                $model->product_id = $modelPayload->product_id;
                $model->property_id = $modelPayload->property_id;
                $model->directory_value_id = $modelPayload->directory_value_id;
                $model->type = $modelPayload->type;
                $model->value = $modelPayload->value;
                $model->name = $modelPayload->name;
                $model->updated_at = $modelPayload->updated_at;
            },
            isToDelete: fn () => isset($modelPayload->deleted_at),
        );
    }
}
