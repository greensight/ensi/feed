<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Stock\StockEventMessage;
use App\Domain\Offers\Models\Stock;
use RdKafka\Message;

class ListenStockAction
{
    public function __construct(
        protected SyncModelAction $syncModelAction,
    ) {
    }

    public function execute(Message $message): void
    {
        $eventMessage = StockEventMessage::makeFromRdKafka($message);
        $modelPayload = $eventMessage->attributes;

        $this->syncModelAction->execute(
            eventMessage: $eventMessage,
            interestingUpdatedFields: [
                'store_id',
                'offer_id',
                'qty',
            ],
            findModel: fn () => Stock::query()->where('stock_id', $modelPayload->id)->first(),
            createModel: function () use ($modelPayload) {
                $model = new Stock();
                $model->stock_id = $modelPayload->id;
                $model->created_at = $modelPayload->created_at;

                return $model;
            },
            fillModel: function (Stock $model) use ($modelPayload) {
                $model->store_id = $modelPayload->store_id;
                $model->offer_id = $modelPayload->offer_id;
                $model->qty = $modelPayload->qty ?? 0;
                $model->updated_at = $modelPayload->updated_at;
            },
        );
    }
}
