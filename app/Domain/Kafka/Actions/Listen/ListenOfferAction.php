<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Offer\OfferEventMessage;
use App\Domain\Offers\Models\Offer;
use RdKafka\Message;

class ListenOfferAction
{
    public function __construct(
        protected SyncModelAction $syncModelAction,
    ) {
    }

    public function execute(Message $message): void
    {
        $eventMessage = OfferEventMessage::makeFromRdKafka($message);
        $modelPayload = $eventMessage->attributes;

        $this->syncModelAction->execute(
            eventMessage: $eventMessage,
            interestingUpdatedFields: [
                'price',
            ],
            findModel: fn () => Offer::query()->where('offer_id', $modelPayload->id)->first(),
            createModel: function () use ($modelPayload) {
                $model = new Offer();
                $model->offer_id = $modelPayload->id;
                $model->product_id = $modelPayload->product_id;
                $model->created_at = $modelPayload->created_at;

                return $model;
            },
            fillModel: function (Offer $model) use ($modelPayload) {
                $model->base_price = $modelPayload->price;
                $model->updated_at = $modelPayload->updated_at;
            },
        );
    }
}
