<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Brand\BrandEventMessage;
use App\Domain\Offers\Models\Brand;
use RdKafka\Message;

class ListenBrandAction
{
    public function __construct(
        protected SyncModelAction $syncModelAction,
    ) {
    }

    public function execute(Message $message): void
    {
        $eventMessage = BrandEventMessage::makeFromRdKafka($message);
        $modelPayload = $eventMessage->attributes;

        $this->syncModelAction->execute(
            eventMessage: $eventMessage,
            interestingUpdatedFields: [
                'name',
                'code',
                'description',
                'logo_file',
                'logo_url',
            ],
            findModel: fn () => Brand::query()->where('brand_id', $modelPayload->id)->first(),
            createModel: function () use ($modelPayload) {
                $model = new Brand();
                $model->brand_id = $modelPayload->id;
                $model->created_at = $modelPayload->created_at;

                return $model;
            },
            fillModel: function (Brand $model) use ($modelPayload) {
                $model->name = $modelPayload->name;
                $model->code = $modelPayload->code;
                $model->description = $modelPayload->description;
                $model->logo_file = $modelPayload->logo_file;
                $model->logo_url = $modelPayload->logo_url;
                $model->updated_at = $modelPayload->updated_at;
            },
        );
    }
}
