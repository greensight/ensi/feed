<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Kafka\Messages\Listen\ModelEvent\CategoryProductLink\CategoryProductLinkEventMessage;
use App\Domain\Offers\Models\CategoryProductLink;
use RdKafka\Message;

class ListenCategoryProductLinkAction
{
    public function __construct(
        protected SyncModelAction $syncModelAction,
    ) {
    }

    public function execute(Message $message): void
    {
        $eventMessage = CategoryProductLinkEventMessage::makeFromRdKafka($message);
        $modelPayload = $eventMessage->attributes;

        $this->syncModelAction->execute(
            eventMessage: $eventMessage,
            interestingUpdatedFields: [
                'category_id',
                'product_id',
            ],
            findModel: fn () => CategoryProductLink::query()->where('category_product_id', $modelPayload->id)->first(),
            createModel: function () use ($modelPayload) {
                $model = new CategoryProductLink();
                $model->category_product_id = $modelPayload->id;
                $model->created_at = $modelPayload->created_at;

                return $model;
            },
            fillModel: function (CategoryProductLink $model) use ($modelPayload) {
                $model->category_id = $modelPayload->category_id;
                $model->product_id = $modelPayload->product_id;
                $model->updated_at = $modelPayload->updated_at;
            },
        );
    }
}
