<?php

namespace App\Domain\Sitemap\Actions;

use App\Domain\Offers\Models\Category;
use App\Domain\Offers\Models\Product;
use App\Domain\Sitemap\Actions\Stages\GenerateCategoriesAction;
use App\Domain\Sitemap\Actions\Stages\GenerateImagesAction;
use App\Domain\Sitemap\Actions\Stages\GenerateIndexAction;
use App\Domain\Sitemap\Actions\Stages\GenerateInnerAction;
use App\Domain\Sitemap\Actions\Stages\GenerateProductsAction;
use App\Domain\Sitemap\Data\SitemapContext;
use Psr\Log\LoggerInterface;

class GenerateSitemapAction
{
    public LoggerInterface $logger;
    public SitemapContext $context;

    public function __construct(
        protected readonly GenerateIndexAction $generateIndexAction,
        protected readonly GenerateProductsAction $generateProductsAction,
        protected readonly GenerateCategoriesAction $generateCategoriesAction,
        protected readonly GenerateInnerAction $generateInnerAction,
        protected readonly GenerateImagesAction $generateImagesAction,
    ) {
        $this->context = new SitemapContext(
            baseDomain: $this->getBaseDomain(),
            directoryPath: $this->getDirectoryPath(),
            products: Product::query()->lazyById(),
            categories: Category::query()->lazyById(),
        );
    }

    public function execute(): void
    {
        $this->generateIndexAction->execute($this->context);
        $this->logger->info('Пройден шаг: генерация индекса');
        $this->generateProductsAction->execute($this->context);
        $this->logger->info('Пройден шаг: генерация данных по продукту');
        $this->generateCategoriesAction->execute($this->context);
        $this->logger->info('Пройден шаг: генерация данных по категории');
        $this->generateInnerAction->execute($this->context);
        $this->logger->info('Пройден шаг: генерация внутренних ссылок');
        $this->generateImagesAction->execute($this->context);
        $this->logger->info('Пройден шаг: генерация данных по продуктовым изображениям');
    }

    public function getBaseDomain(): string
    {
        return rtrim(config('app.frontend_domain'), '/');
    }

    public function getDirectoryPath(): string
    {
        $directoryPath = storage_path('sitemap/');
        if (!is_dir($directoryPath)) {
            mkdir($directoryPath);
        }

        return $directoryPath;
    }

    public function setLogger(LoggerInterface $logger): self
    {
        $this->logger = $logger;

        return $this;
    }
}
