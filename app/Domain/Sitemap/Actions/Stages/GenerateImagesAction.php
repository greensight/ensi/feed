<?php

namespace App\Domain\Sitemap\Actions\Stages;

use App\Domain\Offers\Models\Product;
use App\Domain\Sitemap\Data\SitemapContext;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;

class GenerateImagesAction extends BaseStageAction
{
    public function execute(SitemapContext $context): void
    {
        $this->context = $context;
        $this->sitemap = $this->getBaseSitemapXmlElement(
            'urlset',
            'xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"'
        );

        /** @var Product $product */
        foreach ($context->products as $product) {
            if (blank($product->main_image_file)) {
                continue;
            }

            $url = $this->sitemap->addChild('url');
            $url->addChild('loc', $this->buildProductUrl($context->baseDomain, $product->code));

            $currentImageXml = $url->addChild('xmlns:image:image');
            $currentImageXml->addChild('xmlns:image:loc', EnsiFile::public($product->main_image_file)->getUrl());
            $currentImageXml->addChild('xmlns:image:title', htmlspecialchars($product->name));
        }

        $this->saveSitemap('image.xml');
    }
}
