<?php

namespace App\Domain\Sitemap\Actions\Stages;

use App\Domain\Sitemap\Data\SitemapContext;
use App\Domain\Sitemap\Data\ValueFormatData;

/**
 * Генерирует sitemapindex.xml.
 *
 * В этот файл должны быть ссылки на отдельные файлы для товаров, категорий, дополнительных страниц и картинок.
 */
class GenerateIndexAction extends BaseStageAction
{
    public function execute(SitemapContext $context): void
    {
        $this->context = $context;
        $this->sitemap = $this->getBaseSitemapXmlElement('sitemapindex');

        $lastModString = date(ValueFormatData::DATE) . 'T' . date('H:i:s') . '+03:00';
        /** @var string[] Имена подфайлов сайтмапа */
        $sitemapFileNames = [
            'products.xml',
            'category.xml',
            'inner.xml',
            'image.xml',
        ];

        foreach ($sitemapFileNames as $sitemapFileName) {
            $sitemapTag = $this->sitemap->addChild('sitemap');
            $sitemapTag->addChild('loc', $context->baseDomain . 'sitemap/' . $sitemapFileName);
            $sitemapTag->addChild('lastmod', $lastModString);
        }

        $this->saveSitemap('sitemapindex.xml');
    }
}
