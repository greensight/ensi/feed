<?php

namespace App\Domain\Sitemap\Actions\Stages;

use App\Domain\Offers\Models\Product;
use App\Domain\Sitemap\Data\SitemapContext;
use App\Domain\Sitemap\Data\ValueFormatData;

class GenerateProductsAction extends BaseStageAction
{
    public function execute(SitemapContext $context): void
    {
        $this->context = $context;
        $this->sitemap = $this->getBaseSitemapXmlElement();

        /** @var Product $product */
        foreach ($context->products as $product) {
            $url = $this->sitemap->addChild('url');

            $url->addChild('loc', $this->buildProductUrl($context->baseDomain, $product->code));
            $url->addChild('lastmod', date(ValueFormatData::DATE));
            $url->addChild('changefreq', 'daily');
            $url->addChild('priority', '0.7');
        }

        $this->saveSitemap('products.xml');
    }
}
