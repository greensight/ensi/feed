<?php

namespace App\Domain\Sitemap\Actions\Stages;

use App\Domain\Sitemap\Data\SitemapContext;
use App\Domain\Sitemap\Data\ValueFormatData;

class GenerateInnerAction extends BaseStageAction
{
    public function execute(SitemapContext $context): void
    {
        $this->context = $context;
        $this->sitemap = $this->getBaseSitemapXmlElement();

        /** @var string[] $innerPages Список урлов статических, контентных и пр. внутренних страниц */
        $innerPages = [
            '' => [],
            // ...
        ];

        $defaultSetting = ['priority' => 1];

        /** @var array $settings */
        foreach ($innerPages as $innerPage => $settings) {
            $url = $this->sitemap->addChild('url');
            $url->addChild('loc', $context->baseDomain . $innerPage);
            $url->addChild('lastmod', date(ValueFormatData::DATE));
            $url->addChild('changefreq', 'daily');
            $url->addChild('priority', $settings['priority'] ?? $defaultSetting['priority']);
        }

        $this->saveSitemap('inner.xml');
    }
}
