<?php

namespace App\Domain\Sitemap\Actions\Stages;

use App\Domain\Sitemap\Data\SitemapContext;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use SimpleXMLElement;

class BaseStageAction
{
    protected SitemapContext $context;
    protected SimpleXMLElement $sitemap;

    public function __construct(protected readonly EnsiFilesystemManager $filesystemManager)
    {
    }

    /**
     * Формирует базовый XML-элемент.
     *
     * @param string $additions Дополнительный атрибут или атрибуты для headingTag'а
     */
    protected function getBaseSitemapXmlElement(string $headingTag = 'urlset', string $additions = ''): SimpleXMLElement
    {
        return new SimpleXMLElement(
            "<?xml version='1.0' encoding='UTF-8' ?><" . $headingTag . " xmlns='http://www.sitemaps.org/schemas/sitemap/0.9' " . $additions . " />"
        );
    }

    protected function buildCategoryUrl(string $baseDomain, string $categoryCode): string
    {
        return "{$baseDomain}/catalog/{$categoryCode}";
    }

    protected function buildProductUrl(string $baseDomain, string $productCode): string
    {
        return "{$baseDomain}/products/{$productCode}";
    }

    protected function saveSitemap(string $fileName): void
    {
        $savePath = $this->context->directoryPath . $fileName;
        $this->sitemap->asXML($savePath);

        $disk = Storage::disk($this->filesystemManager->publicDiskName());
        $disk->putFileAs('sitemap', new File($savePath), $fileName);
    }
}
