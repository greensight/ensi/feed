<?php

namespace App\Domain\Sitemap\Actions\Stages;

use App\Domain\Offers\Models\Category;
use App\Domain\Sitemap\Data\SitemapContext;
use App\Domain\Sitemap\Data\ValueFormatData;

class GenerateCategoriesAction extends BaseStageAction
{
    public function execute(SitemapContext $context): void
    {
        $this->context = $context;
        $this->sitemap = $this->getBaseSitemapXmlElement();

        /** @var Category $category */
        foreach ($context->categories as $category) {
            $url = $this->sitemap->addChild('url');
            $url->addChild('loc', $this->buildCategoryUrl($context->baseDomain, $category->code));
            $url->addChild('lastmod', date(ValueFormatData::DATE));
            $url->addChild('changefreq', 'daily');
            $url->addChild('priority', $category->parent_id ? '0.8' : '0.9');
        }

        $this->saveSitemap('category.xml');
    }
}
