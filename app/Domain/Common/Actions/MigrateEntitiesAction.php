<?php

namespace App\Domain\Common\Actions;

use App\Domain\Offers\Actions\MigrateEntitiesFromCatalogAction;
use App\Exceptions\ExceptionFormatter;
use Psr\Log\LoggerInterface;
use Throwable;

class MigrateEntitiesAction
{
    protected LoggerInterface $logger;

    public function __construct(
        public MigrateEntitiesFromCatalogAction $catalogAction
    ) {
        $this->logger = logger()->channel('entities:migration');
    }

    public function execute(): void
    {
        try {
            $this->logger->info('Migrating data from Catalog PIM and Offers');
            $this->catalogAction->execute();
        } catch (Throwable $e) {
            $this->logger->error(ExceptionFormatter::line('Error occurred while migrating data', $e));
        }
    }
}
