<?php

namespace App\Domain\Common\Jobs;

use App\Domain\Common\Actions\MigrateEntitiesAction;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class MigrateEntitiesJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function handle(MigrateEntitiesAction $action): void
    {
        $action->execute();
    }
}
