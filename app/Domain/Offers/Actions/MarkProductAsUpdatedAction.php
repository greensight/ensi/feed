<?php

namespace App\Domain\Offers\Actions;

use App\Domain\Offers\Models\Product;

class MarkProductAsUpdatedAction
{
    public function execute(?callable $filter = null, bool $cloud = false): void
    {
        $query = Product::query();

        if ($filter) {
            $filter($query);
        }

        $fields = [];
        if ($cloud) {
            $fields['cloud_fields_updated_at'] = now();
        }

        if ($fields) {
            $query->update($fields);
        }
    }
}
