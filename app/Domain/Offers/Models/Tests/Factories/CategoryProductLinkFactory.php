<?php

namespace App\Domain\Offers\Models\Tests\Factories;

use App\Domain\Offers\Models\Category;
use App\Domain\Offers\Models\CategoryProductLink;
use App\Domain\Offers\Models\Product;
use Ensi\LaravelTestFactories\BaseModelFactory;
use Illuminate\Support\Collection;

/**
 * @method CategoryProductLink createOne(array $fields = [])
 * @method CategoryProductLink makeOne(array $fields = [])
 * @method CategoryProductLink|CategoryProductLink[]|Collection create(array $fields = [], $parent = null)
 */
class CategoryProductLinkFactory extends BaseModelFactory
{
    protected $model = CategoryProductLink::class;

    /**
     * @inheritDoc
     */
    public function definition(): array
    {
        return [
            'category_product_id' => $this->faker->modelId(),
            'product_id' => Product::factory(),
            'category_id' => Category::factory(),
        ];
    }

    public function createFast(
        Category $category,
        Product $product,
    ): CategoryProductLink {
        $fields = [
            'category_id' => $category->category_id,
            'product_id' => $product->product_id,
        ];

        return $this->create($fields);
    }
}
