<?php

namespace App\Domain\Offers\Models\Tests\Factories;

use App\Domain\Offers\Models\ProductPropertyValue;
use Ensi\LaravelTestFactories\BaseModelFactory;
use Ensi\PimClient\Dto\PropertyTypeEnum;

/**
 * @extends BaseModelFactory<ProductPropertyValue>
 */
class ProductPropertyValueFactory extends BaseModelFactory
{
    protected $model = ProductPropertyValue::class;

    public function definition(): array
    {
        $directoryValueId = $this->faker->nullable()->modelId();

        return [
            'product_property_value_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
            'property_id' => $this->faker->modelId(),
            'type' => $this->faker->randomElement(PropertyTypeEnum::getAllowableEnumValues()),
            'directory_value_id' => $directoryValueId,
            'value' => $this->faker->nullable($directoryValueId ? 0 : 1)->word,
            'name' => $this->faker->nullable($directoryValueId ? 0 : 0.5)->sentence,
        ];
    }
}
