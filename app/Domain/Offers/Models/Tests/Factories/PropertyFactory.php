<?php

namespace App\Domain\Offers\Models\Tests\Factories;

use App\Domain\Offers\Models\Property;
use Ensi\LaravelTestFactories\BaseModelFactory;
use Ensi\PimClient\Dto\PropertyTypeEnum;

/**
 * @extends BaseModelFactory<Property>
 */
class PropertyFactory extends BaseModelFactory
{
    protected $model = Property::class;

    public function definition(): array
    {
        return [
            'property_id' => $this->faker->modelId(),
            'name' => $this->faker->sentence(),
            'code' => $this->faker->slug(),
            'type' => $this->faker->randomElement(PropertyTypeEnum::getAllowableEnumValues()),
            'is_public' => $this->faker->boolean(),
            'is_active' => $this->faker->boolean(),
        ];
    }

    public function active(): self
    {
        return $this->state([
            'is_public' => true,
            'is_active' => true,
        ]);
    }
}
