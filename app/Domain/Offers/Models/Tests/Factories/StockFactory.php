<?php

namespace App\Domain\Offers\Models\Tests\Factories;

use App\Domain\Offers\Models\Stock;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @extends BaseModelFactory<Stock>
 */
class StockFactory extends BaseModelFactory
{
    protected $model = Stock::class;

    public function definition(): array
    {
        return [
            'stock_id' => $this->faker->modelId(),
            'store_id' => $this->faker->modelId(),
            'offer_id' => $this->faker->modelId(),
            'qty' => $this->faker->randomFloat(0, 1, 99),
        ];
    }
}
