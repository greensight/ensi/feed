<?php

namespace App\Domain\Offers\Models\Tests\Factories;

use App\Domain\Offers\Models\ProductGroup;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @extends BaseModelFactory<ProductGroup>
 */
class ProductGroupFactory extends BaseModelFactory
{
    protected $model = ProductGroup::class;

    public function definition(): array
    {
        return [
            'product_group_id' => $this->faker->modelId(),
            'name' => $this->faker->sentence(),
            'category_id' => $this->faker->modelId(),
            'main_product_id' => $this->faker->modelId(),
            'is_active' => $this->faker->boolean(),
        ];
    }
}
