<?php

namespace App\Domain\Offers\Models\Tests\Factories;

use App\Domain\Offers\Models\Offer;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @extends BaseModelFactory<Offer>
 */
class OfferFactory extends BaseModelFactory
{
    protected $model = Offer::class;

    public function definition(): array
    {
        $basePrice = $this->faker->nullable()->randomNumber();

        return [
            'offer_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
            'base_price' => $basePrice,
            'price' => $basePrice ? $this->faker->nullable()->numberBetween(0, $basePrice) : null,
        ];
    }

    public function withBasePrice(bool $with = true): self
    {
        return $this->state([
            'base_price' => $this->faker->optional(weight: $with ? 1 : 0)->numberBetween(1, 10_000_00),
        ]);
    }

    public function withPrice(bool $with = true): self
    {
        $basePrice = $this->faker->optional(weight: $with ? 1 : 0)->numberBetween(1, 10_000_00);

        return $this->state([
            'base_price' => $basePrice,
            'price' => $this->faker->optional(weight: $with ? 1 : 0)->numberBetween(1, $basePrice),
        ]);
    }
}
