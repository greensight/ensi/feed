<?php

namespace App\Domain\Offers\Models\Tests\Factories;

use App\Domain\Offers\Models\PropertyDirectoryValue;
use Ensi\LaravelTestFactories\BaseModelFactory;
use Ensi\PimClient\Dto\PropertyTypeEnum;

/**
 * @extends BaseModelFactory<PropertyDirectoryValue>
 */
class PropertyDirectoryValueFactory extends BaseModelFactory
{
    protected $model = PropertyDirectoryValue::class;

    public function definition(): array
    {
        return [
            'directory_value_id' => $this->faker->modelId(),
            'property_id' => $this->faker->modelId(),
            'name' => $this->faker->nullable()->sentence(),
            'code' => $this->faker->nullable()->modelId(),
            'type' => $this->faker->randomElement(PropertyTypeEnum::getAllowableEnumValues()),
            'value' => $this->faker->word(),
        ];
    }
}
