<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\CategoryProductLinkFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 *
 * @property int $category_product_id - Link ID from PIM
 *
 * @property int $category_id Category ID from PIM
 * @property int $product_id Product ID from PIM
 *
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at

 * @property bool $is_migrated Was saved or created during record migration from master services
 */
class CategoryProductLink extends Model
{
    protected $table = 'category_product_links';

    public static function factory(): CategoryProductLinkFactory
    {
        return CategoryProductLinkFactory::new();
    }
}
