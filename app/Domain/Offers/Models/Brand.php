<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\BrandFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 *
 * @property int $brand_id ID бренда из PIM
 *
 * @property string $name Название
 * @property string $code ЧПУ код
 * @property string|null $description Описание бренда
 * @property string|null $logo_file Файл логотипа
 * @property string|null $logo_url URL логотипа
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property bool $is_migrated - сохранено/создано при миграции записей из мастер-сервисов
 */
class Brand extends Model
{
    protected $table = 'brands';

    protected $casts = [
        'brand_id' => 'int',
        'is_migrated' => 'bool',
    ];

    public static function factory(): BrandFactory
    {
        return BrandFactory::new();
    }
}
