<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\ProductGroupProductFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 *
 * @property int $product_group_product_id - id связи товарной склейки и товара из PIM
 *
 * @property int $product_group_id Id товарной склейки из PIM
 * @property int $product_id Id товара из PIM
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property-read ProductGroup|null $productGroup Товарная склейка
 * @property-read Product $product Товар
 *
 * @property bool $is_migrated - сохранено/создано при миграции записей из мастер-сервисов
 */
class ProductGroupProduct extends Model
{
    protected $table = 'product_group_product';

    protected $casts = [
        'product_group_product_id' => 'int',
        'product_group_id' => 'int',
        'product_id' => 'int',
    ];

    public function productGroup(): BelongsTo
    {
        return $this->belongsTo(ProductGroup::class, 'product_group_id', 'product_group_id');
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id', 'product_id');
    }

    public static function factory(): ProductGroupProductFactory
    {
        return ProductGroupProductFactory::new();
    }
}
