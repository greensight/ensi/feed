<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\PropertyFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 *
 * @property int $property_id ID атрибута из PIM
 *
 * @property string $name Название на витрине
 * @property string $code Код атрибута
 * @property string $type Тип
 * @property bool $is_public Отображать на витрине
 * @property bool $is_active Активность свойства
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property bool $is_migrated - сохранено/создано при миграции записей из мастер-сервисов
 */
class Property extends Model
{
    protected $table = 'properties';

    protected $casts = [
        'property_id' => 'int',
        'is_public' => 'bool',
        'is_active' => 'bool',
        'is_migrated' => 'bool',
    ];

    public static function factory(): PropertyFactory
    {
        return PropertyFactory::new();
    }
}
