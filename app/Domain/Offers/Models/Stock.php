<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\StockFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 *
 * @property int $stock_id - id стока из Offers
 *
 * @property int $store_id - id склада из BU
 * @property int $offer_id - id оффера из Offers
 *
 * @property float $qty - кол-во товара
 *
 * @property CarbonInterface|null $created_at - дата создания
 * @property CarbonInterface|null $updated_at - дата обновления
 *
 * @property Offer $offer
 *
 * @property bool $is_migrated - сохранено/создано при миграции записей из мастер-сервисов
 */
class Stock extends Model
{
    protected $table = 'stocks';

    protected $casts = [
        'stock_id' => 'int',
        'offer_id' => 'int',
        'qty' => 'float',
        'is_migrated' => 'bool',
    ];

    public function offer(): BelongsTo
    {
        return $this->belongsTo(Offer::class, 'offer_id', 'offer_id');
    }

    public static function factory(): StockFactory
    {
        return StockFactory::new();
    }
}
