<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkProductAsUpdatedAction;
use App\Domain\Offers\Models\ProductGroup;
use Illuminate\Database\Eloquent\Builder;

class ProductGroupObserver
{
    public function __construct(
        protected MarkProductAsUpdatedAction $markAction
    ) {
    }

    public function saved(ProductGroup $model): void
    {
        $this->markAction->execute(
            fn (Builder $query) => $query->whereHas('productGroupProduct', fn (Builder $queryValue) => $queryValue->where('product_group_id', $model->product_group_id)),
            $model->wasRecentlyCreated || $model->wasChanged(['is_active'])
        );
        if ($model->wasChanged(['main_product_id'])) {
            $this->markAction->execute(fn (Builder $query) => $query->whereIn('product_id', [$model->main_product_id, $model->getOriginal('main_product_id')]), true);
        }
    }

    // На deleted не вешаем $markAction, т.к. в таком случае придёт обновление по ProductGroupProduct,
    // и через него будет пометка на обновление
}
