<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkProductAsUpdatedAction;
use App\Domain\Offers\Models\CategoryProductLink;
use Illuminate\Database\Eloquent\Builder;

class CategoryProductLinkObserver
{
    public function __construct(
        protected MarkProductAsUpdatedAction $markProductsAction,
    ) {
    }

    public function saved(CategoryProductLink $model): void
    {
        $this->mark($model);
    }

    public function deleted(CategoryProductLink $model): void
    {
        $this->mark($model);
    }

    protected function mark(CategoryProductLink $model): void
    {
        $this->markProductsAction->execute(fn (Builder $queryProduct) => $queryProduct->where('product_id', $model->product_id), true);
    }
}
