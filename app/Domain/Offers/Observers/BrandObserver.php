<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkProductAsUpdatedAction;
use App\Domain\Offers\Models\Brand;
use Illuminate\Database\Eloquent\Builder;

class BrandObserver
{
    public function __construct(
        protected MarkProductAsUpdatedAction $markAction
    ) {
    }

    public function saved(Brand $model): void
    {
        $this->markAction->execute(fn (Builder $query) => $query->where('brand_id', $model->brand_id), $model->wasRecentlyCreated || $model->wasChanged(['name']));
    }

    // На deleted не вешаем $markAction, т.к. в таком случае придёт обновление по самому товару,
    // и через него будет пометка на обновление
}
