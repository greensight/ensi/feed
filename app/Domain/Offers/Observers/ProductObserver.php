<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Models\Product;

class ProductObserver
{
    public function creating(Product $model): void
    {
        $model->cloud_fields_updated_at ??= now();
    }

    public function updating(Product $model): void
    {
        if ($model->isDirty([
            'allow_publish',
            'main_image_file',
            'brand_id',
            'name',
            'description',
            'vendor_code',
            'barcode',
        ])) {
            $model->cloud_fields_updated_at = now();
        }
    }

    public function deleting(Product $model): void
    {
        // Сохранить во время обновления deleted_at невозможно из-за реализации soft-delete в ядре laravel
        $model->cloud_fields_updated_at = now();
        $model->save();
    }
}
