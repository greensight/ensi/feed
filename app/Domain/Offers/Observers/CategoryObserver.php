<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkCategoryAsUpdatedAction;
use App\Domain\Offers\Actions\MarkProductAsUpdatedAction;
use App\Domain\Offers\Models\Category;
use Illuminate\Database\Eloquent\Builder;

class CategoryObserver
{
    public function __construct(
        protected MarkProductAsUpdatedAction $markProductsAction,
        protected MarkCategoryAsUpdatedAction $markCategoryAction,
    ) {
    }

    public function creating(Category $model): void
    {
        $model->cloud_fields_updated_at ??= now();
        $this->markChildrenCloudFields($model);
    }

    public function updating(Category $model): void
    {
        if ($model->isDirty([
            'name',
            'parent_id',
            'is_real_active',
        ])) {
            $model->cloud_fields_updated_at = now();
        }

        if ($model->isDirty(['parent_id'])) {
            $this->markChildrenCloudFields($model);
        }
    }

    public function saved(Category $model): void
    {
        $this->markProductsAction->execute(
            fn (Builder $queryProduct) => $queryProduct->whereHas(
                'categories',
                fn (Builder $queryCategory) => $queryCategory->where(
                    'categories.category_id',
                    $model->category_id
                ),
            ),
            $model->wasRecentlyCreated || $model->wasChanged(['name', 'parent_id'])
        );
    }

    public function deleting(Category $model): void
    {
        // Сохранить во время обновления deleted_at невозможно из-за реализации soft-delete в ядре laravel
        $model->cloud_fields_updated_at = now();
        $model->save();
    }

    protected function markChildrenCloudFields(Category $model): void
    {
        $currentIds = collect([$model->category_id]);
        $updatedIds = collect();
        while ($currentIds->isNotEmpty()) {
            $currentIds = Category::query()->whereIn('parent_id', $currentIds)->pluck('category_id');
            $updatedIds = $updatedIds->merge($currentIds);
        }
        if ($updatedIds->isNotEmpty()) {
            $this->markCategoryAction->execute(fn (Builder $query) => $query->whereIn('category_id', $updatedIds), true);
        }
    }

    // На deleted не вешаем $markProductsAction, т.к. в таком случае придёт обновление по самому товару,
    // и через него будет пометка на обновление
}
