<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Discounts\Data\DiscountOfferIdentify;
use App\Domain\Discounts\Events\CalculateDiscount;
use App\Domain\Offers\Actions\MarkProductAsUpdatedAction;
use App\Domain\Offers\Models\Offer;
use Illuminate\Database\Eloquent\Builder;

class OfferObserver
{
    public function __construct(
        protected MarkProductAsUpdatedAction $markAction
    ) {
    }

    public function saving(Offer $model): void
    {
        $this->dispatchCalculateDiscount($model);
    }

    public function updating(Offer $model): void
    {
        $this->resetPrice($model);
    }

    public function saved(Offer $model): void
    {
        $this->markProductAsUpdated($model, $model->wasRecentlyCreated || $model->wasChanged(['base_price', 'price']));
    }

    public function deleted(Offer $model): void
    {
        $this->markProductAsUpdated($model, true);
    }

    protected function markProductAsUpdated(Offer $model, bool $cloud): void
    {
        $this->markAction->execute(fn (Builder $query) => $query->where('product_id', $model->product_id), $cloud);
    }

    protected function dispatchCalculateDiscount(Offer $model): void
    {
        if ($model->isDirty(['base_price']) && $model->base_price) {
            CalculateDiscount::dispatch(new DiscountOfferIdentify([$model->offer_id]));
        }
    }

    protected function resetPrice(Offer $model): void
    {
        if ($model->isDirty(['base_price'])) {
            $model->price = null;
        }
    }
}
