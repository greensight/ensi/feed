<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkProductAsUpdatedAction;
use App\Domain\Offers\Models\ProductGroupProduct;
use Illuminate\Database\Eloquent\Builder;

class ProductGroupProductObserver
{
    public function __construct(
        protected MarkProductAsUpdatedAction $markAction
    ) {
    }

    public function saved(ProductGroupProduct $model): void
    {
        $this->markProductAsUpdated($model->product_id, $model->wasRecentlyCreated || $model->wasChanged(['product_group_id', 'product_id']));
        if ($model->wasChanged(['product_id'])) {
            $this->markProductAsUpdated($model->getOriginal('product_id'), true);
        }
    }

    public function deleted(ProductGroupProduct $model): void
    {
        $this->markProductAsUpdated($model->product_id, true);
    }

    protected function markProductAsUpdated(int $productId, bool $cloud): void
    {
        $this->markAction->execute(fn (Builder $query) => $query->where('product_id', $productId), $cloud);
    }
}
