<?php


use App\Domain\Discounts\Data\DiscountOfferIdentify;
use App\Domain\Discounts\Events\CalculateDiscount;
use App\Domain\Offers\Models\Offer;
use Illuminate\Database\Eloquent\Model;
use PHPUnit\Framework\Assert;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'offers');

test("Observer OfferObserver create offer", function (bool $withBasePrice) {
    /** @var IntegrationTestCase $this */

    Event::fake([
        CalculateDiscount::class,
    ]);

    /** @var Offer $offer */
    $offer = Offer::factory()->withBasePrice($withBasePrice)->create();

    if ($withBasePrice) {
        CalculateDiscount::assertDispatched(new CalculateDiscount(new DiscountOfferIdentify(
            offerIds: [$offer->offer_id],
        )));
    } else {
        Event::assertNotDispatched(CalculateDiscount::class);
    }
})->with([
    "with price" => [true],
    "without price" => [false],
]);

test("Observer ListenOfferAction update offer", function (?int $currentPrice, ?int $newPrice, bool $sendEvent) {
    /** @var IntegrationTestCase $this */

    $initialEvent = Event::getFacadeRoot();
    Event::fake();
    Model::setEventDispatcher($initialEvent);

    /** @var Offer $offer */
    $offer = Offer::withoutEvents(fn () => Offer::factory()->create(['base_price' => $currentPrice]));

    $offer->base_price = $newPrice;
    $offer->save();

    if ($sendEvent) {
        CalculateDiscount::assertDispatched(new CalculateDiscount(new DiscountOfferIdentify(
            offerIds: [$offer->offer_id],
        )));
    } else {
        Event::assertNotDispatched(CalculateDiscount::class);
    }

    if ($currentPrice != $newPrice) {
        $offer->refresh();
        Assert::assertEquals(null, $offer->price);
    }
})->with([
    [1, null, false],
    [1, 1, false],
    [1, 2, true],
]);
