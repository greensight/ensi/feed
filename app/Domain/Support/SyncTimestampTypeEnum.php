<?php

namespace App\Domain\Support;

enum SyncTimestampTypeEnum: string
{
    case CLOUD_PRODUCTS = 'cloud_products';
    case CLOUD_CATEGORIES = 'cloud_categories';
}
