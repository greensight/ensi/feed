<?php

namespace App\Domain\Support\Models;

use App\Domain\Support\Models\Tests\Factories\SyncTimestampFactory;
use App\Domain\Support\SyncTimestampTypeEnum;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property SyncTimestampTypeEnum $type название синхронизации
 * @property string $stage ветка, на которой происходит индексация
 * @property CarbonInterface $last_schedule
 */
class SyncTimestamp extends Model
{
    protected $table = 'sync_timestamps';

    public $timestamps = false;

    protected $casts = [
        'type' => SyncTimestampTypeEnum::class,
        'last_schedule' => 'datetime',
    ];

    public static function factory(): SyncTimestampFactory
    {
        return SyncTimestampFactory::new();
    }

    public function scopeWhereType(Builder $builder, SyncTimestampTypeEnum $type): Builder
    {
        return $builder->where('type', $type->value)
            ->where('stage', config('app.stage'));
    }

    public static function makeForType(SyncTimestampTypeEnum $type): static
    {
        $model = new static();
        $model->type = $type;
        $model->stage = config('app.stage');

        return $model;
    }
}
