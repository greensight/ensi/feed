<?php

namespace App\Domain\Support\Models\Tests\Factories;

use App\Domain\Support\Models\SyncTimestamp;
use App\Domain\Support\SyncTimestampTypeEnum;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @extends BaseModelFactory<SyncTimestamp>
 */
class SyncTimestampFactory extends BaseModelFactory
{
    protected $model = SyncTimestamp::class;

    public function definition(): array
    {
        return [
            'type' => $this->faker->randomEnum(SyncTimestampTypeEnum::cases()),
            'stage' => config('app.stage'),
            'last_schedule' => $this->faker->dateTime(),
        ];
    }

    public function forType(SyncTimestampTypeEnum $type): self
    {
        return $this->state([
            'type' => $type->value,
        ]);
    }
}
