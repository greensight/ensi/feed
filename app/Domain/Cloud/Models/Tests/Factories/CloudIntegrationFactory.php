<?php

namespace App\Domain\Cloud\Models\Tests\Factories;

use App\Domain\Cloud\Models\CloudIntegration;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @extends BaseModelFactory<CloudIntegration>
 */
class CloudIntegrationFactory extends BaseModelFactory
{
    protected $model = CloudIntegration::class;

    public function definition(): array
    {
        return array_merge($this->generateIntegration(), [
            'stage' => config('app.stage'),
        ]);
    }

    public function enabled(): self
    {
        return $this->state($this->generateIntegration(true));
    }

    public function disabled(): self
    {
        return $this->state($this->generateIntegration(false));
    }

    protected function generateIntegration(?bool $integration = null): array
    {
        $integration = is_null($integration) ? $this->faker->boolean : $integration;

        return [
            'private_api_key' => $this->faker->nullable($integration ? 1 : 0)->ean8(),
            'public_api_key' => $this->faker->nullable($integration ? 1 : 0)->ean8(),
            'integration' => $integration,
        ];
    }
}
