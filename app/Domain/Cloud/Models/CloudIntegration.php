<?php

namespace App\Domain\Cloud\Models;

use App\Domain\Cloud\Models\Tests\Factories\CloudIntegrationFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 *
 * @property string|null $private_api_key Приватный API-ключ
 * @property string|null $public_api_key Публичный API-ключ
 * @property string $stage Название ветки
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property bool $integration Интеграция с сервисом Ensi Cloud Cloud
 */
class CloudIntegration extends Model
{
    protected $table = 'cloud_integration';

    protected $fillable = [
        'stage',
        'private_api_key',
        'public_api_key',
        'integration',
    ];

    protected $casts = [
        'integration' => 'bool',
    ];

    public function __construct(array $attributes = [])
    {
        $attributes['stage'] ??= config('app.stage');

        parent::__construct($attributes);
    }

    public function isDisabled(): bool
    {
        return !$this->integration;
    }

    public function isNotEmptyKey(): bool
    {
        return !empty($this->private_api_key) && !empty($this->public_api_key);
    }

    public function check(): bool
    {
        return $this->isDisabled() || $this->isNotEmptyKey();
    }

    public static function existsForCurrentStage(): bool
    {
        return self::query()
            ->whereStage()
            ->exists();
    }

    public function scopeWhereStage(Builder $builder): void
    {
        $builder->where('stage', config('app.stage'));
    }

    public static function factory(): CloudIntegrationFactory
    {
        return CloudIntegrationFactory::new();
    }
}
