<?php

namespace App\Domain\Cloud\Actions\DispatchToSync;

use App\Domain\Cloud\Jobs\SyncCategoriesJob;
use App\Domain\Offers\Models\Category;
use App\Domain\Support\SyncTimestampTypeEnum;
use Carbon\CarbonInterface;

class DispatchCategoriesToSyncAction implements DispatchEntityToSyncInterface
{
    public function __construct(protected DispatchEntityToSyncAction $dispatchEntityToIndexing)
    {
    }

    public function execute(): void
    {
        $this->dispatchEntityToIndexing->execute(
            type: SyncTimestampTypeEnum::CLOUD_CATEGORIES,
            queryBuilder: function (?CarbonInterface $from, CarbonInterface $to) {
                $query = Category::withTrashed()
                    ->where('cloud_fields_updated_at', '<', $to)
                    ->selectRaw('category_id as dispatch_id');

                if ($from) {
                    $query->where('cloud_fields_updated_at', '>=', $from);
                }

                return $query;
            },
            dispatch: function (array $ids) {
                SyncCategoriesJob::dispatch($ids);
            },
            logger: logger()->channel('cloud:dispatch-to-sync:categories')
        );
    }
}
