<?php

namespace App\Domain\Cloud\Actions\DispatchToSync;

interface DispatchEntityToSyncInterface
{
    public function execute(): void;
}
