<?php

use App\Domain\Cloud\Actions\DispatchToSync\DispatchProductsToSyncAction;
use App\Domain\Cloud\Jobs\SyncProductsJob;
use App\Domain\Offers\Models\Product;
use App\Domain\Support\Models\SyncTimestamp;
use App\Domain\Support\SyncTimestampTypeEnum;
use Illuminate\Support\Facades\Date;

use function Pest\Laravel\assertDatabaseHas;

use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('component', 'DispatchProductsToSyncAction');

test("Action DispatchProductsToSyncAction success with SyncTimestamp", function () {
    /** @var ComponentTestCase $this */
    Date::setTestNow(now());
    /** @var SyncTimestamp $timestamp */
    $timestamp = SyncTimestamp::factory()->forType(SyncTimestampTypeEnum::CLOUD_PRODUCTS)->create([
        'last_schedule' => now()->subDay(),
    ]);

    // Товары добавятся в джоб
    $timeOk = ['cloud_fields_updated_at' => $timestamp->last_schedule->addSecond()];
    $products = Product::factory()->count(2)->create($timeOk);
    $products->push(
        Product::factory()->trashed()->create($timeOk)
    );

    // Товары не добавятся ни в один джоб
    $timeNon = ['cloud_fields_updated_at' => $timestamp->last_schedule->subSecond()];
    Product::factory()->count(2)->create($timeNon);
    Product::factory()->trashed()->create($timeNon);

    Queue::fake();

    resolve(DispatchProductsToSyncAction::class)->execute();

    Queue::assertPushed(function (SyncProductsJob $job) use ($products) {
        return arrayEquals($job->ids, $products->pluck('product_id')->all());
    });

    assertDatabaseHas(SyncTimestamp::class, [
        'id' => $timestamp->id,
        'last_schedule' => now()->format((new SyncTimestamp())->getDateFormat()),
    ]);
});

test("Action DispatchProductsToSyncAction success without SyncTimestamp", function () {
    /** @var ComponentTestCase $this */
    Date::setTestNow(now());

    // Товары добавятся в джоб
    $products = Product::factory()->count(2)->create();

    Queue::fake();

    resolve(DispatchProductsToSyncAction::class)->execute();

    Queue::assertPushed(function (SyncProductsJob $job) use ($products) {
        return arrayEquals($job->ids, $products->pluck('product_id')->all());
    });

    assertDatabaseHas(SyncTimestamp::class, [
        'type' => SyncTimestampTypeEnum::CLOUD_PRODUCTS,
        'last_schedule' => now()->format((new SyncTimestamp())->getDateFormat()),
    ]);
});
