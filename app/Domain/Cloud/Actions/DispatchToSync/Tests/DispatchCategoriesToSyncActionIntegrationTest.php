<?php

use App\Domain\Cloud\Actions\DispatchToSync\DispatchCategoriesToSyncAction;
use App\Domain\Cloud\Jobs\SyncCategoriesJob;
use App\Domain\Offers\Models\Category;
use App\Domain\Support\Models\SyncTimestamp;
use App\Domain\Support\SyncTimestampTypeEnum;
use Illuminate\Support\Facades\Date;

use function Pest\Laravel\assertDatabaseHas;

use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('component', 'DispatchCategoriesToSyncAction');

test("Action DispatchCategoriesToSyncAction success with SyncTimestamp", function () {
    /** @var ComponentTestCase $this */
    Date::setTestNow(now());
    /** @var SyncTimestamp $timestamp */
    $timestamp = SyncTimestamp::factory()->forType(SyncTimestampTypeEnum::CLOUD_CATEGORIES)->create([
        'last_schedule' => now()->subDay(),
    ]);

    // Категории добавятся в джоб
    $timeOk = ['cloud_fields_updated_at' => $timestamp->last_schedule->addSecond()];
    $categories = Category::factory()->count(2)->create($timeOk);
    $categories->push(
        Category::factory()->trashed()->create($timeOk)
    );

    // Категории не добавятся ни в один джоб
    $timeNon = ['cloud_fields_updated_at' => $timestamp->last_schedule->subSecond()];
    Category::factory()->count(2)->create($timeNon);
    Category::factory()->trashed()->create($timeNon);

    Queue::fake();

    resolve(DispatchCategoriesToSyncAction::class)->execute();

    Queue::assertPushed(function (SyncCategoriesJob $job) use ($categories) {
        return arrayEquals($job->ids, $categories->pluck('category_id')->all());
    });

    assertDatabaseHas(SyncTimestamp::class, [
        'id' => $timestamp->id,
        'last_schedule' => now()->format((new SyncTimestamp())->getDateFormat()),
    ]);
});

test("Action DispatchCategoriesToSyncAction success without SyncTimestamp", function () {
    /** @var ComponentTestCase $this */
    Date::setTestNow(now());

    // Категории добавятся в джоб
    $categories = Category::factory()->count(2)->create();

    Queue::fake();

    resolve(DispatchCategoriesToSyncAction::class)->execute();

    Queue::assertPushed(function (SyncCategoriesJob $job) use ($categories) {
        return arrayEquals($job->ids, $categories->pluck('category_id')->all());
    });

    assertDatabaseHas(SyncTimestamp::class, [
        'type' => SyncTimestampTypeEnum::CLOUD_CATEGORIES,
        'last_schedule' => now()->format((new SyncTimestamp())->getDateFormat()),
    ]);
});
