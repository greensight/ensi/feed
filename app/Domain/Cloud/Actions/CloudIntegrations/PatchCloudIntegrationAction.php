<?php

namespace App\Domain\Cloud\Actions\CloudIntegrations;

use App\Domain\Cloud\Models\CloudIntegration;
use App\Exceptions\ValidateException;

class PatchCloudIntegrationAction
{
    public function execute(array $fields): CloudIntegration
    {
        /** @var CloudIntegration $cloudIntegration */
        $cloudIntegration = CloudIntegration::query()
            ->where('stage', config('app.stage'))
            ->firstOrFail();

        $cloudIntegration->fill($fields);

        throw_unless(
            $cloudIntegration->check(),
            ValidateException::class,
            'Отсутствует ключ API'
        );

        $cloudIntegration->save();

        return $cloudIntegration;
    }
}
