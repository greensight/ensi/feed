<?php

namespace App\Domain\Cloud\Actions\CloudIntegrations;

use App\Domain\Cloud\Models\CloudIntegration;
use App\Exceptions\ValidateException;

class CreateCloudIntegrationAction
{
    public function execute(array $fields): CloudIntegration
    {
        throw_if(
            CloudIntegration::existsForCurrentStage(),
            ValidateException::class,
            'Ключ API уже существует'
        );

        $cloudIntegration = new CloudIntegration();
        $cloudIntegration->fill($fields);
        $cloudIntegration->save();

        return $cloudIntegration;
    }
}
