<?php

namespace App\Domain\Cloud\Actions\CloudIntegrations;

use App\Domain\Cloud\Models\CloudIntegration;

class GetCloudIntegrationAction
{
    public function execute(): CloudIntegration
    {
        /** @var CloudIntegration $cloudIntegration */
        $cloudIntegration = CloudIntegration::query()
            ->whereStage()
            ->firstOrFail();

        return $cloudIntegration;
    }
}
