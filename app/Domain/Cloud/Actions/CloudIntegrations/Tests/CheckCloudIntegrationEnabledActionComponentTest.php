<?php

use App\Domain\Cloud\Actions\CloudIntegrations\CheckCloudIntegrationEnabledAction;
use App\Domain\Cloud\Models\CloudIntegration;
use Ensi\CloudApiSdk\Configuration;
use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('component', 'CheckCloudIntegrationEnabledAction');

test("Action CheckCloudIntegrationEnabledAction integration disable", function () {
    /** @var ComponentTestCase $this */

    CloudIntegration::factory()->disabled()->create();

    $mockConfig = $this->mock(Configuration::class);
    $mockConfig->allows([
        'getPublicToken' => '',
        'getPrivateToken' => '',
    ]);
    $mockConfig->shouldNotReceive('setPublicToken');
    $mockConfig->shouldNotReceive('setPrivateToken');

    /** @var CheckCloudIntegrationEnabledAction $action */
    $action = resolve(CheckCloudIntegrationEnabledAction::class);
    $action->execute();
});

test("Action CheckCloudIntegrationEnabledAction integration enabled", function () {
    /** @var ComponentTestCase $this */

    /** @var CloudIntegration $integration */
    $integration = CloudIntegration::factory()->enabled()->create();

    $mockConfig = $this->mock(Configuration::class);
    $mockConfig->allows([
        'getPublicToken' => '',
        'getPrivateToken' => '',
    ]);
    $mockConfig->shouldReceive('setPublicToken')->once()->with($integration->public_api_key);
    $mockConfig->shouldReceive('setPrivateToken')->once()->with($integration->private_api_key);

    /** @var CheckCloudIntegrationEnabledAction $action */
    $action = resolve(CheckCloudIntegrationEnabledAction::class);
    $action->execute();
});

test("Action CheckCloudIntegrationEnabledAction integration enabled twice call", function () {
    /** @var ComponentTestCase $this */

    $mockConfig = $this->mock(Configuration::class);
    $mockConfig->allows([
        'getPublicToken' => '123',
        'getPrivateToken' => '123',
    ]);
    $mockConfig->shouldNotReceive('setPublicToken');
    $mockConfig->shouldNotReceive('setPrivateToken');

    /** @var CheckCloudIntegrationEnabledAction $action */
    $action = resolve(CheckCloudIntegrationEnabledAction::class);
    $action->execute();
});
