<?php

namespace App\Domain\Cloud\Actions\Sync;

use App\Domain\Offers\Models\Category;
use App\Exceptions\ExceptionFormatter;
use Ensi\CloudApiSdk\Api\IndexesApi;
use Ensi\CloudApiSdk\Dto\Indexes\IndexesCategories\CategoryAction\CategoryAction;
use Ensi\CloudApiSdk\Dto\Indexes\IndexesCategories\CategoryAction\CategoryActionEnum;
use Ensi\CloudApiSdk\Dto\Indexes\IndexesCategories\CategoryAction\CategoryBody;
use Ensi\CloudApiSdk\Dto\Indexes\IndexesCategories\IndexesCategoriesRequest;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Psr\Log\LoggerInterface;
use Throwable;

class SyncCategoriesAction
{
    protected LoggerInterface $logger;

    protected array $categoryParentIds = [];

    public function __construct(protected IndexesApi $indexesApi)
    {
        $this->logger = logger()->channel('cloud:sync:categories');
    }

    /**
     * @param Collection<int,Category> $categories
     *
     * @return void
     */
    public function execute(Collection $categories): void
    {
        $this->categoryParentIds = Category::query()->pluck('parent_id', 'category_id')->all();

        $actions = [];
        $categoryCreatedIds = [];
        $categoryDeletedIds = [];
        $this->logger->info("=== Подготовка категорий");
        foreach ($categories as $category) {
            $this->logger->info("Товар: {$category->category_id}. Начинаю подготовку синхронизации");

            try {
                $categoryAction = new CategoryAction();
                $categoryAction->id = (string)$category->category_id;

                if ($category->trashed() || !$category->is_real_active) {
                    if ($category->cloud_created) {
                        $categoryAction->action = CategoryActionEnum::DELETE;
                        $categoryDeletedIds[] = $category->category_id;
                    } else {
                        $this->logger->info("Категория: {$category->category_id}. Пропуск, т.к. категория на удаление, но и не была создана в cloud");

                        continue;
                    }
                } else {
                    if ($category->cloud_created) {
                        $categoryAction->action = CategoryActionEnum::UPDATE;
                    } else {
                        $categoryAction->action = CategoryActionEnum::CREATE;
                        $categoryCreatedIds[] = $category->category_id;
                    }
                    $categoryAction->body = $this->cloudBodyFromModel($category);
                }

                $actions[$category->category_id] = $categoryAction;
                $this->logger->info("Категория: {$category->category_id}. Добавлен в пакет для синхронизации. action = {$categoryAction->action->value}");
            } catch (Throwable $e) {
                $this->logger->error("Категория: {$category->category_id}. " . ExceptionFormatter::line("Ошибка при подготовке к синхронизации", $e));

                continue;
            }
        }

        if (!$actions) {
            $this->logger->warning("Нет событий для отправки");

            return;
        }

        try {
            $this->send(array_values($actions));
        } catch (Exception $e) {
            $this->logger->error(ExceptionFormatter::line("Ошибка при отправке пакета", $e));

            return;
        }

        if ($categoryCreatedIds) {
            $this->logger->info("Устанавливаю cloud_created = true для категорий", [
                "category_ids" => $categoryCreatedIds,
            ]);
            $count = Category::query()
                ->whereIn('category_id', $categoryCreatedIds)
                ->update(['cloud_created' => true]);
            $this->logger->info("cloud_created = true установлено для категорий: {$count}");
        }

        if ($categoryDeletedIds) {
            $this->logger->info("Устанавливаю cloud_created = false для категорий", [
                "category_ids" => $categoryDeletedIds,
            ]);
            $count = Category::withTrashed()
                ->whereIn('category_id', $categoryDeletedIds)
                ->update(['cloud_created' => false]);
            $this->logger->info("cloud_created = false установлено для категорий: {$count}");
        }
    }

    protected function cloudBodyFromModel(Category $category): CategoryBody
    {
        $categoryBody = new CategoryBody();
        $categoryBody->name = $category->name;

        $categoryIds = [];
        $currentId = $category->parent_id;
        while ($currentId) {
            $categoryIds[] = (string)$currentId;
            $currentId = $this->categoryParentIds[$currentId] ?? null;
        }
        $categoryBody->parent_ids = $categoryIds;

        return $categoryBody;
    }

    protected function send(array $actions): void
    {
        $request = new IndexesCategoriesRequest();
        $request->actions = $actions;

        $this->indexesApi->categories($request);
    }
}
