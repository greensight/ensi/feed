<?php

namespace App\Domain\Cloud\Actions\Sync;

use App\Domain\Offers\Models\Category;
use App\Domain\Offers\Models\Product;
use App\Exceptions\ExceptionFormatter;
use Ensi\CloudApiSdk\Api\IndexesApi;
use Ensi\CloudApiSdk\Dto\Indexes\IndexesProducts\IndexesProductsRequest;
use Ensi\CloudApiSdk\Dto\Indexes\IndexesProducts\ProductAction\ProductAction;
use Ensi\CloudApiSdk\Dto\Indexes\IndexesProducts\ProductAction\ProductActionEnum;
use Ensi\CloudApiSdk\Dto\Indexes\IndexesProducts\ProductAction\ProductBody;
use Ensi\CloudApiSdk\Dto\Indexes\IndexesProducts\ProductAction\ProductLocation;
use Ensi\CloudApiSdk\Dto\Indexes\IndexesProducts\ProductAction\ProductProperty;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Psr\Log\LoggerInterface;
use Throwable;

class SyncProductsAction
{
    public const PROPERTY_MAIN_PRODUCT_NAME = 'Главный товар';
    public const PROPERTY_MAIN_PRODUCT_VALUE = 'да';

    protected LoggerInterface $logger;

    public function __construct(protected IndexesApi $indexesApi)
    {
        $this->logger = logger()->channel('cloud:sync:products');
    }

    /**
     * @param Collection<int,Product> $products
     *
     * @return void
     */
    public function execute(Collection $products): void
    {
        $products->loadMissing([
            'offers',
            'productPropertyValues.property',
            'productPropertyValues.directoryValue',
            'brand',
            'productGroup',
            'categories',
        ]);
        $actions = [];
        $productCreatedIds = [];
        $productDeletedIds = [];
        $this->logger->info("=== Подготовка товаров");
        foreach ($products as $product) {
            $this->logger->info("Товар: {$product->product_id}. Начинаю подготовку синхронизации");

            try {
                $productAction = new ProductAction();
                $productAction->id = (string)$product->product_id;

                if ($product->trashed() || !$product->allow_publish) {
                    if ($product->cloud_created) {
                        $productAction->action = ProductActionEnum::DELETE;
                        $productDeletedIds[] = $product->product_id;
                    } else {
                        $this->logger->info("Товар: {$product->product_id}. Пропуск, т.к. товар на удаление, но не был создан в cloud");

                        continue;
                    }
                } else {
                    if ($product->cloud_created) {
                        $productAction->action = ProductActionEnum::UPDATE;
                    } else {
                        $productAction->action = ProductActionEnum::CREATE;
                        $productCreatedIds[] = $product->product_id;
                    }
                    $productAction->body = $this->cloudBodyFromModel($product);
                }

                $actions[$product->product_id] = $productAction;
                $this->logger->info("Товар: {$product->product_id}. Добавлен в пакет для синхронизации. action = {$productAction->action->value}");
            } catch (Throwable $e) {
                $this->logger->error("Товар: {$product->product_id}. " . ExceptionFormatter::line("Ошибка при подготовке к синхронизации", $e));

                continue;
            }
        }

        if (!$actions) {
            $this->logger->warning("Нет событий для отправки");

            return;
        }

        try {
            $this->send(array_values($actions));
        } catch (Exception $e) {
            $this->logger->error(ExceptionFormatter::line("Ошибка при отправке пакета", $e));

            return;
        }

        if ($productCreatedIds) {
            $this->logger->info("Устанавливаю cloud_created = true для товаров", [
                "product_ids" => $productCreatedIds,
            ]);
            $count = Product::query()
                ->whereIn('product_id', $productCreatedIds)
                ->update(['cloud_created' => true]);
            $this->logger->info("cloud_created = true установлено для товаров: {$count}");
        }

        if ($productDeletedIds) {
            $this->logger->info("Устанавливаю cloud_created = false для товаров", [
                "product_ids" => $productDeletedIds,
            ]);
            $count = Product::withTrashed()
                ->whereIn('product_id', $productDeletedIds)
                ->update(['cloud_created' => false]);
            $this->logger->info("cloud_created = false установлено для товаров: {$count}");
        }
    }

    protected function cloudBodyFromModel(Product $product): ProductBody
    {
        $productBody = new ProductBody();
        $productBody->name = $product->name;
        $productBody->category_ids = $product->categories->map(fn (Category $category) => (string)$category->category_id)->toArray();
        if ($product->brand) {
            $productBody->brand = $product->brand->name;
        }
        $productBody->vendor_code = $product->vendor_code;
        $productBody->barcodes = $product->barcode ? [$product->barcode] : [];
        if ($product->description) {
            $productBody->description = $product->description;
        }
        if ($product->main_image_file) {
            $productBody->picture = EnsiFile::public($product->main_image_file)->getUrl();
        }

        $productBody->group_ids = [];
        $isProductMain = true;
        if ($product->productGroup?->is_active) {
            $productBody->group_ids = [(string)$product->productGroup->product_group_id];
            if ($product->productGroup->main_product_id != $product->product_id) {
                $isProductMain = false;
            }

        }

        $locations = [];
        foreach ($product->offers as $offer) {
            $location = new ProductLocation();
            $location->id = '1'; // хардкор по ФЗ
            $price = $offer->price ?? $offer->base_price;
            if ($price) {
                $location->price = $price;
            }
            $locations[] = $location;

            break;
        }
        if (!$locations) {
            throw new Exception("Нет офферов");
        }
        $productBody->locations = $locations;

        $propertyValues = [];
        foreach ($product->productPropertyValues as $propertyValue) {
            $property = $propertyValue->property;
            if (!$property || !$property->is_public || !$property->is_active) {
                continue;
            }
            $propValueAndName = $propertyValue->getPropValueAndName();
            if (!$propValueAndName) {
                continue;
            }

            $propertyValues[$property->name][] = $propValueAndName[0];
        }
        $properties = [];
        foreach ($propertyValues as $name => $values) {
            $property = new ProductProperty();
            $property->name = (string)$name;
            $property->values = array_map('strval', $values);

            $properties[] = $property;
        }
        if ($isProductMain) {
            $property = new ProductProperty();
            $property->name = static::PROPERTY_MAIN_PRODUCT_NAME;
            $property->values = [static::PROPERTY_MAIN_PRODUCT_VALUE];

            $properties[] = $property;
        }

        $productBody->properties = $properties;

        return $productBody;
    }

    protected function send(array $actions): void
    {
        $request = new IndexesProductsRequest();
        $request->actions = $actions;

        $this->indexesApi->products($request);
    }
}
