<?php

namespace App\Domain\Cloud\Jobs\Tests\TestCases;

use App\Domain\Cloud\Models\CloudIntegration;
use Ensi\CloudApiSdk\Dto\Indexes\IndexesCategories\CategoryAction\CategoryAction;
use Ensi\CloudApiSdk\Dto\Indexes\IndexesCategories\IndexesCategoriesRequest;
use Mockery\Expectation;
use Tests\ComponentTestCase;

class SyncCategoriesJobTestCase extends ComponentTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        CloudIntegration::factory()->enabled()->create();
    }

    public function cloudIntegrationDisable(): void
    {
        CloudIntegration::query()->whereStage()->update(['integration' => false]);
    }

    public function &shouldSendCategory(): ?CategoryAction
    {
        $action = null;
        /** @var Expectation $shouldReceive */
        $shouldReceive = $this->mockCloudApiIndexesApi()->shouldReceive('categories');
        $shouldReceive
            ->once()
            ->withArgs(function (IndexesCategoriesRequest $apiRequest) use (&$action) {
                $action = $apiRequest->actions[0] ?? null;

                return count($apiRequest->actions) == 1;
            });

        return $action;
    }

    public function neverSendCategory(): void
    {
        $this->mockCloudApiIndexesApi()->shouldNotReceive('categories');
    }
}
