<?php

use App\Domain\Cloud\Actions\Sync\SyncProductsAction;
use App\Domain\Cloud\Jobs\SyncProductsJob;
use App\Domain\Cloud\Jobs\Tests\TestCases\SyncProductsJobTestCase;
use App\Domain\Offers\Models\Brand;
use App\Domain\Offers\Models\Category;
use App\Domain\Offers\Models\Offer;
use App\Domain\Offers\Models\Product;
use App\Domain\Offers\Models\ProductGroup;
use App\Domain\Offers\Models\ProductGroupProduct;
use App\Domain\Offers\Models\ProductPropertyValue;
use App\Domain\Offers\Models\Property;
use Ensi\CloudApiSdk\Dto\Indexes\IndexesProducts\ProductAction\ProductActionEnum;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;
use Ensi\PimClient\Dto\PropertyTypeEnum;
use Illuminate\Database\Eloquent\Collection;

use function Pest\Laravel\assertDatabaseHas;
use function PHPUnit\Framework\assertArrayNotHasKey;
use function PHPUnit\Framework\assertEquals;

uses(SyncProductsJobTestCase::class);
uses()->group('component', 'SyncProductsJob');

test("Action SyncProductsJob integration disable", function () {
    /** @var SyncProductsJobTestCase $this */
    $this->cloudIntegrationDisable();

    /** @var Collection<Product> $products */
    $products = Product::factory()->active()->count(2)->create();

    $this->neverSendProduct();

    SyncProductsJob::dispatchSync($products->pluck('product_id')->all());
});

test("Action SyncProductsJob create|update success", function (bool $cloudCreated) {
    /** @var SyncProductsJobTestCase $this */

    $categories = Category::factory()->count(2)->create();
    /** @var Product $product */
    $product = Product::factory()->inCategories($categories->all())->active()->create(['cloud_created' => $cloudCreated]);
    Offer::factory()->create(['product_id' => $product->product_id]);

    $action = &$this->shouldSendProduct();

    SyncProductsJob::dispatchSync([$product->product_id]);

    assertEquals($cloudCreated ? ProductActionEnum::UPDATE : ProductActionEnum::CREATE, $action->action);

    assertDatabaseHas(Product::class, [
        'id' => $product->id,
        'cloud_created' => true,
    ]);
})->with([true, false]);

test("Action SyncProductsJob base fields success", function () {
    /** @var SyncProductsJobTestCase $this */

    $categories = Category::factory()->count(2)->create();
    /** @var Product $product */
    $product = Product::factory()->inCategories($categories->all())->active()->create();
    Offer::factory()->create(['product_id' => $product->product_id]);

    $action = &$this->shouldSendProduct();

    SyncProductsJob::dispatchSync([$product->product_id]);

    expect($action->id)->toEqual($product->product_id)->toBeString();

    $body = $action->body->toArray();
    assertEquals($product->name, $action->body->name);

    assertArrayNotHasKey('url', $body);

    expect($action->body->category_ids[0])->toEqual($product->categories->first()->category_id)->toBeString();
    expect($action->body->category_ids)->toHaveCount($product->categories->count());

    assertEquals($product->vendor_code, $action->body->vendor_code);

    assertArrayNotHasKey('country', $body);
});

test("Action SyncProductsJob brand success", function (bool $withBrand) {
    /** @var SyncProductsJobTestCase $this */

    $brand = $withBrand ? Brand::factory()->create() : null;
    /** @var Product $product */
    $product = Product::factory()->active()->create([
        'brand_id' => $brand?->brand_id,
    ]);
    Offer::factory()->create(['product_id' => $product->product_id]);

    $action = &$this->shouldSendProduct();

    SyncProductsJob::dispatchSync([$product->product_id]);

    if ($withBrand) {
        assertEquals($brand->name, $action->body->brand);
    } else {
        assertArrayNotHasKey('brand', $action->body->toArray());
    }

})->with([true, false]);

test("Action SyncProductsJob barcode success", function (bool $withBarcode) {
    /** @var SyncProductsJobTestCase $this */

    /** @var Product $product */
    $product = Product::factory()->active()->create([
        'barcode' => $withBarcode ? '12345' : null,
    ]);
    Offer::factory()->create(['product_id' => $product->product_id]);

    $action = &$this->shouldSendProduct();

    SyncProductsJob::dispatchSync([$product->product_id]);

    assertEquals($withBarcode ? [$product->barcode] : [], $action->body->barcodes);
})->with([true, false]);

test("Action SyncProductsJob description success", function (bool $withDescription) {
    /** @var SyncProductsJobTestCase $this */

    /** @var Product $product */
    $product = Product::factory()->active()->create([
        'description' => $withDescription ? '12345' : null,
    ]);
    Offer::factory()->create(['product_id' => $product->product_id]);

    $action = &$this->shouldSendProduct();

    SyncProductsJob::dispatchSync([$product->product_id]);

    if ($withDescription) {
        assertEquals($product->description, $action->body->description);
    } else {
        assertArrayNotHasKey('description', $action->body->toArray());
    }
})->with([true, false]);

test("Action SyncProductsJob picture success", function (bool $withPicture) {
    /** @var SyncProductsJobTestCase $this */

    /** @var Product $product */
    $product = Product::factory()->active()->create([
        'main_image_file' => $withPicture ? 'http://path.domain' : null,
    ]);
    Offer::factory()->create(['product_id' => $product->product_id]);

    $action = &$this->shouldSendProduct();

    SyncProductsJob::dispatchSync([$product->product_id]);

    if ($withPicture) {
        assertEquals(EnsiFile::public($product->main_image_file)->getUrl(), $action->body->picture);
    } else {
        assertArrayNotHasKey('picture', $action->body->toArray());
    }
})->with([true, false]);

test("Action SyncProductsJob group_ids success", function (
    bool $withGroup,
    bool $groupActive,
    bool $currentIsMain,
    bool $existsProp,
) {
    /** @var SyncProductsJobTestCase $this */

    /** @var Product $product */
    $product = Product::factory()->active()->create();
    Offer::factory()->create(['product_id' => $product->product_id]);
    $productGroup = ProductGroup::factory()->create([
        'main_product_id' => $currentIsMain ? $product->product_id : $product->product_id + 1,
        'is_active' => $groupActive,
    ]);
    if ($withGroup) {
        ProductGroupProduct::factory()->create([
            'product_group_id' => $productGroup->product_group_id,
            'product_id' => $product->product_id,
        ]);
    }

    $action = &$this->shouldSendProduct();

    SyncProductsJob::dispatchSync([$product->product_id]);

    if ($groupActive) {
        expect($action->body->group_ids[0])->toEqual($productGroup->product_group_id)->toBeString();
    }
    expect($action->body->group_ids)->toHaveCount($groupActive ? 1 : 0);

    if ($existsProp) {
        $prop = $action->body->properties[0];
        expect($prop->name)->toEqual(SyncProductsAction::PROPERTY_MAIN_PRODUCT_NAME);
        expect($prop->values)->toEqual([SyncProductsAction::PROPERTY_MAIN_PRODUCT_VALUE]);
    }

    expect($action->body->properties)->toHaveCount($existsProp ? 1 : 0);
})->with([
    // $withGroup, $groupActive, $currentIsMain, $existsProp
    [true, true, true, true],
    [true, true, false, false],
    [true, false, false, true],
    [false, false, false, true],
]);

test("Action SyncProductsJob locations success", function (
    bool $withOffer,
    ?int $basePrice = null,
    ?int $price = null,
    ?int $actionPrice = null
) {
    /** @var SyncProductsJobTestCase $this */

    /** @var Product $product */
    $product = Product::factory()->active()->create();
    if ($withOffer) {
        Offer::factory()->create([
            'product_id' => $product->product_id,
            'price' => $price,
            'base_price' => $basePrice,
        ]);
        $action = &$this->shouldSendProduct();
    } else {
        $this->neverSendProduct();
    }


    SyncProductsJob::dispatchSync([$product->product_id]);

    if ($withOffer) {
        $location = $action->body->locations[0];
        expect($location->id)->toEqual('1')->toBeString();
        if (!is_null($actionPrice)) {
            expect($location->price)->toEqual($actionPrice);
        } else {
            assertArrayNotHasKey('price', $location->toArray());
        }
        expect($action->body->locations)->toHaveCount(1);
    }
})->with([
    [true, 100, null, 100],
    [true, null, 100, 100],
    [true],
    [false],
]);

test("Action SyncProductsJob properties success", function (bool $withProperty) {
    /** @var SyncProductsJobTestCase $this */

    /** @var Product $product */
    $product = Product::factory()->active()->create();
    Offer::factory()->create(['product_id' => $product->product_id]);
    $property = Property::factory()->active()->create([
        'name' => 1,
    ]);
    $propertyValue = 123;
    if ($withProperty) {
        ProductPropertyValue::factory()->create([
            'product_id' => $product->product_id,
            'property_id' => $property->property_id,
            'directory_value_id' => null,
            'type' => PropertyTypeEnum::STRING,
            'value' => $propertyValue,
        ]);
    }

    $action = &$this->shouldSendProduct();

    SyncProductsJob::dispatchSync([$product->product_id]);

    $propOther = null;
    $propMainGroup = null;
    foreach ($action->body->properties as $prop) {
        if ($prop->name == $property->name) {
            $propOther = $prop;
        }

        if ($prop->name == SyncProductsAction::PROPERTY_MAIN_PRODUCT_NAME) {
            $propMainGroup = $prop;
        }
    }
    if ($withProperty) {
        expect($propOther->name)->toEqual($property->name)->toBeString();
        expect($propOther->values)->toEqual([$propertyValue]);
        expect($propOther->values[0])->toBeString();
    }

    expect($propMainGroup?->values ?? [])->toEqual([SyncProductsAction::PROPERTY_MAIN_PRODUCT_VALUE]);

    expect($action->body->properties)->toHaveCount($withProperty ? 2 : 1);
})->with([true, false]);

test("Action SyncProductsJob delete success", function (bool $cloudCreated) {
    /** @var SyncProductsJobTestCase $this */

    /** @var Product $product */
    $product = Product::factory()->trashed()->create([
        'cloud_created' => $cloudCreated,
    ]);
    Offer::factory()->create(['product_id' => $product->product_id]);

    if ($cloudCreated) {
        $action = &$this->shouldSendProduct();
    } else {
        $this->neverSendProduct();
    }

    SyncProductsJob::dispatchSync([$product->product_id]);

    if ($cloudCreated) {
        assertEquals(ProductActionEnum::DELETE, $action->action);
        assertEquals($product->product_id, $action->id);
    }

    assertDatabaseHas(Product::class, [
        'id' => $product->id,
        'cloud_created' => false,
    ]);
})->with([true, false]);

test("Action SyncProductsJob allow_publish success", function (
    bool $initCloudCreated,
    bool $active,
    ?ProductActionEnum $actionType,
    bool $resultCloudCreated,
) {
    /** @var SyncProductsJobTestCase $this */

    /** @var Product $product */
    $product = Product::factory()->active($active)->create([
        'cloud_created' => $initCloudCreated,
    ]);
    Offer::factory()->create(['product_id' => $product->product_id]);

    $action = null;
    if (is_null($actionType)) {
        $this->neverSendProduct();
    } else {
        $action = &$this->shouldSendProduct();
    }

    SyncProductsJob::dispatchSync([$product->product_id]);

    if (!is_null($actionType)) {
        assertEquals($actionType, $action->action);
        assertEquals($product->product_id, $action->id);
    }

    assertDatabaseHas(Product::class, [
        'id' => $product->id,
        'cloud_created' => $resultCloudCreated,
    ]);
})->with([
    [false, true, ProductActionEnum::CREATE, true],
    [true, true, ProductActionEnum::UPDATE, true],
    [false, false, null, false],
    [true, false, ProductActionEnum::DELETE, false],
]);
