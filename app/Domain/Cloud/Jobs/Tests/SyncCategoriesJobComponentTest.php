<?php

use App\Domain\Cloud\Jobs\SyncCategoriesJob;
use App\Domain\Cloud\Jobs\Tests\TestCases\SyncCategoriesJobTestCase;
use App\Domain\Offers\Models\Category;
use Ensi\CloudApiSdk\Dto\Indexes\IndexesCategories\CategoryAction\CategoryActionEnum;
use Illuminate\Database\Eloquent\Collection;

use function Pest\Laravel\assertDatabaseHas;
use function PHPUnit\Framework\assertArrayNotHasKey;
use function PHPUnit\Framework\assertEquals;

uses(SyncCategoriesJobTestCase::class);
uses()->group('component', 'SyncCategoriesJob');

test("Action SyncCategoriesJob integration disable", function () {
    /** @var SyncCategoriesJobTestCase $this */
    $this->cloudIntegrationDisable();

    /** @var Collection<Category> $categories */
    $categories = Category::factory()->count(2)->create();

    $this->neverSendCategory();

    SyncCategoriesJob::dispatchSync($categories->pluck('category_id')->all());
});

test("Action SyncCategoriesJob create|update success", function (bool $cloudCreated) {
    /** @var SyncCategoriesJobTestCase $this */

    /** @var Category $category */
    $category = Category::factory()->active()->create(['cloud_created' => $cloudCreated]);

    $action = &$this->shouldSendCategory();

    SyncCategoriesJob::dispatchSync([$category->category_id]);

    assertEquals($cloudCreated ? CategoryActionEnum::UPDATE : CategoryActionEnum::CREATE, $action->action);

    assertDatabaseHas(Category::class, [
        'id' => $category->id,
        'cloud_created' => true,
    ]);
})->with([true, false]);

test("Action SyncCategoriesJob base fields success", function () {
    /** @var SyncCategoriesJobTestCase $this */

    /** @var Category $category */
    $category = Category::factory()->active()->create();

    $action = &$this->shouldSendCategory();

    SyncCategoriesJob::dispatchSync([$category->category_id]);

    expect($action->id)->toEqual($category->category_id)->toBeString();

    $body = $action->body->toArray();
    assertEquals($category->name, $action->body->name);

    assertArrayNotHasKey('url', $body);
});

test("Action SyncCategoriesJob parent_ids success", function (bool $withParent) {
    /** @var SyncCategoriesJobTestCase $this */

    $expectIds = [];
    $parentId = null;
    if ($withParent) {
        /** @var Category $parentRoot */
        $parentRoot = Category::factory()->create(['parent_id' => null]);
        /** @var Category $parent */
        $parent = Category::factory()->create([
            'parent_id' => $parentRoot->category_id,
        ]);

        $expectIds = [$parentRoot->category_id, $parent->category_id,];
        $parentId = $parent->category_id;
    }

    /** @var Category $category */
    $category = Category::factory()->active()->create(['parent_id' => $parentId]);

    $action = &$this->shouldSendCategory();

    SyncCategoriesJob::dispatchSync([$category->category_id]);

    expect($action->body->parent_ids)->toEqualCanonicalizing($expectIds);
    foreach ($action->body->parent_ids as $value) {
        expect($value)->toBeString();
    }

})->with([true, false]);

test("Action SyncCategoriesJob delete success", function (bool $cloudCreated) {
    /** @var SyncCategoriesJobTestCase $this */

    /** @var Category $category */
    $category = Category::factory()->trashed()->create([
        'cloud_created' => $cloudCreated,
    ]);

    if ($cloudCreated) {
        $action = &$this->shouldSendCategory();
    } else {
        $this->neverSendCategory();
    }

    SyncCategoriesJob::dispatchSync([$category->category_id]);

    if ($cloudCreated) {
        assertEquals(CategoryActionEnum::DELETE, $action->action);
        assertEquals($category->category_id, $action->id);
    }

    assertDatabaseHas(Category::class, [
        'id' => $category->id,
        'cloud_created' => false,
    ]);
})->with([true, false]);

test("Action SyncCategoriesJob active success", function (
    bool $initCloudCreated,
    bool $active,
    ?CategoryActionEnum $actionType,
    bool $resultCloudCreated,
) {
    /** @var SyncCategoriesJobTestCase $this */

    /** @var Category $category */
    $category = Category::factory()->active($active)->create([
        'cloud_created' => $initCloudCreated,
    ]);

    $action = null;
    if (is_null($actionType)) {
        $this->neverSendCategory();
    } else {
        $action = &$this->shouldSendCategory();
    }

    SyncCategoriesJob::dispatchSync([$category->category_id]);

    if (!is_null($actionType)) {
        assertEquals($actionType, $action->action);
        assertEquals($category->category_id, $action->id);
    }

    assertDatabaseHas(Category::class, [
        'id' => $category->id,
        'cloud_created' => $resultCloudCreated,
    ]);
})->with([
    [false, true, CategoryActionEnum::CREATE, true],
    [true, true, CategoryActionEnum::UPDATE, true],
    [false, false, null, false],
    [true, false, CategoryActionEnum::DELETE, false],
]);
