<?php

namespace App\Domain\Cloud\Jobs;

use App\Domain\Cloud\Actions\CloudIntegrations\CheckCloudIntegrationEnabledAction;
use App\Domain\Cloud\Actions\Sync\SyncProductsAction;
use App\Domain\Offers\Models\Product;

class SyncProductsJob extends SyncEntityJob
{
    public function __construct(array $ids)
    {
        parent::__construct($ids);

        $this->queue = "{cloud-sync-products}";
    }

    public function handle(
        SyncProductsAction $action,
        CheckCloudIntegrationEnabledAction $checkAction,
    ): void {
        if (!$checkAction->execute()) {
            return;
        }

        $products = Product::withTrashed()
            ->whereIn('product_id', $this->ids)
            ->get();

        $action->execute($products);
    }
}
