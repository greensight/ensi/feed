<?php

namespace App\Domain\Discounts\Events;

use App\Domain\Discounts\Data\DiscountOfferIdentify;
use Event;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CalculateDiscount
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    public function __construct(public DiscountOfferIdentify $data)
    {
    }

    public static function assertDispatched(self $needEvent): void
    {
        Event::assertDispatched(function (CalculateDiscount $event) use ($needEvent) {
            if (count($event->data->offerIds) != count($needEvent->data->offerIds)) {
                return false;
            }

            foreach ($event->data->offerIds as $offerId) {
                if (!in_array($offerId, $needEvent->data->offerIds)) {
                    return false;
                }
            }

            return true;
        });
    }
}
