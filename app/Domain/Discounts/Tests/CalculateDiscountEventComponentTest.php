<?php

namespace App\Domain\Discounts\Tests;

use App\Domain\Discounts\Data\DiscountOfferIdentify;
use App\Domain\Discounts\Events\CalculateDiscount;
use App\Domain\Discounts\Listeners\DiscountCalculateListener;
use App\Domain\Discounts\Tests\Factories\CalculateCatalogResponseFactory;
use App\Domain\Discounts\Tests\Factories\CalculatedOfferFactory;
use App\Domain\Offers\Models\Category;
use App\Domain\Offers\Models\Offer;
use App\Domain\Offers\Models\Product;
use Ensi\LaravelTestFactories\FakerProvider;
use Ensi\MarketingClient\Dto\CalculateCatalogRequest;
use Ensi\MarketingClient\Dto\DiscountValueTypeEnum;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Event;

use function Pest\Laravel\assertDatabaseHas;
use function PHPUnit\Framework\assertEqualsCanonicalizing;

use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('component', 'discounts');

test("Event CalculateDiscount check marketing price", function (?bool $always) {
    /** @var ComponentTestCase $this */
    FakerProvider::$optionalAlways = $always;
    Event::fake([
        CalculateDiscount::class,
    ]);

    $discountValue = 5_00;

    /** @var Offer $offerWithPrice */
    $offerWithPrice = Offer::factory()->withBasePrice()->create(['price' => 10_00]);
    /** @var Offer $offerWithCalc */
    $offerWithCalc = Offer::factory()->withBasePrice()->create(['price' => null]);
    $offerWithCalcPrice = $offerWithCalc->base_price - 5_00;
    /** @var Offer $offerWithBadCalc */
    $offerWithBadCalc = Offer::factory()->withBasePrice()->create(['price' => null]);
    /** @var Offer $offerWithoutPrice */
    $offerWithoutPrice = Offer::factory()->create(['base_price' => null, 'price' => null]);

    /** @var Category $category */
    $category = Category::factory()->create();

    $factory = Product::factory()->inCategories([$category])->state(['brand_id' => null]);
    // createQuietly() to avoid CategoryProductLinkObserver being triggered
    /** @var Product $productWithPrice */
    $productWithPrice = $factory->createQuietly(['product_id' => $offerWithPrice->product_id]);
    /** @var Product $productWithCalc */
    $productWithCalc = $factory->createQuietly(['product_id' => $offerWithCalc->product_id]);
    /** @var Product $productWithBadCalc */
    $productWithBadCalc = $factory->createQuietly(['product_id' => $offerWithBadCalc->product_id]);
    /** @var Product $productWithoutPrice */
    $productWithoutPrice = $factory->createQuietly(['product_id' => $offerWithoutPrice->product_id]);

    $offer = CalculatedOfferFactory::new()
        ->withDiscounts([
            'value' => $discountValue,
            'value_type' => DiscountValueTypeEnum::RUB,
        ])
        ->make([
            'offer_id' => $offerWithCalc->offer_id,
            'price' => $offerWithCalcPrice,
        ]);

    $this->mockMarketingCalculatorsApi()
        ->shouldReceive('calculateCatalog')
        ->once()
        ->withArgs(function (CalculateCatalogRequest $request) use ($offerWithPrice, $offerWithCalc, $offerWithBadCalc) {
            assertEqualsCanonicalizing(
                [$offerWithPrice->offer_id, $offerWithCalc->offer_id, $offerWithBadCalc->offer_id],
                Arr::pluck($request->getOffers(), 'offer_id')
            );

            return true;
        })
        ->andReturn(CalculateCatalogResponseFactory::new()->withOffer($offer)->make());

    $event = new CalculateDiscount(new DiscountOfferIdentify([
        $offerWithCalc->offer_id,
        $offerWithBadCalc->offer_id,
        $offerWithPrice->offer_id,
        $offerWithoutPrice->offer_id,
    ]));
    /** @var DiscountCalculateListener $listener */
    $listener = resolve(DiscountCalculateListener::class);
    $listener->handle($event);

    assertDatabaseHas(Offer::class, ['offer_id' => $offerWithCalc->offer_id, 'price' => $offerWithCalcPrice]);
    assertDatabaseHas(Offer::class, ['offer_id' => $offerWithBadCalc->offer_id, 'price' => null]);
    assertDatabaseHas(Offer::class, ['offer_id' => $offerWithPrice->offer_id, 'price' => $offerWithPrice->price]);
    assertDatabaseHas(Offer::class, ['offer_id' => $offerWithoutPrice->offer_id, 'price' => null]);

    assertNewModelFieldEquals($productWithPrice, 'cloud_fields_updated_at');
    assertNewModelFieldGreaterThan($productWithCalc, 'cloud_fields_updated_at');
    assertNewModelFieldEquals($productWithBadCalc, 'cloud_fields_updated_at');
    assertNewModelFieldEquals($productWithoutPrice, 'cloud_fields_updated_at');
})->with(FakerProvider::$optionalDataset);
