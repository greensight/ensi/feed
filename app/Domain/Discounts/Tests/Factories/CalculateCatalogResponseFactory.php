<?php

namespace App\Domain\Discounts\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\MarketingClient\Dto\CalculateCatalogResponse;
use Ensi\MarketingClient\Dto\CalculateCatalogResponseData;
use Ensi\MarketingClient\Dto\CalculatedOffer;

class CalculateCatalogResponseFactory extends BaseApiFactory
{
    protected array $offers = [];

    protected function definition(): array
    {
        return [
            'offers' => $this->makeOffers(),
        ];
    }

    public function make(array $extra = []): CalculateCatalogResponse
    {
        return new CalculateCatalogResponse(['data' => new CalculateCatalogResponseData($this->makeArray($extra))]);
    }

    protected function makeOffers(): array
    {
        $count = $this->offers ? count($this->offers) : $this->faker->numberBetween(1, 10);
        $offers = [];
        for ($i = 0; $i < $count; $i++) {
            $offers[] = CalculatedOfferFactory::new()->make($this->offers[$i] ?? []);
        }

        return $offers;
    }

    public function withOffer(CalculatedOffer $offer): static
    {
        $this->offers[] = $offer->toArray();

        return $this;
    }
}
