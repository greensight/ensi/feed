<?php

namespace App\Domain\Discounts\Listeners;

use App\Domain\Discounts\Actions\CalculateDiscountsAction;
use App\Domain\Discounts\Events\CalculateDiscount;

class DiscountCalculateListener
{
    public function __construct(private readonly CalculateDiscountsAction $action)
    {
    }

    public function handle(CalculateDiscount $event): void
    {
        if (filled($event->data->offerIds)) {
            $this->action->execute($event->data->offerIds);
        }
    }
}
