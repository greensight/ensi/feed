<?php

namespace App\Domain\Discounts\Data;

class DiscountOfferIdentify
{
    public function __construct(public array $offerIds = [])
    {
    }
}
