<?php

namespace App\Domain\Feeds\Actions;

use App\Domain\Feeds\Models\FeedSettings;

class CreateFeedSettingsAction
{
    public function execute(array $fields): FeedSettings
    {
        $feedSettings = new FeedSettings();
        $feedSettings->fill($fields);
        $feedSettings->save();

        return $feedSettings;
    }
}
