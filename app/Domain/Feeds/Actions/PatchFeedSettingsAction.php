<?php

namespace App\Domain\Feeds\Actions;

use App\Domain\Feeds\Models\FeedSettings;

class PatchFeedSettingsAction
{
    public function execute(int $id, array $fields): FeedSettings
    {
        /** @var FeedSettings $feedSettings */
        $feedSettings = FeedSettings::query()->findOrFail($id);

        $feedSettings->fill($fields);
        $feedSettings->save();

        return $feedSettings;
    }
}
