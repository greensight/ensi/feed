<?php

namespace App\Domain\Feeds\Actions;

use App\Domain\Feeds\Models\Feed;
use App\Exceptions\ExceptionFormatter;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Exception;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Psr\Log\LoggerInterface;

class DeleteOldFeedsAction
{
    public LoggerInterface $logger;

    public function __construct(protected EnsiFilesystemManager $fs)
    {
        $this->logger = Log::channel('feeds');
    }

    public function execute(): void
    {
        $disk = Storage::disk($this->fs->publicDiskName());

        /** @var Collection<Feed> $feeds */
        $feeds = Feed::query()->where('planned_delete_at', '<=', Date::now())->get();

        foreach ($feeds as $feed) {
            try {
                $this->deleteFeed($disk, $feed);
                $this->logger->error("Фид с кодом {$feed->code} удален");
            } catch (Exception $e) {
                $this->logger->error(ExceptionFormatter::line("Не удалось удалить фид с кодом {$feed->code}", $e));
            }
        }
    }

    private function deleteFeed(FilesystemAdapter $disk, Feed $feed): void
    {
        if ($disk->exists($feed->file)) {
            $disk->delete($feed->file);
        }

        $feed->delete();
    }
}
