<?php

namespace App\Domain\Feeds\Actions;

use App\Domain\Feeds\Models\FeedSettings;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Support\Facades\Storage;

class PublishFeedAction
{
    public function __construct(
        protected EnsiFilesystemManager $fs,
        protected ArchiveFeedAction $archiveFeedAction,
    ) {
    }

    public function execute(FeedSettings $feedSettings, string $tempPath, string $publicPath): void
    {
        $disk = Storage::disk($this->fs->publicDiskName());

        if ($feedSettings->latestFeed) {
            $this->archiveFeedAction->execute($feedSettings->latestFeed);
        }

        $disk->move($tempPath, $publicPath);

        $feedSettings->addFeed($publicPath);
    }
}
