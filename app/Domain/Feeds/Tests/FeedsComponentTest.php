<?php

namespace App\Domain\Feeds\Tests;

use App\Domain\Feeds\Actions\PublishFeedAction;
use App\Domain\Feeds\Models\Feed;
use App\Domain\Feeds\Models\FeedSettings;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Storage;

use function Pest\Laravel\assertDatabaseHas;

use PHPUnit\Framework\Assert;
use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('component', 'feeds');

test('Action PublishFeedAction success', function () {
    $fs = resolve(EnsiFilesystemManager::class);
    $disk = Storage::fake($fs->publicDiskName());

    $now = Carbon::now();
    $stringNow = $now->toIso8601String();
    Carbon::setTestNow($now);

    $feedName = 'feed.xml';
    $tempFeedName = 'temp-feed.xml';
    $historyFeedName = "feed-$stringNow.xml";

    /** @var FeedSettings $newFeedSettings */
    $feedSettings = FeedSettings::factory()->create(['code' => $feedName]);
    /** @var Feed $feed */
    $feed = Feed::factory()->for($feedSettings)->withRealFile($fs->publicDiskName(), $feedName)->create([
        'code' => $feedName,
        'planned_delete_at' => Date::now()->subHours(),
    ]);
    $disk->put($tempFeedName, 'data');

    $publishFeedAction = resolve(PublishFeedAction::class);
    $publishFeedAction->execute($feedSettings, $tempFeedName, $feedName);

    assertDatabaseHas(Feed::class, ['id' => $feed->id, 'code' => $historyFeedName]);
    assertDatabaseHas(Feed::class, ['code' => $feedName]);

    Assert::assertEquals($disk->exists($tempFeedName), false);
    Assert::assertEquals($disk->exists($feedName), true);
    Assert::assertEquals($disk->exists($historyFeedName), true);
});
