<?php

namespace App\Domain\Feeds\Models;

use App\Domain\Feeds\Models\Tests\Factories\FeedSettingsFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\FeedPlatformEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\FeedTypeEnum;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * @property int $id
 *
 * @property string $name - Название
 * @property string $code - Код фида
 * @property bool $active - Активность
 * @property FeedTypeEnum $type - Расширение фида
 * @property FeedPlatformEnum $platform - Платформа для которой генерируется фид
 *
 * @property bool $active_product - Добавлять в фид только активные товары
 * @property bool $active_category - Добавлять в фид только активные категории
 *
 * @property string $shop_name - Название магазина
 * @property string $shop_url - Сайт магазина
 * @property string $shop_company - Название организации
 *
 * @property int $update_time - Частота обновления (в часах)
 * @property int $delete_time - Время хранения старых версий фидов (в днях)
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property-read Feed|null $latestFeed - Актуальный сгенерированный фид
 * @property-read Collection<Feed> $feeds - Фиды сгенерированные для данного шаблона
 *
 * @method static Builder|static forGenerate()
 */
class FeedSettings extends Model
{
    protected $table = 'feed_settings';

    protected $fillable = [
        'name', 'code', 'active', 'type', 'platform',
        'shop_name', 'shop_url', 'shop_company',
        'active_product', 'active_category',
        'update_time', 'delete_time',
    ];

    protected $casts = [
        'active' => 'bool',
        'type' => FeedTypeEnum::class,
        'platform' => FeedPlatformEnum::class,
        'active_product' => 'bool',
        'active_category' => 'bool',
        'update_time' => 'int',
        'delete_time' => 'int',
    ];

    public function isTimeGenerate(): bool
    {
        $this->load('latestFeed');

        if ($this->latestFeed == null) {
            return true;
        }

        return $this->latestFeed->created_at->addHours($this->update_time) < Carbon::now();
    }

    public function scopeForGenerate(Builder $query): Builder
    {
        return $query->with('latestFeed')->where('active', true);
    }

    public function addFeed(string $path): void
    {
        $feed = new Feed();
        $feed->file = $path;
        $feed->code = $this->code;
        $feed->planned_delete_at = Carbon::now()->addDays($this->delete_time);

        $this->feeds()->save($feed);
    }

    public function latestFeed(): HasOne
    {
        return $this->hasOne(Feed::class)->latestOfMany();
    }

    public function feeds(): HasMany
    {
        return $this->hasMany(Feed::class);
    }

    public static function factory(): FeedSettingsFactory
    {
        return FeedSettingsFactory::new();
    }
}
