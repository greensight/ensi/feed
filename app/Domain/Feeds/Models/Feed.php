<?php

namespace App\Domain\Feeds\Models;

use App\Domain\Feeds\Models\Tests\Factories\FeedFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property int $feed_settings_id
 *
 * @property string $code - Уникальный код
 * @property string $file - Файл фида
 *
 * @property CarbonInterface $planned_delete_at - Дата в которую нужно удалить фид
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property-read FeedSettings $feedSettings - Настройки фида
 */
class Feed extends Model
{
    protected $table = 'feeds';

    protected $casts = [
        'feed_settings_id' => 'int',
        'planned_delete_at' => 'date',
    ];

    public function feedSettings(): BelongsTo
    {
        return $this->belongsTo(FeedSettings::class);
    }

    public static function factory(): FeedFactory
    {
        return FeedFactory::new();
    }
}
