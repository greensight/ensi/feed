<?php

namespace App\Domain\Feeds\Models\Tests\Factories;

use App\Domain\Feeds\Models\Feed;
use App\Domain\Feeds\Models\FeedSettings;
use Ensi\LaravelTestFactories\BaseModelFactory;
use Illuminate\Support\Facades\Storage;

/**
 * @extends BaseModelFactory<Feed>
 */
class FeedFactory extends BaseModelFactory
{
    protected $model = Feed::class;

    public function definition(): array
    {
        return [
            'feed_settings_id' => FeedSettings::factory(),
            'code' => $this->faker->unique()->word(),
            'file' => $this->faker->filePath(),
            'planned_delete_at' => $this->faker->dateTime(),
        ];
    }

    public function withRealFile(string $disk, ?string $path = null): self
    {
        $path = $path ?? $this->faker->unique()->filePath();
        Storage::disk($disk)->put($path, 'data');

        return $this->state(['file' => $path]);
    }
}
