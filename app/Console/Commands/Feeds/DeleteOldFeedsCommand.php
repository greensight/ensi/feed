<?php

namespace App\Console\Commands\Feeds;

use App\Domain\Feeds\Actions\DeleteOldFeedsAction;
use Illuminate\Console\Command;

class DeleteOldFeedsCommand extends Command
{
    protected $signature = 'feeds:delete-old';
    protected $description = 'Удаление неактуальных фидов';

    public function handle(DeleteOldFeedsAction $deleteOldFeedsAction): void
    {
        $deleteOldFeedsAction->execute();
    }
}
