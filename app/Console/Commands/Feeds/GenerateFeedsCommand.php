<?php

namespace App\Console\Commands\Feeds;

use App\Domain\Feeds\Job\GenerateFeedJob;
use App\Domain\Feeds\Models\FeedSettings;
use App\Exceptions\ExceptionFormatter;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;

class GenerateFeedsCommand extends Command
{
    protected $signature = 'feeds:generate {--code=}';

    protected $description = 'Генерация фидов
                                --code: Запуск генерацию определенного фида по коду, без учета расписания';

    public LoggerInterface $logger;

    public function __construct()
    {
        parent::__construct();
        $this->logger = Log::channel('feeds');
    }

    public function handle(): void
    {
        $feedSettings = $this->getFeeds();

        try {
            foreach ($feedSettings as $feedSettingsItem) {
                GenerateFeedJob::dispatch($feedSettingsItem);
            }
        } catch (Exception $e) {
            $this->logger->error(ExceptionFormatter::line("Ошибка при генерации фидов", $e));
        }
    }

    /**
     * Получить фиды доступные для генерации
     *
     * @return Collection<FeedSettings>
     */
    protected function getFeeds(): Collection
    {
        $feedQuery = FeedSettings::forGenerate();

        if ($this->option('code')) {
            return $feedQuery->where('code', $this->option('code'))->get();
        }

        return $feedQuery->get()
            ->filter(fn (FeedSettings $feedSettings) => $feedSettings->isTimeGenerate());
    }
}
