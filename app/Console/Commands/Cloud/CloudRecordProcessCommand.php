<?php

namespace App\Console\Commands\Cloud;

use App\Domain\Cloud\Actions\CloudIntegrations\CheckCloudIntegrationEnabledAction;
use App\Domain\Cloud\Actions\DispatchToSync\DispatchCategoriesToSyncAction;
use App\Domain\Cloud\Actions\DispatchToSync\DispatchProductsToSyncAction;
use Illuminate\Console\Command;

class CloudRecordProcessCommand extends Command
{
    protected $signature = 'cloud:records-process';
    protected $description = 'Добавить записи на синхронизацию с Cloud';

    public function handle(
        DispatchProductsToSyncAction $productsToSyncAction,
        DispatchCategoriesToSyncAction $categoriesToSyncAction,
        CheckCloudIntegrationEnabledAction $checkAction,
    ): void {
        if (!$checkAction->execute()) {
            return;
        }

        $productsToSyncAction->execute();
        $categoriesToSyncAction->execute();
    }
}
