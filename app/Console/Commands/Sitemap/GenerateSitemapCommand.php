<?php

namespace App\Console\Commands\Sitemap;

use App\Domain\Sitemap\Actions\GenerateSitemapAction;
use App\Exceptions\ExceptionFormatter;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;

class GenerateSitemapCommand extends Command
{
    protected $signature = 'sitemap:generate';
    protected $description = 'Сгенерировать сайтмап';

    public LoggerInterface $logger;

    public function __construct()
    {
        parent::__construct();
        $this->logger = Log::channel('sitemap');
    }

    public function handle(GenerateSitemapAction $generateSitemapAction): void
    {
        try {
            $this->logger->info('Начала генерации Sitemap');
            $generateSitemapAction->setLogger($this->logger)->execute();
            $this->logger->info('Sitemap успешно сгенерирован');
        } catch (Exception $e) {
            $this->logger->error(ExceptionFormatter::line("Ошибка при генерации Sitemap", $e));
        }
    }
}
