<?php

namespace App\Console\Tests;

use App\Console\Commands\Feeds\DeleteOldFeedsCommand;
use App\Console\Commands\Feeds\GenerateFeedsCommand;
use App\Domain\Feeds\Models\Feed;
use App\Domain\Feeds\Models\FeedSettings;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Storage;

use function Pest\Laravel\artisan;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelExists;
use function Pest\Laravel\assertModelMissing;

use PHPUnit\Framework\Assert;
use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('component', 'feeds');

test('Command GenerateFeedsCommand with code', function () {
    Storage::fake();

    $feedCode = 'test.xml';

    FeedSettings::factory()->active()->create(['code' => $feedCode]);

    artisan(GenerateFeedsCommand::class, [
        '--code' => $feedCode,
    ]);

    assertDatabaseHas(Feed::class, ['code' => $feedCode]);
});

test('Command feeds:generate scheduled generation', function () {
    Storage::fake();

    /** @var FeedSettings $newFeedSettings */
    $newFeedSettings = FeedSettings::factory()->timeGenerate(withFeed: false)->create();
    /** @var FeedSettings $feedSettingsWithFeed */
    $feedSettingsWithFeed = FeedSettings::factory()->timeGenerate(withFeed: true)->create();

    /** @var FeedSettings $feedSettingsOutOfSchedule */
    $feedSettingsOutOfSchedule = FeedSettings::factory()->create();
    Feed::factory()->for($feedSettingsOutOfSchedule)->create();


    artisan(GenerateFeedsCommand::class);

    $newFeedSettings->load('feeds');
    $feedSettingsWithFeed->load('feeds');
    $feedSettingsOutOfSchedule->load('feeds');

    Assert::assertEquals(1, count($newFeedSettings->feeds));
    Assert::assertEquals(2, count($feedSettingsWithFeed->feeds));
    Assert::assertEquals(1, count($feedSettingsOutOfSchedule->feeds));
});

test('Command DeleteOldFeedsCommand success', function () {
    $fs = resolve(EnsiFilesystemManager::class);
    $disk = Storage::fake($fs->publicDiskName());

    /** @var FeedSettings $newFeedSettings */
    $feedSettings = FeedSettings::factory()->create();
    /** @var Feed $historyFeed */
    $historyFeed = Feed::factory()->for($feedSettings)->withRealFile($fs->publicDiskName())->create([
        'code' => 'history-feed.xml',
        'planned_delete_at' => Date::now()->subHours(),
    ]);
    /** @var Feed $actualFeed */
    $actualFeed = Feed::factory()->for($feedSettings)->withRealFile($fs->publicDiskName())->create([
        'code' => 'actual-feed.xml',
        'planned_delete_at' => Date::now()->addHour(),
    ]);

    artisan(DeleteOldFeedsCommand::class);

    assertModelExists($actualFeed);
    assertModelMissing($historyFeed);

    Assert::assertEquals($disk->exists($historyFeed->file), false);
    Assert::assertEquals($disk->exists($actualFeed->file), true);
});
