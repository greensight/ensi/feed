<?php

namespace App\Providers;

use App\Domain\Discounts\Events\CalculateDiscount;
use App\Domain\Discounts\Listeners\DiscountCalculateListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        CalculateDiscount::class => [DiscountCalculateListener::class],
    ];

    public function boot(): void
    {
        //
    }

    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
