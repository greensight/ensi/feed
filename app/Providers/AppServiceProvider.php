<?php

namespace App\Providers;

use App\Domain\Offers\Models\Brand;
use App\Domain\Offers\Models\Category;
use App\Domain\Offers\Models\CategoryProductLink;
use App\Domain\Offers\Models\Offer;
use App\Domain\Offers\Models\Product;
use App\Domain\Offers\Models\ProductGroup;
use App\Domain\Offers\Models\ProductGroupProduct;
use App\Domain\Offers\Models\ProductPropertyValue;
use App\Domain\Offers\Models\Property;
use App\Domain\Offers\Models\PropertyDirectoryValue;
use App\Domain\Offers\Observers\BrandObserver;
use App\Domain\Offers\Observers\CategoryObserver;
use App\Domain\Offers\Observers\CategoryProductLinkObserver;
use App\Domain\Offers\Observers\OfferObserver;
use App\Domain\Offers\Observers\ProductGroupObserver;
use App\Domain\Offers\Observers\ProductGroupProductObserver;
use App\Domain\Offers\Observers\ProductObserver;
use App\Domain\Offers\Observers\ProductPropertyValueObserver;
use App\Domain\Offers\Observers\PropertyDirectoryValueObserver;
use App\Domain\Offers\Observers\PropertyObserver;
use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\ServiceProvider;
use Spatie\QueryBuilder\QueryBuilderRequest;

class AppServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        //
    }

    public function boot(): void
    {
        Model::preventLazyLoading(!app()->isProduction());
        Date::use(CarbonImmutable::class);
        QueryBuilderRequest::setFilterArrayValueDelimiter('');

        Brand::observe(BrandObserver::class);
        Category::observe(CategoryObserver::class);
        Offer::observe(OfferObserver::class);
        ProductGroup::observe(ProductGroupObserver::class);
        ProductGroupProduct::observe(ProductGroupProductObserver::class);
        Product::observe(ProductObserver::class);
        ProductPropertyValue::observe(ProductPropertyValueObserver::class);
        PropertyDirectoryValue::observe(PropertyDirectoryValueObserver::class);
        Property::observe(PropertyObserver::class);
        CategoryProductLink::observe(CategoryProductLinkObserver::class);
    }
}
